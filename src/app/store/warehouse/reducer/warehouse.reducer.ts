import { Action, createReducer, on } from '@ngrx/store';
import { warehouseAction } from '../action/warehouse.action';

export interface State {
  data: any;
}

export const initialState: State = {
    data: {},
  };

const Reducer = createReducer(
  initialState,
  on(warehouseAction, state => ({ ...state })),
);

export function warehouseReducer(state: State | undefined, action: Action) {
    return Reducer(state, action);
}
