import { ActionReducerMap } from '@ngrx/store';

import { warehouseReducer } from './warehouse/reducer/warehouse.reducer';

export interface IAppState {
  warehouseData: any;
}

export const reducers: ActionReducerMap<IAppState> = {
  warehouseData: warehouseReducer,
};
