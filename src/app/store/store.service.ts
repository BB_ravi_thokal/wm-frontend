// IONIC - ANGULAR
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { warehouseAction } from './warehouse/action/warehouse.action';

@Injectable()

export class StoreService {
  warehouseData$: Observable<any>;
  showLoader$: Observable<boolean>;
  constructor(
    private store: Store<{ warehouseData: any }>,
  ) {
    this.warehouseData$ = this.store.pipe(select('warehouseData'));
  }

  public dispatchWarehouseData(data) {
    console.log('in dispatchWarehouseData', data);
    this.store.dispatch(warehouseAction({ payload: { warehouseData: data } }));
  }

  /**
   * @description This function will return the latest state of warehouse from store
   * @returns object of user data
   */
  public async getWarehouseData() {
    let details;
    this.warehouseData$.subscribe((data) => {
      console.log('data in get warehouse', data);
      if (Object.keys(data.warehouseData).length > 0) {
        console.log('in getWarehouseData', data);
        details = data.warehouseData;
      }
    });

    return details;
  }
}
