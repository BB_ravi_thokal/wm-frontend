export interface UpdateVendor {
    isDeleted?: boolean;
    _id: string;
    auth?: string;
    warehouse: string;
    name: string;
    emailId: string;
    contactNumber: number;
    TotalAmountPaid?: number;
    pendingDues?: number;
}

export interface Vendor {
    name: string;
    role: string;
    warehouseId: string;
    contactNumber: number;
    emailId: string;
}

export interface PackagingType {
    _id?: string;
    warehouseId: string;
    packagingType: string;
    isDeleted?: boolean;
}

export interface Product {
    warehouseId: string;
    packagingTypeId: string;
    name: string;
    rate: number;
    vendor: string;
    _id?: string;
    isDeleted?: boolean;
}

export interface Transaction {
    _id?: string;
    warehouse: string;
    vendor: string;
    productDetails: any[];
    DONumber: string;
    actionType: string;
    vehicleNumber: string;
    messageBy: string;
    driverContactNumber: string;
    isFaulty?: boolean;
    faultyProduct?: any[];
}
