import { Inject, Injectable } from '@angular/core';
import * as WebURL from '../../config/web-urls';
import { Observable } from 'rxjs';
import { BASE_URL } from '../../config/app-config';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import * as Valid from '../../interface/warehouse.interface';

@Injectable({
    providedIn: 'root'
})
export class VendorService {
    constructor(
        private http: HttpClient,
    ) { }

    // API related to Vendor  -------- Started -----------------------------
    getVendor$(url, params): Observable<any> {
        console.log(url, 'url');
        console.log(BASE_URL, 'BASE_URL');
        console.log('params', params);
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        const httpParams = new HttpParams()
            .set('warehouseId', params.warehouseId)
            .set('vendorId', params.vendorId);
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get<any>(BASE_URL + url, httpOptions);
    }

    getTransaction$(url, params): Observable<any> {
        console.log(url, 'url');
        console.log(BASE_URL, 'BASE_URL');
        console.log('params', params);
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        const httpParams = new HttpParams()
            .set('transactionId', params.transactionId)
            .set('warehouseId', params.warehouseId)
            .set('vendorId', params.vendorId)
            .set('productId', params.productId)
            .set('paginationObject', params.paginationObject);
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get<any>(BASE_URL + url, httpOptions);
    }

    getProducts$(url, params): Observable<any> {
        console.log(url, 'url');
        console.log(BASE_URL, 'BASE_URL');
        console.log('params', params);
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        const httpParams = new HttpParams()
            .set('productId', params.productId)
            .set('warehouseId', params.warehouseId)
            .set('packageTypeId', params.packageTypeId)
            .set('vendorId', params.vendorId);
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get<any>(BASE_URL + url, httpOptions);
    }

}
