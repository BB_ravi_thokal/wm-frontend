import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorRoutingModule } from './vendor-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ReportComponent } from './report/report.component';
import { SharedModule } from '../shared/shared.module';
import { VendorService } from './vendor.service';
import { StoreService } from '../../store/store.service';

@NgModule({
  declarations: [ ReportComponent ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule,
    SharedModule,
    VendorRoutingModule
  ],
  providers: [ VendorService, StoreService ]
})
export class VendorModule { }
