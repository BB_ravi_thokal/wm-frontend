import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaderState$, PopUpState$ } from '../../../app.component';
import { VendorService } from '../vendor.service';
import { Router } from '@angular/router';
import * as WebURL from '../../../config/web-urls';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['../../../app.component.scss', './report.component.scss']
})
export class ReportComponent implements OnInit {
  paginationObject = {
    filterBy: '',
    searchData: '',
    totalItems: '',
    itemsPerPage: 10,
    currentPage: 1,
    startDate: null,
    endDate: null,
    getAllData: false
  };
  pageName = 'Report';
  showButton = true;
  items = [];
  showTable = false;
  params;
  vendorData;
  invoiceHeader;
  allImportTrans = [];
  allProduct = [];
  actions;
  productData;
  submitted = false;
  showTransactionDetails = false;
  updateDetails: any;
  showConfirmModal = false;
  itemToDelete: any;
  reqBody: any;
  selectedProduct;
  constructor(
    private vendorService: VendorService,
    private router: Router,
  ) { }

  async ngOnInit() {
    const temp = await localStorage.getItem('VendorData');
    this.vendorData = JSON.parse(temp);
    console.log('warehouse data in vendor', this.vendorData);
    if (!this.vendorData) {
      localStorage.clear();
      this.router.navigate(['dw-home']);
      return;
    }
    this.getImportTransactions();
    this.getProducts();
    this.actions = {
      isDelete: false,
      isEdit: false,
      isDetailsView: true
    };
  }


  async getImportTransactions() {
    LoaderState$.next(true);
    const params = {
      transactionId: '',
      warehouseId: this.vendorData.warehouse._id,
      vendorId: this.vendorData._id,
      productId: (this.selectedProduct != 'Select' && this.selectedProduct != undefined) ? this.selectedProduct : '',
      paginationObject: JSON.stringify(this.paginationObject)
    };
    console.log('params in get import', params);
    await this.vendorService.getTransaction$(WebURL.GET_TRANSACTION, params).subscribe({
      next: (results) => {
        LoaderState$.next(false);
        console.log('get import trans result', results);
        this.items = [];
        if (results.data.result.length > 0) {
          this.allImportTrans = results.data.result;
          results.data.result.forEach(async (element) => {
            const importObj = {
              id: element._id,
              'Product/Qty': await this.getProductName(element.productDetails),
              Vendor: element.vendor.name,
              'DO Number': element.DONumber,
              'Vehicle Number': element.vehicleNumber,
              Date: new Date(element.createdAt).toLocaleDateString(),
              'Bill Type': element.actionType
            };
            this.items.push(importObj);
          });
          this.paginationObject.totalItems = results.data.count;
          this.showTable = true;
        } else {
          this.items = [];
          this.showTable = false;
        }
      }, error: error => {
        LoaderState$.next(false);
        if (error.status === 404) {
          PopUpState$.next({
            state: 'error',
            message: error.error.message ? error.error.message : 'No past transaction'
          });
        }
        console.log('error in get transaction', error);
      }
    });
  }

  async getProductName(productData) {
    let tagStr = '';
    console.log('tagStr 111', Array.isArray(productData));
    if (!Array.isArray(productData)) {
      tagStr = tagStr + ' ' + productData.name + ' (' + productData.quantity + ')';
    } else {
      for (const element of productData) {
        if (tagStr !== '') {
          tagStr = tagStr + ', ';
        }
        console.log('in loop get name');
        tagStr = tagStr + ' ' + element.name + ' (' + element.quantity + ')'; 
      }
    }
    console.log('tagStr', tagStr);
    return tagStr;
  }

  changePage(event) {
    this.paginationObject.currentPage = event;
  }

  disableDetailsModal() {
    this.showTransactionDetails = false;
  }

  showUpdateModal(event) {
    console.log('in show details view', event);
    const index = this.allImportTrans.findIndex(ind => ind._id === event.id);
    this.updateDetails = this.allImportTrans[index];
    console.log('this.updateDetails -->', this.updateDetails);
    this.invoiceHeader = (this.updateDetails.actionType === 'import') ? 'Import Transaction Invoice' : 'Export Transaction Invoice';
    this.showTransactionDetails = true;
  }

  async getProducts() {
    const params = {
      productId: '',
      warehouseId: this.vendorData.warehouse._id,
      packageTypeId: '',
      vendorId: this.vendorData._id
    };
    console.log('params get product', params);
    await this.vendorService.getProducts$(WebURL.GET_PRODUCT, params).subscribe({
      next: (results) => {
        console.log('get product result', results);
        if (results.data.result.length > 0) {
          this.allProduct = results.data.result;
        } else {
          this.allProduct = [];
        }
      }, error: error => {
      }
    });
  }

  async logout() {
    await localStorage.clear();
    this.router.navigate(['dw-home']);
  }

}

