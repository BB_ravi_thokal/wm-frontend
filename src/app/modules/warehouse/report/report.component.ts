import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaderState$, PopUpState$ } from '../../../app.component';
import { WarehouseService } from '../warehouse.service';
import { Router } from '@angular/router';
import * as WebURL from '../../../config/web-urls';
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'


@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['../../../app.component.scss', './report.component.scss']
})
export class ReportComponent implements OnInit {

  @ViewChild('content') content: ElementRef;
  paginationObject = {
    filterBy:'',
    searchData:'',
    totalItems: '',
    itemsPerPage: 8,
    currentPage: 1,
    startDate: null,
    endDate: null,
    getAllData: false,
  };
  pageName = 'Report';
  items = [];
  isUpdateForm = false;
  selectedVendorId;
  selectedPackageId;
  selectedProductId;
  selectedVendorName;
  selectedPackageName;
  selectedProductName;
  updateFieldId;
  showTable = false;
  params;
  warehouseData;
  allImportTrans = [];
  allVendors = [];
  allPackageType = [];
  allProduct = [];
  allFaultyProduct = [];
  actions;
  importForm: FormGroup;
  importDetailsForm: FormGroup;
  productData;
  faultyProductData;
  submitted = false;
  showNewVendorModal = false;
  displayFaulty = false;
  showTransactionDetails = false;
  selectedFiles;
  updateDetails: any;
  showConfirmModal = false;
  itemToDelete: any;
  reqBody: any;
  showContent = true;
  selectedVendor;
  selectedProduct;
  confirmModalObject = {
    bodyText: 'Are you sure want to delete this import entry?',
    headerText: 'Delete import',
    leftBtnText: 'Cancel',
    rightBtnText: 'Delete',
  };
  constructor(
    private formBuilder: FormBuilder,
    private warehouseService: WarehouseService,
    private router: Router,
  ) { }

  async ngOnInit() {
    const temp = await localStorage.getItem('WarehouseData');
    console.log('warehouse data in vendor', this.warehouseData);
    if (temp == 'undefined') {
      console.log('in side');
      localStorage.clear();
      this.router.navigate(['login/warehouse-login']);
      return;
    }
    this.warehouseData = JSON.parse(temp);
    this.getImportTransactions();
    this.getAllVendors();
    this.getProducts();
    this.formValidation();
    // this.paginationObject = {
    //   filterBy: '',
    //   searchData: '',
    //   totalItems: 20,
    //   itemsPerPage: 5,
    //   currentPage: 1
    // };

    this.actions = {
      isDelete: true,
      isEdit: false,
      isDetailsView: true
    };
  }

  get f() { return this.importForm.controls; }

  formValidation() {
    this.importForm = this.formBuilder.group({
      product_1: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      quantity_1: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      faulty_1: new FormControl(
        '',
      ),
      faultyQuantity_1: new FormControl(
        '',
      ),
      packageType: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      vendorId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      messageBy: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      dONumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      vehicleNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      driverContactNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
    });

    this.importDetailsForm = this.formBuilder.group({
      product_1: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      quantity_1: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      faulty_1: new FormControl(
        '',
      ),
      faultyQuantity_1: new FormControl(
        '',
      ),
      packageType: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      vendorId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      messageBy: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      dONumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      vehicleNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      driverContactNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
    });
  }

  async getAllVendors() {
    const params = {
      warehouseId: this.warehouseData._id,
      vendorId: ''
    };
    await this.warehouseService.getVendor$(WebURL.GET_VENDOR, params).subscribe({
      next: (results) => {
        console.log('get vendor result', results);
        if (results.data.result.length > 0) {
          this.allVendors = results.data.result;
        } else {
          PopUpState$.next({
            state: 'error',
            message: 'You do not have any vendor mapped with you. Please add vendor first'
          });
          this.allVendors = [];
        }
      }, error: error => {
        console.log('error in get vendor', error);
        PopUpState$.next({
          state: 'error',
          message: error.error.message ? error.error.message : 'You do not have any vendor mapped with you. Please add vendor first'
        });
        this.allVendors = [];
      }
    });
  }

  async getImportTransactions() {
    // this.paginationObject.itemsPerPage = 8;
    this.getProducts();
    LoaderState$.next(true);
    console.log('this.selectedVendor, this.selectedProduct', this.selectedVendor, this.selectedProduct);
    const params = {
      transactionId: '',
      warehouseId: this.warehouseData._id,
      vendorId: (this.selectedVendor != 'Select' && this.selectedVendor != undefined) ? this.selectedVendor : '',
      productId: (this.selectedProduct != 'Select' && this.selectedProduct != undefined) ? this.selectedProduct : '',
      paginationObject: JSON.stringify(this.paginationObject)
    };
    console.log('getImportTransactions params', params);
    await this.warehouseService.getTransaction$(WebURL.GET_TRANSACTION, params).subscribe({
      next: async(results) => {
        console.log('get import trans result', results);
        this.items = [];
        if (results.data.result.length > 0) {
          this.allImportTrans = results.data.result;
          if (this.paginationObject.getAllData) {
            this.paginationObject.getAllData = false;
            results.data.result.forEach( async (element) => {
              element.productDetails.forEach( async (prod) => {
                console.log('am here =====================>>>>>>>>', prod.totalStorage);
                if (this.selectedProduct != 'Select' && this.selectedProduct != undefined) {
                  if (prod._id === this.selectedProduct) {
                    const importObj = {
                      id: element._id,
                      'Product Name': prod.name,
                      Quantity: prod.quantity,
                      'Balance Qty': prod.totalStorage ? prod.totalStorage : 0,
                      Vendor: element.vendor.name,
                      DONumber: element.DONumber,
                      VehicleNumber: element.vehicleNumber,
                      Date: new Date(element.createdAt).toLocaleDateString(),
                      ActionType: element.actionType == "import" ? "Inward" : "Outward",
                    };
                    this.items.push(importObj);
                  }
                } else {
                  const importObj = {
                    id: element._id,
                    'Product Name': prod.name,
                    Quantity: prod.quantity,
                    'Balance Qty': prod.totalStorage ? prod.totalStorage : 0,
                    Vendor: element.vendor.name,
                    DONumber: element.DONumber,
                    VehicleNumber: element.vehicleNumber,
                    Date: new Date(element.createdAt).toLocaleDateString(),
                    ActionType: element.actionType == "import" ? "Inward" : "Outward",
                  };
                  this.items.push(importObj);
                }
              });
            });
            console.log('am here final', this.items);
            LoaderState$.next(false);
            await this.warehouseService.setReportData(this.items);
            this.router.navigate(['warehouse/warehouse-report-template']);
          } else {
            results.data.result.forEach( async (element) => {
              element.productDetails.forEach( async (prod) => {
                console.log('am here');
                if (this.selectedProduct != 'Select' && this.selectedProduct != undefined) {
                  if (prod._id === this.selectedProduct) {
                    const importObj = {
                      id: element._id,
                      'Product Name': prod.name,
                      Quantity: prod.quantity,
                      Vendor: element.vendor.name,
                      DONumber: element.DONumber,
                      VehicleNumber: element.vehicleNumber,
                      Date: new Date(element.createdAt).toLocaleDateString(),
                      ActionType: element.actionType == "import" ? "Inward" : "Outward",
                      'Balance Qty': prod.totalStorage ? prod.totalStorage : 0,
                    };
                    this.items.push(importObj);
                  }
                } else {
                  const importObj = {
                    id: element._id,
                    'Product Name': prod.name,
                    Quantity: prod.quantity,
                    Vendor: element.vendor.name,
                    DONumber: element.DONumber,
                    VehicleNumber: element.vehicleNumber,
                    Date: new Date(element.createdAt).toLocaleDateString(),
                    ActionType: element.actionType == "import" ? "Inward" : "Outward",
                    'Balance Qty': prod.totalStorage ? prod.totalStorage : 0,
                  };
                  this.items.push(importObj);
                }
              });
            });
            console.log('am here final', this.items);
            LoaderState$.next(false);
            if (this.items.length > 8) {
              this.paginationObject.totalItems = (this.items.length - 8) + results.data.count;
            } else {
              this.paginationObject.totalItems = results.data.count;
            }
            // this.paginationObject.itemsPerPage = (this.items.length < 8) ? 8 : this.items.length;
            console.log('final table', this.paginationObject.totalItems);
            this.showTable = true;
          }
        } else {
          LoaderState$.next(false);
          this.items = [];
          this.showTable = false;
        }
      }, error: error => {
        LoaderState$.next(false);
        if (error.status === 404) {
          PopUpState$.next({
            state: 'error',
            message: error.error.message ? error.error.message : 'No past transaction'
          });
        }
        console.log('error in get transaction', error);
      }
    });
  }

  async getProductName(productData) {
    let tagStr = '';
    console.log('tagStr 111', Array.isArray(productData));
    if (!Array.isArray(productData)) {
      tagStr = tagStr + ' ' + productData.name + ' (' + productData.quantity + ')';
    } else {
      for (const element of productData) {
        if (tagStr !== '') {
          tagStr = tagStr + ', ';
        }
        console.log('in loop get name');
        tagStr = tagStr + ' ' + element.name + ' (' + element.quantity + ')'; 
      }
    }
    console.log('tagStr', tagStr);
    return tagStr;
  }

  changePage(event) {
    console.log('event on change page', event);
    this.paginationObject.currentPage = event;
    this.getImportTransactions();
    // this.paginationObject.currentPage = event;
  }

  disableDetailsModal() {
    this.showTransactionDetails = false;
    this.displayFaulty = false;
  }

  showUpdateModal(event) {
    console.log('in show details view', event);
    const index = this.allImportTrans.findIndex(ind => ind._id === event.id);
    this.updateDetails = this.allImportTrans[index];
    console.log('this.updateDetails -->', this.updateDetails);
    if (this.updateDetails.isFaulty) {
      this.displayFaulty = true;
    }
    this.showTransactionDetails = true;
  }

  showVendorModal() {
    console.log('called');
    // this.productSelectedItems = [];
    // this.vendorSelectedItems = [];
    // this.packageSelectedItems = [];
    this.importForm.controls.messageBy.setValue('');
    // this.importForm.controls.quantity.setValue('');
    this.importForm.controls.dONumber.setValue('');
    this.importForm.controls.vehicleNumber.setValue('');
    this.importForm.controls.driverContactNumber.setValue('');
    this.showNewVendorModal = true;
  }

  disableVendorModal() {
    this.showNewVendorModal = false;
    this.isUpdateForm = false;
    this.importForm.controls.vendorId.setValue('');
    this.importForm.controls.packageType.setValue('');
    this.importForm.controls.dONumber.setValue('');
    this.importForm.controls.vehicleNumber.setValue('');
    this.importForm.controls.messageBy.setValue('');
    this.importForm.controls.driverContactNumber.setValue('');
    this.displayFaulty = false;
    this.submitted = false;
  }

  getReport() {
    // console.log(this.content.nativeElement, 'html');
    // const doc = new jsPDF();
    // doc.autoTable({ html: this.content.nativeElement });
    // doc.save('table.pdf');
    this.paginationObject.getAllData = true;
    this.getImportTransactions();
  }

  dateChange(event) {
    console.log('date change event', event);
    console.log('start date', new Date(event.start._d));
    console.log('end date', new Date(event.end._d));
    this.paginationObject.startDate = new Date(event.start._d);
    this.paginationObject.endDate = new Date(event.end._d);
    console.log('this.paginationObject', this.paginationObject);
    this.getImportTransactions();
  }

  async getProducts() {
    this.params = {
      productId: '',
      warehouseId: this.warehouseData._id,
      packageTypeId: '',
      vendorId: (this.selectedVendor != 'Select' && this.selectedVendor != undefined) ? this.selectedVendor : '',
    };
    await this.warehouseService.getProducts$(WebURL.GET_PRODUCT, this.params).subscribe({
      next: (results) => {
        console.log('get product result', results);
        if (results.data.result.length > 0) {
          this.productData = results.data.result;
        }
      }, error: error => {
        console.log('error in get package', error);
      }
    });
  }

}

