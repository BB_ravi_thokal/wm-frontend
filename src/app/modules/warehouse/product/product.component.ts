import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaderState$, PopUpState$ } from '../../../app.component';
import { WarehouseService } from '../warehouse.service';
import { Router } from '@angular/router';
import * as WebURL from '../../../config/web-urls';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['../../../app.component.scss', './product.component.scss']
})
export class ProductComponent implements OnInit {
  paginationObject = {
    filterBy: '',
    searchData: '',
    totalItems: '',
    itemsPerPage: 8,
    currentPage: 1,
  };
  items;
  selectedVendorId;
  pageName = 'Product Details'
  selectedPackageId;
  selectedVendorName;
  selectedPackageName;
  updateFieldId;
  showTable = false;
  params;
  warehouseData;
  allVendors = [];
  allPackageType = [];
  productForm: FormGroup;
  submitted = false;
  showNewVendorModal = false;
  isUpdateForm = false;
  modalHeader = 'Add New Product';
  constructor(
    private formBuilder: FormBuilder,
    private warehouseService: WarehouseService,
    private router: Router,
  ) { }

  async ngOnInit() {
    const temp = await localStorage.getItem('WarehouseData');
    console.log('warehouse data in vendor', this.warehouseData);
    if (temp == 'undefined') {
      console.log('in side');
      localStorage.clear();
      this.router.navigate(['login/warehouse-login']);
      return;
    }
    this.warehouseData = JSON.parse(temp);
    this.getProducts();
    this.getAllVendors();
    this.getAllPackageType();
    this.formValidation();

  }

  get f() { return this.productForm.controls; }

  formValidation() {
    this.productForm = this.formBuilder.group({
      productName: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      vendorId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      packagingType: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      rate: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.min(0.1)
        ])
      ),
    });
  }

  async getAllVendors() {
    const params = {
      warehouseId: this.warehouseData._id,
      vendorId: ''
    };
    await this.warehouseService.getVendor$(WebURL.GET_VENDOR, params).subscribe({
      next: (results) => {
        console.log('get vendor result', results);
        if (results.data.result.length > 0) {
          this.allVendors = results.data.result;
        }
      }, error: error => {
        console.log('error in get vendor', error);
      }
    });
  }

  async getAllPackageType() {
    const params = {
      warehouseId: this.warehouseData._id,
      typeId: ''
    };
    await this.warehouseService.getPackagingType$(WebURL.GET_PACKAGING_TYPE, params).subscribe({
      next: (results) => {
        console.log('get package result', results);
        if (results.data.length > 0) {
          this.allPackageType = results.data;
        }
      }, error: error => {
        console.log('error in get package', error);
      }
    });
  }

  async getProducts() {
    LoaderState$.next(true);
    this.params = {
      productId: '',
      warehouseId: this.warehouseData._id,
      packageTypeId: '',
      vendorId: '',
      paginationObject: JSON.stringify(this.paginationObject)
    };
    await this.warehouseService.getProducts$(WebURL.GET_PRODUCT, this.params).subscribe({
      next: (results) => {
        LoaderState$.next(false);
        console.log('get product result', results);
        this.items = [];
        if (results.data.result.length > 0) {
          results.data.result.forEach(element => {
            const packageObj = {
              id: element._id,
              Name: element.name,
              'Pack Type': element.packagingType.packagingType,
              Storage: element.storage.quantity,
              Vendor: element.vendor.name,
              Rate: element.rate,
            };
            this.items.push(packageObj);
          });
          this.paginationObject.totalItems = results.data.count;
          console.log('finally table', this.items);
          this.showTable = true;
        } else {
          this.items = [];
          this.showTable = false;
        }
      }, error: error => {
        LoaderState$.next(false);
        if (error.status === 404) {
          this.showTable = false;
          this.items = [];
        }
        console.log('error in get package', error);
      }
    });
  }

  changePage(event) {
    this.paginationObject.currentPage = event;
    this.getProducts();
  }

  async deleteProduct(event) {
    console.log('Product remove called', event);
    const packageIndex = this.allPackageType.findIndex(ind => ind.packagingType === event['Pack Type']);
    const vendorIndex = this.allVendors.findIndex(ind => ind.name === event.Vendor);
    this.selectedPackageName = event['Pack Type'];
    this.selectedVendorName = event.Vendor;
    this.selectedPackageId = this.allPackageType[packageIndex]._id;
    this.selectedVendorId = this.allVendors[vendorIndex]._id;
    const reqBody = {
      _id: event.id,
      warehouseId: this.warehouseData._id,
      name: event.Name,
      packagingTypeId: this.selectedPackageId,
      rate: event.Rate,
      vendor: this.selectedVendorId,
      isDeleted: true
    };
    console.log('reqBody', reqBody);
    await this.warehouseService.deleteProduct$(WebURL.DELETE_PRODUCT, reqBody).subscribe({
      next: (results) => {
        console.log('delete package result', results);
        // PopUpState$.next({
        //   state: 'success',
        //   message: results.data.message ? results.data.message : 'Product deleted successfully'
        // });
        this.getProducts();
      }, error: error => {
        console.log('error in delete package', error);
        PopUpState$.next({
          state: 'error',
          message: error.error.message ? error.error.message : 'Not able to delete at the moment. Please try again later'
        });
      }
    });
  }

  showUpdateModal(event) {
    console.log('show product update called', event);
    this.isUpdateForm = true;
    this.updateFieldId = event.id;
    const packageIndex = this.allPackageType.findIndex(ind => ind.packagingType === event['Pack Type']);
    const vendorIndex = this.allVendors.findIndex(ind => ind.name === event.Vendor);
    this.selectedPackageName = event['Pack Type'];
    this.selectedVendorName = event.Vendor;
    this.selectedPackageId = this.allPackageType[packageIndex]._id;
    this.selectedVendorId = this.allVendors[vendorIndex]._id;
    console.log('this.selectedVendor', this.selectedVendorId, this.selectedVendorName);
    console.log('this.selectedPackage', this.selectedPackageId, this.selectedPackageName);
    this.productForm.controls.productName.setValue(event.Name);
    this.productForm.controls.vendorId.setValue(this.selectedVendorId);
    this.productForm.controls.packagingType.setValue(this.selectedPackageId);
    this.productForm.controls.rate.setValue(event.Rate);
    // this.productForm.controls.formHeading.setValue("Edit Product Details");
    this.showNewVendorModal = true;
    this.modalHeader = 'Edit Product'
  }

  async editProduct() {
    if (this.productForm.valid) {
      const reqBody = {
        _id: this.updateFieldId,
        warehouseId: this.warehouseData._id,
        name: this.productForm.get('productName').value,
        packagingTypeId: this.productForm.get('packagingType').value,
        rate: this.productForm.get('rate').value,
        vendor: this.productForm.get('vendorId').value
      };
      console.log('reqBody', reqBody);
      await this.warehouseService.updateProduct$(WebURL.UPDATE_PRODUCT, reqBody).subscribe({
        next: (results) => {
          console.log('update product result', results);
          this.showTable = false;
          this.showNewVendorModal = false;
          this.isUpdateForm = false;
          // PopUpState$.next({
          //   state: 'success',
          //   message: results.data.message ? results.data.message : 'Product updated successfully'
          // });
          this.getProducts();
        }, error: error => {
          console.log('error in update package', error);
          PopUpState$.next({
            state: 'error',
            message: error.error.message ? error.error.message : 'Not able to update at the moment. Please try again later'
          });
        }
      });
    } else {
      LoaderState$.next(false);
      return;
    }
  }

  showVendorModal() {
    console.log('called');
    this.productForm.controls.productName.setValue('');
    this.productForm.controls.vendorId.setValue('');
    this.productForm.controls.packagingType.setValue('');
    this.productForm.controls.rate.setValue('');
    this.modalHeader = 'Add new product';
    this.showNewVendorModal = true;
  }

  disableVendorModal() {
    this.showNewVendorModal = false;
    this.submitted = false;
    this.isUpdateForm = false;
  }

  async addProduct() {
    this.submitted = true;
    if (this.productForm.valid) {
      LoaderState$.next(true);
      const reqBody = {
        warehouseId: this.warehouseData._id,
        packagingTypeId: this.productForm.get('packagingType').value,
        name: this.productForm.get('productName').value,
        rate: this.productForm.get('rate').value,
        vendor: this.productForm.get('vendorId').value,
      };
      console.log('req body for create prod', reqBody);
      await this.warehouseService.createProduct$(WebURL.CREATE_PRODUCT, reqBody).subscribe({
        next: (results) => {
          LoaderState$.next(false);
          console.log('create product result', results);
          this.showTable = false;
          this.showNewVendorModal = false;
          this.submitted = false;
          // PopUpState$.next({
          //   state: 'success',
          //   message: results.data.message ? results.data.message : 'Product addedd successfully'
          // });
          this.getProducts();
        }, error: error => {
          LoaderState$.next(false);
          console.log('error in create vendor', error);
          PopUpState$.next({
            state: 'error',
            message: error.error.message ? error.error.message : 'Not able to create at the moment. Please try again later'
          });
        }
      });
    } else {
      LoaderState$.next(false);
      return;
    }
  }

}

