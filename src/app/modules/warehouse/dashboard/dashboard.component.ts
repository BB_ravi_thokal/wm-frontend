import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WarehouseService } from '../warehouse.service';
import { StoreService } from '../../../store/store.service';
import * as WebURL from '../../../config/web-urls';
import { LoaderState$, PopUpState$ } from '../../../app.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  reportData;
  adminCards;
  warehouseData;
  params;
  pageName = 'Dashboard'
  isActive = true;
  paginationObject = {
    filterBy:'',
    searchData:'',
    totalItems: '',
    itemsPerPage: 5,
    currentPage: 1,
    startDate: null,
    endDate: null
  };
  constructor(
    private router: Router,
    private warehouseService: WarehouseService,
    private storeService: StoreService
  ) {
    this.warehouseData = JSON.parse(localStorage.getItem('WarehouseData'));
  }

  async ngOnInit() {
    LoaderState$.next(false);
    this.getWarehouseDetails();
    this.adminCards = [
      {
        name: 'Import',
        route: 'warehouse/warehouse-import'
      },
      {
        name: 'Export',
        route: 'warehouse/warehouse-export'
      },
      {
        name: 'Vendor',
        route: 'warehouse/warehouse-vendor'
      },
      {
        name: 'Product',
        route: 'warehouse/warehouse-product'
      },
      {
        name: 'Report',
        route: 'warehouse/warehouse-report'
      },
    ];
    //this.reportData = await this.warehouseService.getReportData();
    //console.log('this.reportData', this.reportData);
  }

  async getImportTransactions() {
    const params = {
      transactionId: '',
      warehouseId: this.warehouseData._id,
      vendorId: '',
      productId: '',
      paginationObject: JSON.stringify(this.paginationObject)
    };
    console.log('getImportTransactions params', params);
    await this.warehouseService.getTransaction$(WebURL.GET_TRANSACTION, params).subscribe({
      next: async(results) => {
        console.log('get import trans result', results);
        this.reportData = [];
        if (results.data.result.length > 0) {
          results.data.result.forEach( async (element) => {
            element.productDetails.forEach( async (prod) => {
              console.log('am here');
              const importObj = {
                id: element._id,
                'Product Name': prod.name,
                Quantity: prod.quantity,
                Vendor: element.vendor.name,
                DONumber: element.DONumber,
                VehicleNumber: element.vehicleNumber,
                Date: new Date(element.createdAt).toLocaleDateString(),
                ActionType: element.actionType == "import" ? "Inward" : "Outward",
                'Balance Qty': prod.totalStorage,
              };
              this.reportData.push(importObj);
            });
          });
        }
      }, error: error => {
        LoaderState$.next(false);
        console.log('error in get transaction', error);
      }
    });
  }

  gotoCard(route) {
    this.router.navigate([`${route}`]);
  }

  async getWarehouseDetails() {
    console.log('getWarehouseDetails called');
    const temp = await localStorage.getItem('WarehouseData');
    if (temp) {
      this.warehouseData = JSON.parse(temp);
      this.checkWarehouseActiveStatus();
    } else {
      localStorage.removeItem('access_token');
      this.router.navigate(['login/warehouse-login']);
    }
  }
  
  async checkWarehouseActiveStatus() {
    const params = {
      warehouseId: this.warehouseData._id
    }
    await this.warehouseService.checkWarehouseActiveStatus$(WebURL.CHECK_ACTIVE_STATUS, params).subscribe({
      next: (results) => {
        console.log('Warehouse Status', results);
        if (results.data.status === 'expired') {
          this.isActive = false;
          PopUpState$.next({
            state: 'error',
            message: 'Your subscription is over please do contact admin for renew'
          });
          localStorage.clear();
          this.router.navigate(['dw-home']);
          console.log('Warehouse Status change active', this.isActive);
        } 
        this.getImportTransactions();
      }, error: error => {
        console.log('Get warehouse status error', error);
      }
    });
  }

}
