import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { WarehouseService } from '../warehouse.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-report-template',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportTemplateComponent implements OnInit {

  reportData;
  warehouseData;

  constructor(
    private warehouseService: WarehouseService,
    private router: Router,
  ) {
    this.warehouseData = JSON.parse(localStorage.getItem('WarehouseData'));
  }

  async ngOnInit() {
    console.log('this.warehouseData', this.warehouseData);
    this.reportData = await this.warehouseService.getReportData();
    console.log('this.reportData', this.reportData);
    setTimeout(() => {
      console.log('in set time')
      this.download();
    }, 2000)
  }

  async download() {
    window.print();
    this.router.navigate(['warehouse/warehouse-report']);
  }
}
