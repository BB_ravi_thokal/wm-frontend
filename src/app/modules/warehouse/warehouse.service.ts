import { Inject, Injectable } from '@angular/core';
import * as WebURL from '../../config/web-urls';
import { Observable } from 'rxjs';
import { BASE_URL } from '../../config/app-config';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import * as Valid from '../../interface/warehouse.interface';

@Injectable({
    providedIn: 'root'
})
export class WarehouseService {
    reportData;
    constructor(
        private http: HttpClient,
    ) { }
    async setReportData(data) {
        this.reportData = data;
    }

    async getReportData() {
        return this.reportData;
    }
    // API related to Warehouse  -------- Started -----------------------------

    /**
     * @description method to get warehouse record based on the params
     * @param url endpoint for get warehouse API
     * @param params reference of auth table
     */
    checkWarehouseActiveStatus$(url, params): Observable<any> {
        console.log(url, 'url');
        console.log(BASE_URL, 'BASE_URL');
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        const httpParams = new HttpParams()
            .set('warehouseId', params.warehouseId);
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get<any>(BASE_URL + url, httpOptions);
    }

    /**
     * @description method to get warehouse record based on the params
     * @param url endpoint for get warehouse API
     * @param params reference of auth table
     */
    getWarehouse$(url, params): Observable<any> {
        console.log(url, 'url');
        console.log(BASE_URL, 'BASE_URL');
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        const httpParams = new HttpParams()
            .set('warehouseId', params.warehouseId);
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: new HttpParams(params)
            };
        }
        return this.http.get<any>(BASE_URL + url, httpOptions);
    }

    // API related to Vendor  -------- Ended -----------------------------


    // API related to Vendor  -------- Started -----------------------------
    getVendor$(url, params): Observable<any> {
        console.log(url, 'url');
        console.log(BASE_URL, 'BASE_URL');
        console.log('params', params);
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        let httpParams;
        if (params.paginationObject) {
            httpParams = new HttpParams()
                .set('warehouseId', params.warehouseId)
                .set('vendorId', params.vendorId)
                .set('paginationObject', params.paginationObject);
        } else {
            httpParams = new HttpParams()
                .set('warehouseId', params.warehouseId)
                .set('vendorId', params.vendorId);
        }
        
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get<any>(BASE_URL + url, httpOptions);
    }

    createVendor$(url, requestBody: Valid.Vendor): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        return this.http.post<any>(BASE_URL + url, requestBody, httpOptions);
    }

    updateVendor$(url, requestBody: Valid.UpdateVendor): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        return this.http.put<any>(BASE_URL + url, requestBody, httpOptions);
    }

    deleteVendor$(url, requestBody: Valid.UpdateVendor): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        return this.http.put<any>(BASE_URL + url, requestBody, httpOptions);
    }
    // API related to Vendor  -------- Ended -----------------------------

    // API related to PackagingType  -------- Started -----------------------------
    getPackagingType$(url, params): Observable<any> {
        console.log(url, 'url');
        console.log(BASE_URL, 'BASE_URL');
        console.log('params', params);
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        const httpParams = new HttpParams()
            .set('typeId', params.typeId)
            .set('warehouseId', params.warehouseId);
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get<any>(BASE_URL + url, httpOptions);
    }

    createPackagingType$(url, requestBody: Valid.PackagingType): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        return this.http.post<any>(BASE_URL + url, requestBody, httpOptions);
    }

    updatePackagingType$(url, requestBody: Valid.PackagingType): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        return this.http.put<any>(BASE_URL + url, requestBody, httpOptions);
    }

    deletePackagingType$(url, requestBody: Valid.PackagingType): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        return this.http.put<any>(BASE_URL + url, requestBody, httpOptions);
    }
    // API related to Packaging Type  -------- Ended -----------------------------

    // API related to Product  -------- Started -----------------------------
    getProducts$(url, params): Observable<any> {
        console.log(url, 'url');
        console.log(BASE_URL, 'BASE_URL');
        console.log('params', params);
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        let httpParams;
        if (params.paginationObject) {
            httpParams = new HttpParams()
            .set('productId', params.productId)
            .set('warehouseId', params.warehouseId)
            .set('packageTypeId', params.packageTypeId)
            .set('vendorId', params.vendorId)
            .set('paginationObject', params.paginationObject);
        } else {
            httpParams = new HttpParams()
            .set('productId', params.productId)
            .set('warehouseId', params.warehouseId)
            .set('packageTypeId', params.packageTypeId)
            .set('vendorId', params.vendorId);
        }
        
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get<any>(BASE_URL + url, httpOptions);
    }

    createProduct$(url, requestBody: Valid.Product): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        return this.http.post<any>(BASE_URL + url, requestBody, httpOptions);
    }

    updateProduct$(url, requestBody: Valid.Product): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        return this.http.put<any>(BASE_URL + url, requestBody, httpOptions);
    }

    deleteProduct$(url, requestBody: Valid.Product): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        return this.http.put<any>(BASE_URL + url, requestBody, httpOptions);
    }
    // API related to Packaging Type  -------- Ended -----------------------------

    // API related to Product  -------- Started -----------------------------
    getTransaction$(url, params): Observable<any> {
        console.log(url, 'url');
        console.log(BASE_URL, 'BASE_URL');
        console.log('params', params);
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        const httpParams = new HttpParams()
            .set('transactionId', params.transactionId)
            .set('warehouseId', params.warehouseId)
            .set('vendorId', params.vendorId)
            .set('productId', params.productId)
            .set('paginationObject', params.paginationObject);
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get<any>(BASE_URL + url, httpOptions);
    }

    getImportTransaction$(url, params): Observable<any> {
        console.log(url, 'url');
        console.log(BASE_URL, 'BASE_URL');
        console.log('params', params);
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        const httpParams = new HttpParams()
            .set('transactionId', params.transactionId)
            .set('warehouseId', params.warehouseId)
            .set('vendorId', params.vendorId)
            .set('productId', params.productId)
            .set('paginationObject', params.paginationObject);
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get<any>(BASE_URL + url, httpOptions);
    }

    getExportTransaction$(url, params): Observable<any> {
        console.log(url, 'url');
        console.log(BASE_URL, 'BASE_URL');
        console.log('params', params);
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        const httpParams = new HttpParams()
            .set('transactionId', params.transactionId)
            .set('warehouseId', params.warehouseId)
            .set('vendorId', params.vendorId)
            .set('productId', params.productId)
            .set('paginationObject', params.paginationObject);
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get<any>(BASE_URL + url, httpOptions);
    }

    createTransaction$(url, requestBody): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        console.log('httpOptions', httpOptions);
        return this.http.post<any>(BASE_URL + url, requestBody, httpOptions);
    }

    deleteImportTransaction$(url, requestBody: Valid.Transaction): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        return this.http.put<any>(BASE_URL + url, requestBody, httpOptions);
    }

    deleteExportTransaction$(url, requestBody: Valid.Transaction): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        return this.http.put<any>(BASE_URL + url, requestBody, httpOptions);
    }
    // API related to Packaging Type  -------- Ended -----------------------------

    sendTransactionalEmail$(url, files): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    // 'Content-Type': 'multipart/form-data; boundary=something',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        console.log('httpOptions', httpOptions);
        return this.http.post<any>(BASE_URL + url, files, httpOptions);
    }

}
