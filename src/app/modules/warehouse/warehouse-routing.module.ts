import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { VendorComponent } from './vendor/vendor.component';
import { ImportComponent } from './import/import.component';
import { ExportComponent } from './export/export.component';
import { ProductComponent } from './product/product.component';
import { ReportComponent } from './report/report.component';
import { PackagingComponent } from './packagingType/packagingType.component';
import { ReportTemplateComponent } from './reportTemplate/report.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'warehouse-dashboard',
      },
      { path: 'warehouse-dashboard', component: DashboardComponent },
      { path: 'warehouse-import', component: ImportComponent },
      { path: 'warehouse-export', component: ExportComponent },
      { path: 'warehouse-vendor', component: VendorComponent },
      { path: 'warehouse-product', component: ProductComponent },
      { path: 'warehouse-report', component: ReportComponent },
      { path: 'warehouse-packaging-type', component: PackagingComponent },
      { path: 'warehouse-report-template', component: ReportTemplateComponent },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WarehouseRoutingModule {
}
