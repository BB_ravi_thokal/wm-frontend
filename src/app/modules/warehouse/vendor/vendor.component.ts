import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaderState$, PopUpState$ } from '../../../app.component';
import { WarehouseService } from '../warehouse.service';
import * as WebURL from '../../../config/web-urls';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['../../../app.component.scss', './vendor.component.scss']
})
export class VendorComponent implements OnInit {
  paginationObject = {
    filterBy: '',
    searchData: '',
    totalItems: '',
    itemsPerPage: 8,
    currentPage: 1,
  };
  warehouseData;
  updateFieldId;
  pageName = 'Vendor'
  showTable = false;
  items = [];
  actions;
  registerVendorForm: FormGroup;
  submitted = false;
  showNewVendorModal = false;
  isUpdateForm = false;
  params = {};
  constructor(
    private formBuilder: FormBuilder,
    private warehouseService: WarehouseService,
    private router: Router,
  ) { }

  async ngOnInit() {
    const temp = await localStorage.getItem('WarehouseData');
    console.log('temp --->', temp);
    if (temp == 'undefined') {
      console.log('in side');
      localStorage.clear();
      this.router.navigate(['login/warehouse-login']);
      return;
    }
    this.warehouseData = JSON.parse(temp);
    this.formValidation();
    this.getVendor();
    this.actions = {
      isDelete: true,
      isEdit: true
    };
  }

  get f() { return this.registerVendorForm.controls; }

  formValidation() {
    this.registerVendorForm = this.formBuilder.group({
      vendorName: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      vendorContactNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      vendorEmailId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
        ])
      ),
    });
  }

  async getVendor() {
    LoaderState$.next(true);
    this.params = {
      warehouseId: this.warehouseData._id,
      vendorId: '',
      paginationObject: JSON.stringify(this.paginationObject)
    };
    await this.warehouseService.getVendor$(WebURL.GET_VENDOR, this.params).subscribe({
      next: (results) => {
        LoaderState$.next(false);
        console.log('get vendor result', results);
        this.items = [];
        if (results.data.result.length > 0) {
          LoaderState$.next(false);
          results.data.result.forEach(element => {
            const vendorObj = {
              id: element._id,
              Name: element.name,
              EmailId: element.emailId,
              ContactNumber: element.contactNumber
            };
            this.items.push(vendorObj);
          });
          this.paginationObject.totalItems = results.data.count;
          this.showTable = true;
        } else {
          LoaderState$.next(false);
          this.items = [];
          this.showTable = false;
        }
      }, error: error => {
        LoaderState$.next(false);
        if (error.status === 404) {
          console.log('error in get vendor', error);
          this.items = [];
          this.showTable = false;
        }
      }
    });
  }

  changePage(event) {
    this.paginationObject.currentPage = event;
    this.getVendor();
  }

  async deleteVendor(event) {
    console.log('Vendor remove called', event);
    const reqBody = {
      name: event.Name,
      role: 'vendor',
      warehouse: this.warehouseData._id,
      contactNumber: event.ContactNumber,
      emailId: event.EmailId,
      _id: event.id,
      isDeleted: true
    };
    await this.warehouseService.deleteVendor$(WebURL.DELETE_VENDOR, reqBody).subscribe({
      next: (results) => {
        console.log('delete vendor result', results);
        this.showNewVendorModal = false;
        if (results.data) {
          this.getVendor();
        }
      }, error: error => {
        console.log('error in delete vendor', error);
        PopUpState$.next({
          state: 'error',
          message: error.error.message ? error.error.message : 'Not able to delete at the moment. Please try again later'
        });
      }
    });
  }

  async updateVendor(data) {
    if (this.registerVendorForm.valid) {
      LoaderState$.next(true);
      this.submitted = true;
      const reqBody = {
        name: this.registerVendorForm.get('vendorName').value,
        role: 'vendor',
        warehouse: this.warehouseData._id,
        contactNumber: this.registerVendorForm.get('vendorContactNumber').value,
        emailId: this.registerVendorForm.get('vendorEmailId').value,
        _id: this.updateFieldId
      };
      console.log('reqBody', reqBody);
      await this.warehouseService.updateVendor$(WebURL.UPDATE_VENDOR, reqBody).subscribe({
        next: (results) => {
          LoaderState$.next(false);
          console.log('update vendor result', results);
          this.showNewVendorModal = false;
          if (results.data) {
            this.getVendor();
          }
        }, error: error => {
          console.log('error in update vendor', error);
          LoaderState$.next(false);
          PopUpState$.next({
            state: 'error',
            message: error.error.message ? error.error.message : 'Not able to update at the moment. Please try again later'
          });
        }
      });
    } else {
      LoaderState$.next(false);
      return;
    }
  }

  showEditModal(event) {
    console.log('Vendor edit called', event);
    this.isUpdateForm = true;
    this.submitted = false;
    this.updateFieldId = event.id;
    this.registerVendorForm.controls.vendorName.setValue(event.Name);
    this.registerVendorForm.controls.vendorContactNumber.setValue(event.ContactNumber);
    this.registerVendorForm.controls.vendorEmailId.setValue(event.EmailId);
    this.showNewVendorModal = true;
  }

  showVendorModal() {
    console.log('called');
    this.isUpdateForm = false;
    this.submitted = false;
    this.showNewVendorModal = true;
    this.registerVendorForm.controls.vendorName.setValue('');
    this.registerVendorForm.controls.vendorContactNumber.setValue('');
    this.registerVendorForm.controls.vendorEmailId.setValue('');
  }

  disableVendorModal() {
    this.showNewVendorModal = false;
    this.isUpdateForm = true;
  }

  async addVendor() {
    this.isUpdateForm = false;
    console.log('this.loginForm.valid', this.registerVendorForm.get('vendorName').value);
    this.submitted = true;
    if (this.registerVendorForm.valid) {
      LoaderState$.next(true);
      this.submitted = false;
      const reqBody = {
        name: this.registerVendorForm.get('vendorName').value,
        role: 'vendor',
        warehouseId: this.warehouseData._id,
        contactNumber: this.registerVendorForm.get('vendorContactNumber').value,
        emailId: this.registerVendorForm.get('vendorEmailId').value
      };
      await this.warehouseService.createVendor$(WebURL.CREATE_VENDOR, reqBody).subscribe({
        next: (results) => {
          LoaderState$.next(false);
          console.log('create vendor result', results);
          this.showNewVendorModal = false;
          this.submitted = false;
          this.getVendor();
          // if (results.data) {
          //   const vendorObj = {
          //     id: results.data._id,
          //     Name: results.data.name,
          //     EmailId: results.data.emailId,
          //     ContactNumber: results.data.contactNumber
          //   };
          //   this.items.push(vendorObj);
          //   this.showTable = true;
          // }
        }, error: error => {
          LoaderState$.next(false);
          console.log('error in create vendor', error);
          PopUpState$.next({
            state: 'error',
            message: error.error.message ? error.error.message : 'Not able to create at the moment. Please try again later'
          });
        }
      });
    } else {
      LoaderState$.next(false);
      return;
    }
  }

}
