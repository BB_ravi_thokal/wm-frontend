import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaderState$, PopUpState$ } from '../../../app.component';
import { WarehouseService } from '../warehouse.service';
import * as WebURL from '../../../config/web-urls';
import { Router } from '@angular/router';
import { ERROR_COMPONENT_TYPE } from '@angular/compiler';

@Component({
  selector: 'app-packaging-type',
  templateUrl: './packagingType.component.html',
  styleUrls: ['../../../app.component.scss', './packagingType.component.scss']
})
export class PackagingComponent implements OnInit {
  paginationObject;
  items = [];
  updateFieldId;
  showTable = false;
  pageName = 'Add Packaging Type';
  warehouseData;
  params;
  productForm: FormGroup;
  submitted = false;
  showNewVendorModal = false;
  isUpdateForm = false;
  constructor(
    private formBuilder: FormBuilder,
    private warehouseService: WarehouseService,
    private router: Router,
  ) { }

  async ngOnInit() {
    const temp = await localStorage.getItem('WarehouseData');
    console.log('warehouse data in vendor', this.warehouseData);
    if (temp == 'undefined') {
      console.log('in side');
      localStorage.clear();
      this.router.navigate(['login/warehouse-login']);
      return;
    }
    this.warehouseData = JSON.parse(temp);
    this.formValidation();
    this.getPackagingType();
    this.paginationObject = {
      filterBy: '',
      searchData: '',
      totalItems: '',
      itemsPerPage: 5,
      currentPage: 1
    };

  }

  get f() { return this.productForm.controls; }

  formValidation() {
    this.productForm = this.formBuilder.group({
      packagingType: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
    });
  }

  async getPackagingType() {
    LoaderState$.next(true);
    this.params = {
      warehouseId: this.warehouseData._id,
      typeId: ''
    };
    await this.warehouseService.getPackagingType$(WebURL.GET_PACKAGING_TYPE, this.params).subscribe({
      next: (results) => {
        LoaderState$.next(false);
        console.log('get package result', results);
        this.items = [];
        if (results.data.length > 0) {
          results.data.forEach(element => {
            const packageObj = {
              id: element._id,
              Type: element.packagingType
            };
            this.items.push(packageObj);
          });
          this.showTable = true;
        } else {
          this.items = [];
          this.showTable = false;
        }
      }, error: error => {
        LoaderState$.next(false);
        if (error.status === 404) {
          this.showTable = false;
          this.items = [];
        }
        console.log('error in get package', error);
      }
    });
  }

  changePage(event) {
    this.paginationObject.currentPage = event;
  }

  async deletePackage(event) {
    console.log('Package remove called');
    const reqBody = {
      _id: event.id,
      warehouseId: this.warehouseData._id,
      packagingType: event.Type,
      isDeleted: true
    };
    await this.warehouseService.deletePackagingType$(WebURL.DELETE_PACKAGING_TYPE, reqBody).subscribe({
      next: (results) => {
        console.log('delete Package result', results);
        this.showNewVendorModal = false;
        if (results.data) {
          this.getPackagingType();
        }
      }, error: error => {
        console.log('error in delete package', error);
        PopUpState$.next({
          state: 'error',
          message: error.error.message ? error.error.message : 'Not able to delete at the moment. Please try again later'
        });
      }
    });
  }

  showUpdateModal(event) {
    console.log('show update modal', event);
    this.updateFieldId = event.id;
    this.isUpdateForm = true;
    this.submitted = false;
    this.productForm.controls.packagingType.setValue(event.Type);
    this.showNewVendorModal = true;
  }

  async updatePackage() {
    if (this.productForm.valid) {
      const reqBody = {
        _id: this.updateFieldId,
        warehouseId: this.warehouseData._id,
        packagingType: this.productForm.get('packagingType').value
      };
      console.log('reqBody', reqBody);
      await this.warehouseService.updatePackagingType$(WebURL.UPDATE_PACKAGING_TYPE, reqBody).subscribe({
        next: (results) => {
          console.log('update package result', results);
          this.updateFieldId = '';
          this.showNewVendorModal = false;
          if (results.data) {
            this.getPackagingType();
          }
        }, error: error => {
          console.log('error in update package', error);
          PopUpState$.next({
            state: 'error',
            message: error.error.message ? error.error.message : 'Not able to update at the moment. Please try again later'
          });
        }
      });
    } else {
      LoaderState$.next(false);
      return;
    }
  }

  showVendorModal() {
    console.log('called');
    this.productForm.controls.packagingType.setValue('');
    this.submitted = false;
    this.isUpdateForm = false;
    this.showNewVendorModal = true;
  }

  disableVendorModal() {
    this.showNewVendorModal = false;
    this.isUpdateForm = false;
  }

  async addPackagingType() {
    if (this.productForm.valid) {
      LoaderState$.next(true);
      this.submitted = true;
      const reqBody = {
        warehouseId: this.warehouseData._id,
        packagingType: this.productForm.get('packagingType').value,
      };
      await this.warehouseService.createPackagingType$(WebURL.CREATE_PACKAGING_TYPE, reqBody).subscribe({
        next: (results) => {
          LoaderState$.next(false);
          console.log('create package result', results);
          this.showNewVendorModal = false;
          this.submitted = false;
          if (results.data) {
            const packageObj = {
              id: results.data._id,
              Type: results.data.packagingType,
            };
            this.items.push(packageObj);
            this.showTable = true;
          }
        }, error: error => {
          LoaderState$.next(false);
          console.log('error in create vendor', error);
          PopUpState$.next({
            state: 'error',
            message: error.error.message ? error.error.message : 'Not able to create at the moment. Please try again later'
          });
        }
      });
    } else {
      LoaderState$.next(false);
      return;
    }
  }

}

