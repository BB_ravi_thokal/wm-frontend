import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaderState$, PopUpState$ } from '../../../app.component';
import { WarehouseService } from '../warehouse.service';
import { Router } from '@angular/router';
import * as WebURL from '../../../config/web-urls';
import { formatDate } from '@angular/common' 

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['../../../app.component.scss', './import.component.scss']
})
export class ImportComponent implements OnInit {
  @ViewChild('content') content: ElementRef;
  paginationObject = {
    filterBy: '',
    searchData: '',
    totalItems: '',
    itemsPerPage: 8,
    currentPage: 1,
    startDate: null,
    endDate: null
  };
  pageName = 'Inward';
  invoiceHeader = 'STORAGE RECIEPT';
  items = [];
  isUpdateForm = false;
  selectedVendorId;
  selectedPackageId;
  selectedProductId;
  selectedVendorName;
  selectedPackageName;
  selectedProductName;
  updateFieldId;
  showTable = false;
  params;
  warehouseData;
  allImportTrans = [];
  allVendors = [];
  allPackageType = [];
  allProduct = [];
  productArray = [];
  vendorArray = [];
  allFaultyProduct = [];
  actions;
  importForm: FormGroup;
  importDetailsForm: FormGroup;
  productData;
  faultyProductData;
  submitted = false;
  showNewVendorModal = false;
  displayFaulty = false;
  showTransactionDetails = false;
  selectedFiles = [];
  updateDetails: any;
  showConfirmModal = false;
  itemToDelete: any;
  reqBody: any;
  selectedProduct;
  selectedVendor;
  showButton = true;
  maxDate;
  confirmModalObject = {
    bodyText: 'Are you sure want to delete this import entry?',
    headerText: 'Delete import',
    leftBtnText: 'Cancel',
    rightBtnText: 'Delete',
  };
  constructor(
    private formBuilder: FormBuilder,
    private warehouseService: WarehouseService,
    private router: Router,
  ) { }

  async ngOnInit() {
    
    const year = new Date().getFullYear();
    const month = new Date().getMonth() +  1;
    const firstDate = new Date(year, month, 1);
    console.log('firstDate --->', firstDate);
    const temp = await localStorage.getItem('WarehouseData');
    console.log('warehouse data in vendor', this.warehouseData);
    if (temp == 'undefined') {
      console.log('in side');
      localStorage.clear();
      this.router.navigate(['login/warehouse-login']);
      return;
    }
    this.warehouseData = JSON.parse(temp);
    this.getImportTransactions();
    this.getAllPackageType();
    this.formValidation();
    this.calculateMaxDate();

    this.productData = [
      {
        formControlProduct: 'product_1',
        formControlQuantity: 'quantity_1',
        validation: false,
      },
    ];
    this.faultyProductData = [
      {
        formControlProduct: 'faulty_1',
        formControlQuantity: 'faultyQuantity_1',
        validation: false,
      },
    ];

    this.actions = {
      isDelete: true,
      isEdit: false,
      isDetailsView: true
    };

    this.importForm.get('importDate').setValue("02/01/2021"); // = new Date();
    this.importForm.controls.importDate.setValue("02/01/2021");
  }

  calculateMaxDate() {
    const dtToday = new Date();

    let month = (dtToday.getMonth() + 1).toString();
    let day = (dtToday.getDate()).toString();
    const year = dtToday.getFullYear();

    if(parseInt(month) < 10)
        month = '0' + month.toString();
    if(parseInt(day) < 10)
        day = '0' + day.toString();

    this.maxDate = year + '-' + month + '-' + day; 
  }

  get f() { return this.importForm.controls; }

  formValidation() {
    this.importForm = this.formBuilder.group({
      product_1: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      quantity_1: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      faulty_1: new FormControl(
        '',
      ),
      faultyQuantity_1: new FormControl(
        '',
      ),
      packageType: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      vendorId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      importDate: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      // messageBy: new FormControl(
      //   '',
      //   Validators.compose([
      //     Validators.required,
      //   ])
      // ),
      // dONumber: new FormControl(
      //   '',
      //   Validators.compose([
      //     Validators.required,
      //   ])
      // ),
      vehicleNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      driverContactNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
    });

    this.importDetailsForm = this.formBuilder.group({
      product_1: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      quantity_1: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      faulty_1: new FormControl(
        '',
      ),
      faultyQuantity_1: new FormControl(
        '',
      ),
      packageType: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      vendorId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      // messageBy: new FormControl(
      //   '',
      //   Validators.compose([
      //     Validators.required,
      //   ])
      // ),
      // dONumber: new FormControl(
      //   '',
      //   Validators.compose([
      //     Validators.required,
      //   ])
      // ),
      vehicleNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      driverContactNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
    });
    this.getAllVendors();
    this.getProducts('all');
  }

  async getImportTransactions() {
    this.getProducts('all');
    LoaderState$.next(true);
    const params = {
      transactionId: '',
      warehouseId: this.warehouseData._id,
      vendorId: (this.selectedVendor != 'Select' && this.selectedVendor != undefined) ? this.selectedVendor : '',
      productId: (this.selectedProduct != 'Select' && this.selectedProduct != undefined) ? this.selectedProduct : '',
      paginationObject: JSON.stringify(this.paginationObject)
    };
    console.log('params in get import', params);
    await this.warehouseService.getImportTransaction$(WebURL.GET_IMPORT_TRANSACTION, params).subscribe({
      next: (results) => {
        LoaderState$.next(false);
        console.log('get import trans result', results);
        this.items = [];
        if (results.data.result.length > 0) {
          this.allImportTrans = results.data.result;
          results.data.result.forEach(async (element) => {
            const importObj = {
              id: element._id,
              'Product/Qty': await this.getProductName(element.productDetails),
              Vendor: element.vendor.name,
              // 'DO Number': element.DONumber,
              'Vehicle Number': element.vehicleNumber,
              Date: new Date(element.createdAt).toLocaleDateString()
            };
            this.items.push(importObj);
          });
          this.paginationObject.totalItems = results.data.count;
          this.showTable = true;
        } else {
          this.items = [];
          this.showTable = false;
        }
      }, error: error => {
        LoaderState$.next(false);
        if (error.status === 404) {
          PopUpState$.next({
            state: 'error',
            message: error.error.message ? error.error.message : 'No past transaction'
          });
        }
        console.log('error in get transaction', error);
      }
    });
  }

  async getProductName(productData) {
    let tagStr = '';
    console.log('tagStr 111', Array.isArray(productData));
    if (!Array.isArray(productData)) {
      tagStr = tagStr + ' ' + productData.name + ' (' + productData.quantity + ')';
    } else {
      for (const element of productData) {
        if (tagStr !== '') {
          tagStr = tagStr + ', ';
        }
        console.log('in loop get name');
        tagStr = tagStr + ' ' + element.name + ' (' + element.quantity + ')'; 
      }
    }
    console.log('tagStr', tagStr);
    return tagStr;
  }

  async getAllVendors() {
    const params = {
      warehouseId: this.warehouseData._id,
      vendorId: ''
    };
    await this.warehouseService.getVendor$(WebURL.GET_VENDOR, params).subscribe({
      next: (results) => {
        console.log('get vendor result', results);
        if (results.data.result.length > 0) {
          this.allVendors = results.data.result;
          this.vendorArray = results.data.result;
          this.removeProductField(undefined);
          this.removeFaultyProductField(undefined);
        } else {
          PopUpState$.next({
            state: 'error',
            message: 'You do not have any vendor mapped with you. Please add vendor first'
          });
          this.allVendors = [];
          this.vendorArray = [];
          this.allPackageType = [];
          this.removeProductField(undefined);
          this.removeFaultyProductField(undefined);
        }
      }, error: error => {
        console.log('error in get vendor', error);
        PopUpState$.next({
          state: 'error',
          message: error.error.message ? error.error.message : 'You do not have any vendor mapped with you. Please add vendor first'
        });
        this.allVendors = [];
        this.allPackageType = [];
        this.removeProductField(undefined);
        this.removeFaultyProductField(undefined);
      }
    });
  }

  async getAllPackageType() {
    const params = {
      warehouseId: this.warehouseData._id,
      typeId: ''
    };
    await this.warehouseService.getPackagingType$(WebURL.GET_PACKAGING_TYPE, params).subscribe({
      next: (results) => {
        console.log('get package result', results);
        if (results.data.length > 0) {
          this.allPackageType = results.data;
          this.removeProductField(undefined);
          this.removeFaultyProductField(undefined);
        } else {
          PopUpState$.next({
            state: 'error',
            message: 'We not able to find any packaging type mapped with the selected vendor. Please add packaging type first'
          });
          this.allPackageType = [];
          this.removeProductField(undefined);
          this.removeFaultyProductField(undefined);
        }
      }, error: error => {
        console.log('error in get package', error);
        PopUpState$.next({
          state: 'error',
          message: error.error.message ? error.error.message :
          'We not able to find any packaging type mapped with the selected vendor. Please add packaging type first'
        });
        this.allPackageType = [];
        this.removeProductField(undefined);
        this.removeFaultyProductField(undefined);
      }
    });
  }

  async getProducts(type = '') {
    let params;
    if (type === 'all') {
      params = {
        productId: '',
        warehouseId: this.warehouseData._id,
        packageTypeId: '',
        vendorId: (this.selectedVendor != 'Select' && this.selectedVendor != undefined) ? this.selectedVendor : '',
      };
    } else {
      console.log('in get Products', this.importForm.get('packageType').value, this.importForm.get('vendorId').value);
      if (this.importForm.get('packageType').value != 'Select' && this.importForm.get('vendorId').value != 'Select') {
        console.log('pass');
        params = {
          productId: '',
          warehouseId: this.warehouseData._id,
          packageTypeId: this.importForm.get('packageType').value,
          vendorId: this.importForm.get('vendorId').value
        };
      } else {
        console.log('fail');
        params = {
          productId: '',
          warehouseId: this.warehouseData._id,
          packageTypeId: '',
          vendorId: ''
        };
      }
    }
    console.log('params get product', params);
    await this.warehouseService.getProducts$(WebURL.GET_PRODUCT, params).subscribe({
      next: (results) => {
        console.log('get product result', results);
        if (results.data.result.length > 0) {
          this.allProduct = results.data.result;
          this.allFaultyProduct = results.data.result;
          this.removeProductField(undefined);
          this.removeFaultyProductField(undefined);
          if (type === 'all') {
            this.productArray = results.data.result;
          }
        } else {
          this.allProduct = [];
          this.productArray = [];
          this.allFaultyProduct = [];
          this.removeProductField(undefined);
          this.removeFaultyProductField(undefined);
        }
      }, error: error => {
        console.log('error in get package', error);
        PopUpState$.next({
          state: 'error',
          message: error.error.message ? error.error.message :
          'We not able to find any product mapped with the selected vendor/ packaging type. Please add product first'
        });
        this.removeProductField(undefined);
        this.removeFaultyProductField(undefined);
      }
    });
  }

  changePage(event) {
    this.paginationObject.currentPage = event;
    this.getImportTransactions();
  }

  deleteTransaction(event) {
    console.log('import remove called', event);
    const index = this.allImportTrans.findIndex(ind => ind._id === event.id);
    this.itemToDelete = this.allImportTrans[index];
    const m = new Date().getMonth();
    const y = new Date().getFullYear();
    const date = new Date(y, m, 1);
    console.log('date of 1st', date);
    console.log('this.itemToDelete.createdAt', this.itemToDelete.createdAt);
    console.log('this.itemToDelete.createdAt', new Date(this.itemToDelete.createdAt));
    console.log('compare', new Date(this.itemToDelete.createdAt) >= date);
    if (new Date(this.itemToDelete.createdAt) >= date) {
      this.showConfirmModal = true;
    } else {
      PopUpState$.next({
        state: 'error',
        message: 'You cannot delete this entry. As, bill already sent to vendor for this entry.'
      });
    }
  }

  cancelDelete(event) {
    console.log('inside cancel delete transaction', event);
    this.showConfirmModal = false;
  }

  async confirmDelete() {
    console.log('inside confirm delete transaction', this.itemToDelete);
    this.showConfirmModal = false;
    await this.warehouseService.deleteImportTransaction$(WebURL.DELETE_TRANSACTION, this.itemToDelete).subscribe({
      next: (results) => {
        console.log('delete result', results);
        this.getImportTransactions();
      }, error: error => {
        console.log('error in delete', error);
        PopUpState$.next({
          state: 'error',
          message: error.error.message ? error.error.message :
          'Not able to delete now. Please try again later'
        });
      }
    });
  }

  showUpdateModal(event) {
    console.log('in show details view', event);
    const index = this.allImportTrans.findIndex(ind => ind._id === event.id);
    this.updateDetails = this.allImportTrans[index];
    console.log('this.updateDetails -->', this.updateDetails);
    if (this.updateDetails.isFaulty) {
      this.displayFaulty = true;
    }
    this.showTransactionDetails = true;
  }

  showBillModal(data) {
    this.updateDetails = data;
    console.log('this.updateDetails -->', this.updateDetails);
    if (this.updateDetails.isFaulty) {
      this.displayFaulty = true;
    }
    this.showTransactionDetails = true;
    this.showButton = false;
    setTimeout(() => {
      this.sendEmail(this.updateDetails);
    }, 2000);
    
  }

  editBill() {
    console.log('Edit bill called');
  }

  disableDetailsModal() {
    this.showTransactionDetails = false;
    this.displayFaulty = false;
  }

  showVendorModal() {
    console.log('called');
    // this.productSelectedItems = [];
    // this.vendorSelectedItems = [];
    // this.packageSelectedItems = [];
    // this.importForm.controls.messageBy.setValue('');
    // this.importForm.controls.quantity.setValue('');
    // this.importForm.controls.dONumber.setValue('');
    this.importForm.controls.vehicleNumber.setValue('');
    this.importForm.controls.driverContactNumber.setValue('');
    this.showNewVendorModal = true;
  }

  disableVendorModal() {
    this.showNewVendorModal = false;
    this.isUpdateForm = false;
    this.importForm.controls.vendorId.setValue('');
    this.importForm.controls.packageType.setValue('');
    // this.importForm.controls.dONumber.setValue('');
    this.importForm.controls.vehicleNumber.setValue('');
    // this.importForm.controls.messageBy.setValue('');
    this.importForm.controls.driverContactNumber.setValue('');
    this.displayFaulty = false;
    this.submitted = false;
    this.removeProductField(undefined);
    this.removeFaultyProductField(undefined);
  }

  async addBill() {
    const year = new Date().getFullYear();
    const month = new Date().getMonth();
    const firstDate = new Date(year, month, 1);
    console.log('firstDate --->', firstDate);
    if (new Date(this.importForm.get('importDate').value) > new Date()) {
      PopUpState$.next({
        state: 'error',
        message: 'Import date cant be future date'
      });
      return;
    }
    if (new Date(this.importForm.get('importDate').value) < firstDate) {
      PopUpState$.next({
        state: 'error',
        message: 'You can select only current month date'
      });
      return;
    }
    const checkProduct = await this.checkProductField();
    let checkFaulty;
    console.log('this.displayFaulty', this.displayFaulty);
    if (this.displayFaulty) {
      checkFaulty = await this.checkFaultyProductField();
    } else {
      checkFaulty = true;
    }
    if (checkProduct && checkFaulty) {
      console.log('checkProduct, checkFaulty', checkProduct, checkFaulty);
      this.submitted = true;
      if (this.importForm.valid) {
        LoaderState$.next(true);
        // const formData = new FormData();
        // formData.append('file', this.selectedFiles[0]);
        // console.log('formData --->', formData);
        const reqBody = {
          warehouse: this.warehouseData._id,
          vendor: this.importForm.get('vendorId').value,
          productDetails: checkProduct,
          // DONumber: this.importForm.get('dONumber').value,
          actionType: 'import',
          vehicleNumber: this.importForm.get('vehicleNumber').value,
          // messageBy: this.importForm.get('messageBy').value,
          driverContactNumber: this.importForm.get('driverContactNumber').value,
          isFaulty: this.displayFaulty,
          faultyProduct: this.displayFaulty ? checkFaulty : [],
          createdAt: new Date(this.importForm.get('importDate').value)
        };
        this.reqBody = reqBody;
        console.log('req body for create transaction', new Date(this.importForm.get('importDate').value));
        await this.warehouseService.createTransaction$(WebURL.CREATE_TRANSACTION, reqBody).subscribe({
          next: (results) => {
            console.log('create transaction result', results);
            this.submitted = false;
            this.disableVendorModal();
            this.showBillModal(results.data);
            LoaderState$.next(false);
            this.removeFaultyProductField(undefined);
            this.removeProductField(undefined);
            this.getImportTransactions();
          }, error: error => {
            LoaderState$.next(false);
            console.log('error in create vendor', error);
            PopUpState$.next({
              state: 'error',
              message: error.error.message ? error.error.message : 'Not able to add import entry at this moment. Please try again later'
            });
          }
        });

      } else {
        console.log('invalid form');
        LoaderState$.next(false);
        return;
      }
    } else {
      PopUpState$.next({
        state: 'error',
        message: 'Product details not filled correctly. Please cross check'
      });
    }
  }

  onProductSelect(item: any) {
    console.log(item);
  }

  onSelectAllProduct(items: any) {
    console.log(items);
  }

  onVendorSelect(item: any) {
    console.log(item);
  }

  onPackageSelect(items: any) {
    console.log(items);
  }

  addProductField = () => {
    const tempProduct = 'product_' + (this.productData.length + 1);
    const tempQty = 'quantity_' + (this.productData.length + 1);
    const formCtrl = this.productData.length - 1;
    this.productData.push({
      formControlProduct: 'product_' + (this.productData.length + 1),
      formControlQuantity: 'quantity_' + (this.productData.length + 1),
      validation: false,
    });

    this.importForm.addControl(
      tempProduct,
      new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      )
    );
    this.importForm.addControl(
      tempQty,
      new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      )
    );
  }

  createProdFieldForView() {
    // this.importForm.controls['product_1'];
    const tempProduct = 'product_' + (this.productData.length + 1);
    const tempQty = 'quantity_' + (this.productData.length + 1);
    const formCtrl = this.productData.length - 1;
    this.productData.push({
      formControlProduct: 'product_' + (this.productData.length + 1),
      formControlQuantity: 'quantity_' + (this.productData.length + 1),
      validation: false,
    });

    this.importForm.addControl(
      tempProduct,
      new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      )
    );
    this.importForm.addControl(
      tempQty,
      new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      )
    );
  }

  removeProductField(index) {
    console.log('in remove product--->', index);
    if (index) {
      this.importForm.removeControl(this.productData[index].formControlProduct);
      this.importForm.removeControl(this.productData[index].formControlQuantity);
      this.productData.splice(index, 1);
    } else {
      console.log('in remove all field');
      this.importForm.controls[this.productData[0].formControlProduct].setValue('');
      this.importForm.controls[this.productData[0].formControlQuantity].setValue(0);
      // tslint:disable-next-line:prefer-for-of
      for (let i = 1; i < this.productData.length; i++) {
        this.importForm.removeControl(this.productData[i].formControlProduct);
        this.importForm.removeControl(this.productData[i].formControlQuantity);
        this.productData.splice(i, 1);
      }
    }
  }

  async checkProductField() {
    let count = 0;
    const productDetails = [];
    console.log('checkProductField ===>', this.allProduct);
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.productData.length; i++) {
      // tslint:disable-next-line:no-unused-expression
      if (this.importForm.get(this.productData[i].formControlProduct).value &&
        this.importForm.get(this.productData[i].formControlQuantity).value) {
          const index = this.allProduct.findIndex(ind => ind._id === this.importForm.get(this.productData[i].formControlProduct).value);
          const temp = {
            _id: this.allProduct[index]._id,
            quantity: parseFloat(this.importForm.get(this.productData[i].formControlQuantity).value),
            name: this.allProduct[index].name,
            storageId: this.allProduct[index].storage._id,
            totalStorage: this.allProduct[index].storage.quantity + parseFloat(this.importForm.get(this.productData[i].formControlQuantity).value),
            packageType: this.allProduct[index].packagingType.packagingType
          };
          productDetails.push(temp);
      } else {
        count ++;
        break;
      }
    }
    console.log('count --->', count);
    if (count > 0) {
      return false;
    } else {
      return productDetails;
    }
  }

  faultyStatus(isFaulty) {
    console.log('Faulty status change to ', isFaulty);
    this.displayFaulty = isFaulty;
    if (!isFaulty) {
      this.removeFaultyProductField(undefined);
    }
  }


  addFaultyProductField() {
    const tempProduct = 'faulty' + (this.faultyProductData.length + 1);
    const tempQty = 'faultyQuantity' + (this.faultyProductData.length + 1);
    const formCtrl = this.faultyProductData.length - 1;
    this.faultyProductData.push({
      formControlProduct: 'faulty' + (this.faultyProductData.length + 1),
      formControlQuantity: 'faultyQuantity' + (this.faultyProductData.length + 1),
      validation: false,
    });

    this.importForm.addControl(
      tempProduct,
      new FormControl(
        '',
      )
    );
    this.importForm.addControl(
      tempQty,
      new FormControl(
        '',
      )
    );
  }

  removeFaultyProductField(index) {
    console.log('in remove faulty product--->', index);
    if (index) {
      this.importForm.removeControl(this.faultyProductData[index].formControlProduct);
      this.importForm.removeControl(this.faultyProductData[index].formControlQuantity);
      this.faultyProductData.splice(index, 1);
    } else {
      console.log('in remove all field');
      this.importForm.controls[this.faultyProductData[0].formControlProduct].setValue('');
      this.importForm.controls[this.faultyProductData[0].formControlQuantity].setValue(0);
      // tslint:disable-next-line:prefer-for-of
      for (let i = 1; i < this.faultyProductData.length; i++) {
        this.importForm.removeControl(this.faultyProductData[i].formControlProduct);
        this.importForm.removeControl(this.faultyProductData[i].formControlQuantity);
        this.faultyProductData.splice(i, 1);
      }
    }
  }

  async checkFaultyProductField() {
    let count = 0;
    const productDetails = [];
    console.log('faultyProductData ===>', this.allFaultyProduct);
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.faultyProductData.length; i++) {
      // tslint:disable-next-line:no-unused-expression
      if (this.importForm.get(this.faultyProductData[i].formControlProduct).value &&
        this.importForm.get(this.faultyProductData[i].formControlQuantity).value) {
          const index =
            this.allFaultyProduct.findIndex(ind => ind._id === this.importForm.get(this.faultyProductData[i].formControlProduct).value);
          const temp = {
            _id: this.allFaultyProduct[index]._id,
            quantity: parseFloat(this.importForm.get(this.faultyProductData[i].formControlQuantity).value),
            name: this.allFaultyProduct[index].name,
            packageType: this.allFaultyProduct[index].packagingType.packagingType
          };
          productDetails.push(temp);
      } else {
        count ++;
        break;
      }
    }
    if (count > 0) {
      return false;
    } else {
      return productDetails;
    }
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
    console.log('selected files', this.selectedFiles);
    // this.sendEmail();
    // const formData = new FormData();
    // formData.append('file', this.selectedFiles);
    // console.log('selected files-- formData', formData.get('file'));
  }

  async sendEmail(data) {
    const formData = new FormData();
    // console.log('html content', this.content.nativeElement);
    // const test = this.content.nativeElement;
    if (this.selectedFiles.length > 0) {
      for (const element of this.selectedFiles) {
        formData.append('file', element);  
      }
    }
    console.log('test--->', JSON.stringify(data));
    data['content'] = document.getElementById('print').outerHTML;
    // console.log('data-->', data);
    formData.append('body', JSON.stringify(data));  
    this.showButton = true; 
    // console.log('formData-->', formData.get('file'));
    // console.log('formData body-->', formData.get('body'));
    await this.warehouseService.sendTransactionalEmail$(WebURL.SEND_TRANSACTIONAL_EMAIl, formData).subscribe({
      next: (results) => {
        console.log('email result', results);
      }, error: error => {
        LoaderState$.next(false);
        console.log('error in send email', error);
        PopUpState$.next({
          state: 'error',
          message: error.error.message ? error.error.message : 'Not able to send email to vendor please inform him'
        });
      }
    });
  }

  dateChange(event) {
    console.log('date change event', event);
    console.log('start date', new Date(event.start._d));
    console.log('end date', new Date(event.end._d));
    this.paginationObject.startDate = new Date(event.start._d);
    this.paginationObject.endDate = new Date(event.end._d);
    console.log('this.paginationObject', this.paginationObject);
    this.getImportTransactions();
  }

}
