import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WarehouseRoutingModule } from './warehouse-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { SharedModule } from '../shared/shared.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { VendorComponent } from './vendor/vendor.component';
import { ImportComponent } from './import/import.component';
import { ExportComponent } from './export/export.component';
import { ProductComponent } from './product/product.component';
import { ReportComponent } from './report/report.component';
import { PackagingComponent } from './packagingType/packagingType.component';
import { ReportTemplateComponent } from './reportTemplate/report.component';
import { WarehouseService } from './warehouse.service';
import { StoreService } from '../../store/store.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxDateRangeModule } from 'ngx-daterange';

@NgModule({
  declarations: [DashboardComponent, VendorComponent, ImportComponent, ExportComponent, ProductComponent, ReportComponent,
    PackagingComponent, ReportTemplateComponent ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxDateRangeModule,
    NgMultiSelectDropDownModule,
    SharedModule,
    WarehouseRoutingModule,
  ],
  providers: [ WarehouseService, StoreService ]
})
export class WarehouseModule { }
