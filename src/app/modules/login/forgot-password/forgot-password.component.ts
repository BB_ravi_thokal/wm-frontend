import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaderState$, PopUpState$ } from '../../../app.component';
import { LoginService } from '../login.service';
import * as WebURL from '../../../config/web-urls';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['../../../app.component.scss', '../login.component.scss', './forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  getOTPForm: FormGroup;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.formValidation();
  }

  get f() { return this.getOTPForm.controls; }
  formValidation() {
    this.getOTPForm = this.formBuilder.group({
      emailId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
        ])
      )
    });
  }

  async login() {
    LoaderState$.next(true);
    this.submitted = true;
    console.log('this.getOTPForm.valid', this.getOTPForm.get('emailId').value);
    this.submitted = true;
    if (this.getOTPForm.valid) {
      const data = {
        emailId: this.getOTPForm.get('emailId').value
      };
      await this.loginService.login$(WebURL.FORGOT_PASSWORD, data).subscribe({
        next: (results) => {
          console.log('forgot password result', results);
          LoaderState$.next(false);
          PopUpState$.next({
            state: 'success',
            message: 'Temporary password sent on your email.'
          });
          this.router.navigate(['login/change-password']);
        }, error: error => {
          console.log('forgot password result', error);
            LoaderState$.next(false);
            PopUpState$.next({
              state: 'error',
              message: error.error.message ? error.error.message : 'Not able to connect with server please try later'
            });
        }
      });
    } else {
      LoaderState$.next(false);
      return;
    }
  }


}

