import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaderState$, PopUpState$ } from '../../../app.component';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';
import * as WebURL from '../../../config/web-urls';

@Component({
  selector: 'app-vendor-login',
  templateUrl: './vendor-login.component.html',
  styleUrls: ['../../../app.component.scss', '../login.component.scss', './vendor-login.component.scss']
})
export class VendorLoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    this.formValidation();
  }
  get f() { return this.loginForm.controls; }
  formValidation() {
    this.loginForm = this.formBuilder.group({
      userId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
        ])
      ),
      password: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      )
    });
  }

  async login() {
    this.submitted = true;
    LoaderState$.next(true);
    if (this.loginForm.valid) {
      const data = {
        userId: this.loginForm.get('userId').value,
        password: this.loginForm.get('password').value
      };
      await this.loginService.login$(WebURL.LOGIN, data).subscribe({
        next: (results) => {
          LoaderState$.next(false);
          localStorage.setItem('access_token', results.data.token);
          localStorage.setItem('VendorData', JSON.stringify(results.data.warehouseData));
          // this.storeService.dispatchWarehouseData(results.data.warehouseData);
          console.log('success result', results);
          this.router.navigate(['vendor/report']);
        }, error: error => {
            LoaderState$.next(false);
            PopUpState$.next({
              state: 'error',
              message: error.error.message ? error.error.message : 'Not able to login at the moment. Please try again later'
            });
        }
      });
    } else {
      LoaderState$.next(false);
      return;
    }
  }


}
