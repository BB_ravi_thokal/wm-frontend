import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendorLoginComponent } from './vendor-login/vendor-login.component';
import { WarehouseLoginComponent } from './warehouse-login/warehouse-login.component';
import { WarehouseSignupComponent } from './warehouse-signup/warehouse-signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'warehouse-login',
      },
      { path: 'warehouse-login', component: WarehouseLoginComponent },
      { path: 'vendor-login', component: VendorLoginComponent },
      { path: 'warehouse-sign', component: WarehouseSignupComponent },
      { path: 'forgot-password', component: ForgotPasswordComponent },
      { path: 'change-password', component: ChangePasswordComponent }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule {
}
