import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaderState$, PopUpState$ } from '../../../app.component';
import { LoginService } from '../login.service';
import * as WebURL from '../../../config/web-urls';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['../../../app.component.scss', '../login.component.scss', './change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  changePassForm: FormGroup;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.formValidation();
  }
  get f() { return this.changePassForm.controls; }
  formValidation() {
    this.changePassForm = this.formBuilder.group({
      emailId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
        ])
      ),
      tempPassword: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      newPassword: new FormControl(
        '',
        Validators.compose([
          Validators.required
        ])
      )
    });
  }

  async login() {
    LoaderState$.next(true);
    this.submitted = true;
    console.log('this.changePassForm.valid', this.changePassForm.get('emailId').value);
    this.submitted = true;
    if (this.changePassForm.valid) {
      console.log('emailId ===>', this.changePassForm.get('emailId').value);
      console.log('tempPassword ===>', this.changePassForm.get('tempPassword').value);
      console.log('newPassword ===>', this.changePassForm.get('newPassword').value);
      const data = {
        emailId: this.changePassForm.get('emailId').value,
        temporaryPassword: this.changePassForm.get('tempPassword').value,
        newPassword: this.changePassForm.get('newPassword').value 
      };
      await this.loginService.login$(WebURL.CHANGE_PASSWORD, data).subscribe({
        next: (results) => {
          console.log('change password success', results);
          LoaderState$.next(false);
          PopUpState$.next({
            state: 'success',
            message: 'Password change successfully'
          });
          this.router.navigate(['dw-home']);
        }, error: error => {
          console.log('change password success', error);
            LoaderState$.next(false);
            PopUpState$.next({
              state: 'error',
              message: error.error.message ? error.error.message : 'Not able to connect with server please try later'
            });
        }
      });
      LoaderState$.next(false);
    } else {
      LoaderState$.next(false);
      return;
    }
  }


}
