import { Inject, Injectable } from '@angular/core';
import { WebService } from '../../services/webApi.service';
import * as WebURL from '../../config/web-urls';
import { Observable } from 'rxjs';
import { BASE_URL } from '../../config/app-config';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
    constructor(
        private webService: WebService,
        private http: HttpClient,
    ) { }
    
    /**
     * This method is for Post Api calls
     * @param url String Api URL
     * @param requestBody Object required for the Post request
     * @returns Return the respective response from the Apis.
     */
    public login$(url, requestBody): Observable<any> {
        console.log(url, 'url');
        console.log(BASE_URL, 'BASE_URL');
        console.log(requestBody, 'requestBody');
        let httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                })
            };
        return this.http.post<any>(BASE_URL + url, requestBody, httpOptions);
    }

}
