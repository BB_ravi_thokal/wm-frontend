import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaderState$ } from '../../../app.component';
@Component({
  selector: 'app-warehouse-signup',
  templateUrl: './warehouse-signup.component.html',
  styleUrls: ['../../../app.component.scss', '../login.component.scss', './warehouse-signup.component.scss']
})
export class WarehouseSignupComponent implements OnInit {
  selectedWareHouse = 'Choose Warehouse';
  registerWarehouseForm: FormGroup;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.formValidation();
  }

  get f() { return this.registerWarehouseForm.controls; }

  formValidation() {
    this.registerWarehouseForm = this.formBuilder.group({
      wareHouseName: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      ownerName: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      ownerContactNumber: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
      ownerEmailId: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
        ])
      ),
      address: new FormControl(
        '',
        Validators.compose([
          Validators.required,
        ])
      ),
    });
  }

  requestAccess() {
    LoaderState$.next(true);
    this.submitted = true;
    console.log('this.loginForm.valid', this.registerWarehouseForm.get('userId').value);
    this.submitted = true;
    if (this.registerWarehouseForm.valid) {
      console.log('wareHouseName ===>', this.registerWarehouseForm.get('wareHouseName').value);
      console.log('ownerName ===>', this.registerWarehouseForm.get('ownerName').value);
      LoaderState$.next(false);
    } else {
      LoaderState$.next(false);
      return;
    }
  }

  selectWarehouse(wareHouse) {
    console.log('selected warehouse', wareHouse);
    this.selectedWareHouse = wareHouse;
  }

}

