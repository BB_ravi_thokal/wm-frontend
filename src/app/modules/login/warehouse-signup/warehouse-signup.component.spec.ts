import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseSignupComponent } from './warehouse-signup.component';

describe('WarehouseSignupComponent', () => {
  let component: WarehouseSignupComponent;
  let fixture: ComponentFixture<WarehouseSignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseSignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
