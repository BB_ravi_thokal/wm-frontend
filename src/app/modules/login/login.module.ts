import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login-routing.module';
import { VendorLoginComponent } from './vendor-login/vendor-login.component';
import { WarehouseLoginComponent } from './warehouse-login/warehouse-login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WarehouseSignupComponent } from './warehouse-signup/warehouse-signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { LoginService } from './login.service';
import { StoreService } from '../../store/store.service';

@NgModule({
  declarations: [
    VendorLoginComponent,
    WarehouseLoginComponent,
    WarehouseSignupComponent,
    ForgotPasswordComponent,
    ChangePasswordComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LoginRoutingModule
  ],
  providers: [ LoginService, StoreService ],
})
export class LoginModule { }
