import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.scss']
})
export class ConfirmationModalComponent implements OnInit {

  @Input() confirmModalObject;
  @Input() showConfirmModal = false;

  @Output() OnLeftClick = new EventEmitter<any>();
  @Output() OnRightClick = new EventEmitter<any>();

  selectIndex = [];
  subscription: Subscription;

  constructor(
  ) { }

  ngOnInit() {
  }

  onLeftClick(element) {
    console.log('delete method called', element);
    this.showConfirmModal = false;
    this.OnLeftClick.emit(element);
  }

  onRightClick(element) {
    console.log('delete method called', element);
    this.showConfirmModal = false;
    this.OnRightClick.emit(element);
  }

  closeModal() {
    this.showConfirmModal = false;
  }

}
