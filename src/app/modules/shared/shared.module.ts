import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {SidebarComponent} from './Sidebar/Sidebar.component';
import { ConfirmationModalComponent } from './confirmation-modal/confirmation-modal.component';
import { TableComponent } from './table/table.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { KeysPipe } from '../../pipes/keys.pipe';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [HeaderComponent, FooterComponent, SidebarComponent, TableComponent, KeysPipe,
    ConfirmationModalComponent, InvoiceComponent ],
  imports: [
    CommonModule,
    NgxPaginationModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [ HeaderComponent, FooterComponent, SidebarComponent, TableComponent, KeysPipe, ConfirmationModalComponent, InvoiceComponent ]
})
export class SharedModule { }
