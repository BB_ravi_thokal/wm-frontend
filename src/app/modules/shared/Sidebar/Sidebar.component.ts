import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-Sidebar',
  templateUrl: './Sidebar.component.html',
  styleUrls: ['./Sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() activeScreen;
  @Input() isActive = true;
  
  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
    console.log('activeScreen', this.activeScreen);
    console.log('isActive', this.activeScreen);
  }

  async logout() {
    await localStorage.clear();
    this.router.navigate(['dw-home']);
  }

}
