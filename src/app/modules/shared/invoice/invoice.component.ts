import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
//import * as jsPDF from 'jspdf';
import { jsPDF } from 'jspdf';
//import * as html2canvas from 'html2canvas';
import html2canvas from 'html2canvas';
//import 'jspdf-autotable'

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

  @ViewChild('content') content: ElementRef;
  @Input() orderDetails;
  @Input() showButton = true;
  @Input() header;
  @Input() type;

  warehouseData;

  constructor(
  ) {
    this.loadContent();
  }

  async loadContent() {
    let temp: any = await localStorage.getItem('WarehouseData');
    if (temp == undefined) {
      console.log('inside');
      const temp: any = await JSON.parse(localStorage.getItem('VendorData'));
      this.warehouseData = temp.warehouse;
      return;
    } else {
      console.log('outside');
      this.warehouseData = JSON.parse(temp);
    }

  }

  async ngOnInit() {
    console.log('this.warehouseData', this.warehouseData);
    console.log('orderDetails', this.orderDetails);
  }

  async download() {
    this.showButton = false;
    setTimeout(() => {
      window.print();
    }, 1000)
    setTimeout(() => {2
      this.showButton = true;
    }, 4000)
  }

  async generatePdf() {
    // const elementToPrint = document.getElementById('downloadPDF');
    // let handleElement = {
    //   '#editor':function(element,renderer){
    //     return true;
    //   }
    // };
    // let doc = new jsPDF('p','pt','a4');
    // doc.fromHTML(elementToPrint.innerHTML,15,15,{
    //   'width': 200,
    //   'elementHandlers': handleElement
    // });
    // doc.save('angular-demo.pdf');
    // const elementToPrint = document.getElementById('downloadPDF').outerHTML; //The html element to become a pdf
    
    // doc.addHTML(elementToPrint, () => {
    //     doc.save('web.pdf');
    // });
    
    
    
    //const pdf = new jsPDF('p', 'pt', 'a4');
    //await doc.html(elementToPrint);
    //await doc.save('web.pdf');
    
    const doc = new jsPDF('p', 'pt', 'a4');
    const data = document.getElementById('downloadPDF');
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  

      const contentDataURL = canvas.toDataURL('image/jpeg')  
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL,'jpeg', 0, 0, imgWidth, imgHeight);  
      pdf.save('MYPdf.pdf'); // Generated PDF   
    });
    
    // doc.addHTML(document.getElementById('print').outerHTML, function() {
    //   doc.save('test.pdf');
    // });
  }
}
