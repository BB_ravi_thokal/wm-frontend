import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input() items;
  @Input() paginationObject;
  @Input() isDelete;
  @Input() isEdit;
  @Input() isView;

  @Output() OnDelete = new EventEmitter<any>();
  @Output() OnEdit = new EventEmitter<any>();
  @Output() OnView = new EventEmitter<any>();
  @Output() OnPageChange = new EventEmitter<any>();

  selectIndex = [];
  subscription: Subscription;

  constructor(
  ) { }

  ngOnInit() {
    // this.subscription = this.callFunction.getFunctionCall().subscribe(() => {
    //   this.selectIndex = [];
    // });
    console.log('items in table component', this.items);

  }

  editMethod(element) {
    console.log('delete method called', element);
    this.OnEdit.emit(element);
  }

  deleteMethod(element) {
    console.log('delete method called', element);
    this.OnDelete.emit(element);
  }

  viewMethod(element) {
    console.log('delete method called', element);
    this.OnView.emit(element);
  }

  pageChange(event) {
    console.log('called');
    this.OnPageChange.emit(event);
  }

}
