import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BASE_URL } from '../config/app-config';
import {catchError} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class WebService {

    constructor(
        private http: HttpClient,
    ) { }
    /**
     * @description This method is for Get Api Calls
     * @param url String url for the API
     * @param httpOptions header part is optional
     * @returns This function returns the respective response from the Api
     */
    public getApi(url): Observable<any> {
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        } else {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                })
            };
        }
        return this.http.get(BASE_URL + url, httpOptions);
    }

    /**
     * This method is for Post Api calls
     * @param url String Api URL
     * @param requestBody Object required for the Post request
     * @returns Return the respective response from the Apis.
     */
    public postApi$(url, requestBody): Observable<any> {
        console.log(url, 'url');
        console.log(BASE_URL, 'BASE_URL');
        console.log(requestBody, 'requestBody');
        const accessToken = localStorage.getItem('access_token');
        let httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        return this.http.post<any>(BASE_URL + url, requestBody, httpOptions);
    }
}
