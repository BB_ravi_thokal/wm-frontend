import { Injectable, Inject } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { Observable } from 'rxjs';

@Injectable()
export class VendorAuthGuard implements CanActivate {


  constructor(
    private router: Router,
  ) {
  }

  /**
   * @description This method will check user already login or not
   * @returns Return the true if user already logged in or else false
   */
  public canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    try {
      const isTokenAvailable = localStorage.getItem('access_token');
      if (isTokenAvailable) {
        const decoded: any = jwt_decode(isTokenAvailable);
        if (decoded.role === 'Vendor') {
          return true;
        } else {
          this.router.navigate(['/dw-home']);  
        }
        return true;
      } else {
        this.router.navigate(['/dw-home']);  
      }
    } catch (err) {
      return false;
    }
  }
}
