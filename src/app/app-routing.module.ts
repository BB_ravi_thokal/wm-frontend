import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './modules/landing/landing.component';
import { WarehouseAuthGuard } from './services/warehouse-auth-guard.service';
import { VendorAuthGuard } from './services/vendor-auth-guard.service';
import { AuthGuard } from './services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    pathMatch: 'full',
    redirectTo: 'dw-home',
  },
  { 
    path: 'dw-home', 
    component: LandingComponent
  },
  {
    path: 'login',
    canActivate: [AuthGuard],
    loadChildren: './modules/login/login.module#LoginModule',
  },
  {
    path: 'warehouse',
    canActivate: [WarehouseAuthGuard],
    loadChildren: './modules/warehouse/warehouse.module#WarehouseModule',
  },
  {
    path: 'vendor',
    // canActivate: [VendorAuthGuard],
    loadChildren: './modules/vendor/vendor.module#VendorModule',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
