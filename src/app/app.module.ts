import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './modules/landing/landing.component';
import { StoreModule } from '@ngrx/store';
import { reducers } from './store/reducer-composition';
import { WarehouseAuthGuard } from './services/warehouse-auth-guard.service';
import { VendorAuthGuard } from './services/vendor-auth-guard.service';
import { AuthGuard } from './services/auth-guard.service';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxPaginationModule,
    HttpClientModule,
    StoreModule.forRoot({
      warehouseData: reducers.warehouseData,
    }),
  ],
  providers: [ WarehouseAuthGuard, VendorAuthGuard, AuthGuard ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
