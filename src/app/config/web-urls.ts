//export const BASE_URL = 'http://localhost:4000/';
export const BASE_URL = 'https://ec2-3-135-198-16.us-east-2.compute.amazonaws.com/'; // production
// https://ec2-3-135-198-16.us-east-2.compute.amazonaws.com/

// login urls
export const LOGIN = 'login';
export const FORGOT_PASSWORD = 'login/forgot-password';
export const CHANGE_PASSWORD = 'login/change-password';

// Warehouse urls
export const CREATE_WAREHOUSE = 'warehouse/create';
export const GET_WAREHOUSE = 'warehouse';
export const UPDATE_WAREHOUSE = 'warehouse/update';
export const DELETE_WAREHOUSE = 'warehouse/delete';
export const CHECK_ACTIVE_STATUS = 'warehouse/check-active';

// Vendor urls
export const CREATE_VENDOR = 'vendor/create';
export const GET_VENDOR = 'vendor';
export const UPDATE_VENDOR = 'vendor/update';
export const DELETE_VENDOR = 'vendor/delete';

// Role urls
export const CREATE_ROLE = 'role/create';
export const GET_ROLE = 'role';
export const DELETE_ROLE = 'role/delete';

// Packaging type
export const CREATE_PACKAGING_TYPE = 'packagingType/create';
export const GET_PACKAGING_TYPE = 'packagingType';
export const UPDATE_PACKAGING_TYPE = 'packagingType/update';
export const DELETE_PACKAGING_TYPE = 'packagingType/delete';

// Packaging type
export const CREATE_PRODUCT = 'product/create';
export const GET_PRODUCT = 'product';
export const UPDATE_PRODUCT = 'product/update';
export const DELETE_PRODUCT = 'product/delete';

// Packaging type
export const CREATE_OFFER = 'offer/create';
export const GET_OFFER = 'offer';
export const UPDATE_OFFER = 'offer/update';
export const DELETE_OFFER = 'offer/delete';

// transaction urls for import export
export const CREATE_TRANSACTION = 'transaction/create';
export const GET_TRANSACTION = 'transaction/';
export const GET_IMPORT_TRANSACTION = 'transaction/get-import';
export const GET_EXPORT_TRANSACTION = 'transaction/get-export';
export const DELETE_TRANSACTION = 'transaction/delete-transaction';

// subscription
export const GET_SUBSCRIPTION = 'subscription';
export const CREATE_SUBSCRIPTION = 'subscription/create';

// send Email
// tslint:disable-next-line:variable-name
export const SEND_TRANSACTIONAL_EMAIl = 'email/transaction';

