(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-vendor-vendor-module"],{

/***/ "./src/app/modules/vendor/report/report.component.html":
/*!*************************************************************!*\
  !*** ./src/app/modules/vendor/report/report.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar\" data-color=\"purple\" data-background-color=\"white\" data-image=\"../../../../assets/img/sidebar-3.jpg\">\n  <div class=\"logo\">\n      <a href=\"#\" class=\"simple-text logo-normal\">\n          <div class=\"image-container\" style=\"height: 90px;\">\n              <img src=\"../../../../assets/img/wmlogowithtitle2.png\" style=\"width: 300px;margin: -34px -50px -44px -48px;\">\n          </div>     \n      </a>\n  </div>\n  <div class=\"sidebar-wrapper\">\n      <ul class=\"nav\">\n        <li class=\"nav-item active\">\n            <a class=\"nav-link\" >\n                <i class=\"material-icons\">library_books</i>\n                <p>Report</p>\n            </a>\n        </li>\n        <li>\n          <a class=\"nav-link\" (click)='logout()' >\n              <i class=\"material-icons\">logout</i>\n              <p>Log out</p>\n          </a>\n        </li>\n      </ul>\n  </div>\n</div>\n<div class=\"main-panel\">\n  <app-header [pageName]=\"pageName\"></app-header>\n  <div class=\"content\">\n    <div class=\"container-fluid\">\n      <app-confirmation-modal [confirmModalObject]=\"confirmModalObject\" [showConfirmModal]=\"showConfirmModal\"\n        (OnLeftClick)=\"cancelDelete($event)\" (OnRightClick)=\"confirmDelete($event)\" ></app-confirmation-modal>\n      <div class=\"row\">\n        <div class=\"container-fluid\">\n          <div class=\"row\">\n            <div class=\"col-md-3\" >\n                <label>Product</label>\n                <select class=\"dropdown_ select\" (change)=\"getImportTransactions('',$event.target.value)\" [(ngModel)]=\"selectedProduct\">\n              <option >Select</option>\n              <option *ngFor=\"let product of allProduct\" [value]=\"product._id\" \n                  [selected]=\"product._id == _id\">\n                {{product?.name}}\n              </option>\n            </select>\n            </div>\n          </div>\n          \n          <div style=\"margin-top: 20px; margin-bottom: 20px\" *ngIf=\"showTable\">\n            <app-table (OnPageChange)=\"changePage($event)\" (OnDelete)=\"deleteTransaction($event)\" (OnView)=\"showUpdateModal($event)\"\n              [items]=\"items\" [isDelete]=\"false\" [isEdit]=\"false\" [isView]=\"true\"\n              [paginationObject]=\"paginationObject\"></app-table>\n          </div>\n        </div>\n        <ng-container *ngIf=\"showNewVendorModal\">\n          <div class=\"modal-class fade-class\">\n            <div class=\"modal-dialog\">\n              <div class=\"modal-content\">\n        \n                <!-- Modal Header -->\n                <div class=\"modal-header\">\n                  <h4 class=\"modal-title\">Create Import Bill</h4>\n                  <button type=\"button\" class=\"close\" (click)=\"disableVendorModal()\">&times;</button>\n                </div>\n        \n                <!-- Modal body -->\n                <div class=\"modal-body\">\n                  <div class=\"card2 card border-0 px-4 py-5\">\n                    <form class=\"form-signin\" [formGroup]=\"importForm\">\n                      <div class=\"form-group row px-3\">\n                          <label class=\"mb-1\">\n                              <h6 class=\"mb-0 text-sm\">Select Vendor</h6>\n                          </label>\n                        <select formControlName=\"vendorId\" [ngClass]=\"{ 'is-invalid': submitted && f.vendorId.errors }\" type=\"text\"  class=\"form-control dropdown_ select\" (change)=\"getProducts()\" >\n                          <option *ngIf=\"!isUpdateForm\" >Select</option>\n                          <option *ngIf=\"isUpdateForm\" [value]=\"selectedVendorId\" selected>{{selectedVendorName}}</option>\n                          <option *ngFor=\"let vendor of allVendors\" [value]=\"vendor?._id\">\n                          {{vendor?.name}}\n                          </option>\n                        </select>\n                        <div *ngIf=\"submitted && f.vendorId.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.vendorId.errors.required\">Please select vendor belongs to this product</div>\n                        </div>\n                      </div>\n                      <div class=\"form-group row px-3\">\n                          <label class=\"mb-1\">\n                              <h6 class=\"mb-0 text-sm\">Select Packaging Type</h6>\n                          </label>\n                        <select formControlName=\"packageType\" [ngClass]=\"{ 'is-invalid': submitted && f.packageType.errors }\" type=\"text\"  class=\"form-control dropdown_ select\" (change)=\"getProducts()\">\n                          <option *ngIf=\"!isUpdateForm\">Select</option>\n                          <option *ngIf=\"isUpdateForm\" [value]=\"selectedPackageId\" selected>{{selectedPackageName}}</option>\n                          <option *ngFor=\"let package of allPackageType\" [value]=\"package?._id\">\n                          {{package?.packagingType}}\n                          </option>\n                        </select>\n                        <div *ngIf=\"submitted && f.packageType.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.packageType.errors.required\">Please select packaging type</div>\n                        </div>\n                      </div>\n                      <div *ngFor=\"let item of productData;let i =index; let last = last;\" class=\"form-group\">\n                        <div class=\"form-group row px-3\">\n                          <label class=\"mb-1\">\n                            <h6 class=\"mb-0 text-sm\">Select product</h6>\n                          </label>\n                          <select formControlName=\"{{item.formControlProduct}}\" type=\"text\"\n                            class=\"form-control dropdown_ select\">\n                            <option *ngIf=\"!isUpdateForm\">Select</option>\n                            <option *ngIf=\"isUpdateForm\" [value]=\"selectedProductId\" selected>{{selectedProductName}}</option>\n                            <option *ngFor=\"let product of allProduct\" [value]=\"product?._id\">\n                              {{product?.name}}\n                            </option>\n                          </select>\n                          <div *ngIf=\"submitted && item.validation.required\">\n                            <small class=\"invalid-feedback d-block text-left\">Please fill the field</small>\n                          </div>\n                        </div>\n                        <div class=\"form-group row px-3\">\n                          <label class=\"mb-1\">\n                            <h6 class=\"mb-0 text-sm\">Quantity</h6>\n                          </label>\n                          <input formControlName=\"{{item.formControlQuantity}}\" type=\"number\" class=\"form-control\"\n                            placeholder=\"Enter product quantity\">\n                          <div *ngIf=\"item.validation\">\n                            <small class=\"invalid-feedback d-block text-left\">Please fill the field</small>\n                          </div>\n                        </div>\n                        <ng-container>\n                          <div *ngIf=\"last\">\n                            <button type=\"button\" class=\"btn text-center\" (click)=\"addProductField()\">Add</button>\n                          </div>\n                          <div *ngIf=\"!last\">\n                            <button type=\"button\" class=\"btn text-center\" (click)=\"removeProductField(i)\">Remove</button>\n                          </div>\n                        </ng-container>\n                      </div>\n                      <div class=\"form-group row px-3\">\n                        <label class=\"mb-1\">\n                          <h6 class=\"mb-0 text-sm\">Message By</h6>\n                        </label>\n                        <input formControlName=\"messageBy\" [ngClass]=\"{ 'is-invalid': submitted && f.messageBy.errors }\"\n                          type=\"text\" class=\"form-control\" placeholder=\"Enter name of reference person\">\n                        <div *ngIf=\"submitted && f.messageBy.errors\" class=\"invalid-feedback\">\n                          <div *ngIf=\"f.messageBy.errors.required\">Please add reference party name</div>\n                        </div>\n                      </div>\n                      <div class=\"form-group row px-3\">\n                        <label class=\"mb-1\">\n                          <h6 class=\"mb-0 text-sm\">D/O Number</h6>\n                        </label>\n                        <input formControlName=\"dONumber\" [ngClass]=\"{ 'is-invalid': submitted && f.dONumber.errors }\"\n                          type=\"text\" class=\"form-control\" placeholder=\"Enter D/O number\">\n                        <div *ngIf=\"submitted && f.dONumber.errors\" class=\"invalid-feedback\">\n                          <div *ngIf=\"f.dONumber.errors.required\">Please add D/O number</div>\n                        </div>\n                      </div>\n                      <div class=\"form-group row px-3\">\n                        <label class=\"mb-1\">\n                          <h6 class=\"mb-0 text-sm\">Vehicle Number</h6>\n                        </label>\n                        <input formControlName=\"vehicleNumber\" [ngClass]=\"{ 'is-invalid': submitted && f.vehicleNumber.errors }\"\n                          type=\"text\" class=\"form-control\" placeholder=\"Enter vehicle number\">\n                        <div *ngIf=\"submitted && f.vehicleNumber.errors\" class=\"invalid-feedback\">\n                          <div *ngIf=\"f.vehicleNumber.errors.required\">Please add vehicle number</div>\n                        </div>\n                      </div>\n                      <div class=\"form-group row px-3\">\n                        <label class=\"mb-1\">\n                          <h6 class=\"mb-0 text-sm\">Driver Contact Number</h6>\n                        </label>\n                        <input formControlName=\"driverContactNumber\"\n                          [ngClass]=\"{ 'is-invalid': submitted && f.driverContactNumber.errors }\" type=\"number\"\n                          class=\"form-control\" placeholder=\"Enter driver contact number\">\n                        <div *ngIf=\"submitted && f.driverContactNumber.errors\" class=\"invalid-feedback\">\n                          <div *ngIf=\"f.driverContactNumber.errors.required\">Please add driver contact number</div>\n                        </div>\n                      </div>\n                      <div class=\"form-group row px-3\">\n                        <label class=\"mb-1\">\n                          <h6 class=\"mb-0 text-sm\">Is any faulty product</h6>\n                        </label>\n                          <input style=\"padding-left: 20px; margin-left: 50px;\" type=\"radio\" id=\"true\" name=\"isFaulty\" value=\"true\" (change)=\"faultyStatus(true)\">\n                          <label style=\"padding-left: 10px; padding-right: 10px\" for=\"true\">True</label><br>\n                          <input style=\"padding-left: 20px\" type=\"radio\" id=\"false\" name=\"isFaulty\" value=\"false\" (change)=\"faultyStatus(false)\" checked>\n                          <label style=\"padding-left: 10px; padding-right: 10px\" for=\"age2\">False</label><br>\n                      </div>\n                      <div *ngIf=\"displayFaulty\">\n                        <div *ngFor=\"let item of faultyProductData;let i =index; let last = last;\" class=\"form-group\">\n                          <div class=\"form-group row px-3\">\n                            <label class=\"mb-1\">\n                              <h6 class=\"mb-0 text-sm\">Select faulty product</h6>\n                            </label>\n                            <select formControlName=\"{{item.formControlProduct}}\" type=\"text\"\n                              class=\"form-control dropdown_ select\">\n                              <option *ngIf=\"!isUpdateForm\">Select</option>\n                              <option *ngIf=\"isUpdateForm\" [value]=\"selectedProductId\" selected>{{selectedProductName}}</option>\n                              <option *ngFor=\"let product of allFaultyProduct\" [value]=\"product?._id\">\n                                {{product?.name}}\n                              </option>\n                            </select>\n                            <div *ngIf=\"submitted && item.validation.required\">\n                              <small class=\"invalid-feedback d-block text-left\">Please fill the field</small>\n                            </div>\n                          </div>\n                          <div class=\"form-group row px-3\">\n                            <label class=\"mb-1\">\n                              <h6 class=\"mb-0 text-sm\">Quantity</h6>\n                            </label>\n                            <input formControlName=\"{{item.formControlQuantity}}\" type=\"text\" class=\"form-control\"\n                              placeholder=\"Enter product quantity\">\n                            <div *ngIf=\"item.validation\">\n                              <small class=\"invalid-feedback d-block text-left\">Please fill the field</small>\n                            </div>\n                          </div>\n                          <ng-container>\n                            <div *ngIf=\"last\">\n                              <button type=\"button\" class=\"btn text-center\" (click)=\"addFaultyProductField()\">Add</button>\n                            </div>\n                            <div *ngIf=\"!last\">\n                              <button type=\"button\" class=\"btn text-center\" (click)=\"removeFaultyProductField(i)\">Remove</button>\n                            </div>\n                          </ng-container>\n                        </div>\n                        <div class=\"form-group row px-3\">\n                          <label class=\"mb-1\">\n                            <h6 class=\"mb-0 text-sm\">Upload image</h6>\n                          </label>\n                        </div>\n                        <input type=\"file\" id=\"myfile\" name=\"myfile\" multiple accept=\"image/*\" (change)=\"selectFile($event)\"><br><br>\n                      </div>\n                    </form>\n        \n                  </div>\n                </div>\n                <!-- Modal footer -->\n                <div class=\"modal-footer\">\n                  <button *ngIf=\"!isUpdateForm\" type=\"button\" class=\"btn btn-blue text-center\" (click)=\"addBill()\">Add</button>\n                  <button *ngIf=\"isUpdateForm\" type=\"button\" class=\"btn btn-blue text-center\" (click)=\"updateBill()\">Update</button>\n                </div>\n        \n              </div>\n            </div>\n          </div>\n        </ng-container>\n        <ng-container *ngIf=\"showTransactionDetails\">\n          <div class=\"modal-class fade-class\" #content id=\"print\">\n            <div class=\"modal-dialog\" style=\"min-width: 100%; height: 100%; margin: 0; padding: 0;\" >\n            <div class=\"modal-content\" style=\"height: auto;min-height: 100%; border-radius: 0;\">\n                <div class=\"modal-header\">\n                  <button type=\"button\" class=\"close\" (click)=\"disableDetailsModal()\">&times;</button>\n                </div>\n                <div class=\"modal-body\">\n                  <div class=\"card2 card border-0 px-4 py-5\">\n                    <app-invoice [orderDetails]=\"updateDetails\" [showButton]=\"showButton\" [header]=\"invoiceHeader\"></app-invoice>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </ng-container>\n      </div>\n    </div>\n  </div>\n  <app-footer></app-footer>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/modules/vendor/report/report.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/modules/vendor/report/report.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\n  background-color: #3e94ec;\n  font-family: \"Roboto\", helvetica, arial, sans-serif;\n  font-size: 16px;\n  font-weight: 400;\n  text-rendering: optimizeLegibility; }\n\ndiv.table-title {\n  display: block;\n  margin: auto;\n  max-width: 600px;\n  padding: 5px;\n  width: 100%; }\n\n.table-title h3 {\n  color: #fafafa;\n  font-size: 30px;\n  font-weight: 400;\n  font-style: normal;\n  font-family: \"Roboto\", helvetica, arial, sans-serif;\n  text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);\n  text-transform: uppercase; }\n\n.modal-class {\n  position: fixed;\n  top: 0;\n  left: 0;\n  z-index: 1050;\n  width: 100%;\n  height: 100%;\n  overflow: scroll;\n  outline: 0; }\n\n/*** Table Styles **/\n\n.table-fill {\n  background: white;\n  border-radius: 3px;\n  border-collapse: collapse;\n  height: 320px;\n  margin: auto;\n  max-width: 600px;\n  padding: 5px;\n  width: 100%;\n  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);\n  -webkit-animation: float 5s infinite;\n          animation: float 5s infinite; }\n\nth {\n  color: #D5DDE5;\n  background: #1b1e24;\n  border-bottom: 4px solid #9ea7af;\n  border-right: 1px solid #343a45;\n  font-size: 23px;\n  font-weight: 100;\n  padding: 24px;\n  text-align: left;\n  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);\n  vertical-align: middle; }\n\nth:first-child {\n  border-top-left-radius: 3px; }\n\nth:last-child {\n  border-top-right-radius: 3px;\n  border-right: none; }\n\ntr {\n  border-top: 1px solid #C1C3D1;\n  color: #666B85;\n  font-size: 16px;\n  font-weight: normal;\n  text-shadow: 0 1px 1px rgba(255, 255, 255, 0.1); }\n\ntr:hover td {\n  background: #4E5066;\n  color: #FFFFFF;\n  border-top: 1px solid #22262e; }\n\ntr:first-child {\n  border-top: none; }\n\ntr:last-child {\n  border-bottom: none; }\n\ntr:nth-child(odd) td {\n  background: #EBEBEB; }\n\ntr:nth-child(odd):hover td {\n  background: #4E5066; }\n\ntr:last-child td:first-child {\n  border-bottom-left-radius: 3px; }\n\ntr:last-child td:last-child {\n  border-bottom-right-radius: 3px; }\n\ntd {\n  background: #FFFFFF;\n  padding: 20px;\n  text-align: left;\n  vertical-align: middle;\n  font-weight: 300;\n  font-size: 18px;\n  text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);\n  border-right: 1px solid #C1C3D1; }\n\ntd:last-child {\n  border-right: 0px; }\n\nth.text-left {\n  text-align: left; }\n\nth.text-center {\n  text-align: center; }\n\nth.text-right {\n  text-align: right; }\n\ntd.text-left {\n  text-align: left; }\n\ntd.text-center {\n  text-align: center; }\n\ntd.text-right {\n  text-align: right; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0Rlc2t0b3AvUmF2aS9XTS93bS1mcm9udGVuZC93bS1mcm9udGVuZC9zcmMvYXBwL21vZHVsZXMvdmVuZG9yL3JlcG9ydC9yZXBvcnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBeUI7RUFDekIsbURBQW1EO0VBQ25ELGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0NBQWtDLEVBQUE7O0FBR3BDO0VBQ0csY0FBYztFQUNmLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsWUFBVztFQUNYLFdBQVcsRUFBQTs7QUFHYjtFQUNHLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGtCQUFpQjtFQUNqQixtREFBbUQ7RUFDbkQsNkNBQTZDO0VBQzdDLHlCQUF3QixFQUFBOztBQUczQjtFQUNFLGVBQWU7RUFDZixNQUFNO0VBQ04sT0FBTztFQUNQLGFBQWE7RUFDYixXQUFXO0VBQ1gsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixVQUFVLEVBQUE7O0FBR1osb0JBQUE7O0FBRUE7RUFDRSxpQkFBaUI7RUFDakIsa0JBQWlCO0VBQ2pCLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2IsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixZQUFXO0VBQ1gsV0FBVztFQUNYLHlDQUF5QztFQUN6QyxvQ0FBNEI7VUFBNUIsNEJBQTRCLEVBQUE7O0FBRzlCO0VBQ0UsY0FBYTtFQUNiLG1CQUFrQjtFQUNsQixnQ0FBK0I7RUFDL0IsK0JBQStCO0VBQy9CLGVBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsYUFBWTtFQUNaLGdCQUFlO0VBQ2YseUNBQXlDO0VBQ3pDLHNCQUFxQixFQUFBOztBQUd2QjtFQUNFLDJCQUEwQixFQUFBOztBQUc1QjtFQUNFLDRCQUEyQjtFQUMzQixrQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSw2QkFBNkI7RUFFN0IsY0FBYTtFQUNiLGVBQWM7RUFDZCxtQkFBa0I7RUFDbEIsK0NBQStDLEVBQUE7O0FBR2pEO0VBQ0UsbUJBQWtCO0VBQ2xCLGNBQWE7RUFDYiw2QkFBNkIsRUFBQTs7QUFHL0I7RUFDRSxnQkFBZSxFQUFBOztBQUdqQjtFQUNFLG1CQUFrQixFQUFBOztBQUdwQjtFQUNFLG1CQUFrQixFQUFBOztBQUdwQjtFQUNFLG1CQUFrQixFQUFBOztBQUdwQjtFQUNFLDhCQUE2QixFQUFBOztBQUcvQjtFQUNFLCtCQUE4QixFQUFBOztBQUdoQztFQUNFLG1CQUFrQjtFQUNsQixhQUFZO0VBQ1osZ0JBQWU7RUFDZixzQkFBcUI7RUFDckIsZ0JBQWU7RUFDZixlQUFjO0VBQ2QsNkNBQTZDO0VBQzdDLCtCQUErQixFQUFBOztBQUdqQztFQUNFLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLGlCQUFpQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy92ZW5kb3IvcmVwb3J0L3JlcG9ydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImJvZHkge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMzZTk0ZWM7XG4gICAgZm9udC1mYW1pbHk6IFwiUm9ib3RvXCIsIGhlbHZldGljYSwgYXJpYWwsIHNhbnMtc2VyaWY7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgdGV4dC1yZW5kZXJpbmc6IG9wdGltaXplTGVnaWJpbGl0eTtcbiAgfVxuICBcbiAgZGl2LnRhYmxlLXRpdGxlIHtcbiAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIG1heC13aWR0aDogNjAwcHg7XG4gICAgcGFkZGluZzo1cHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbiAgXG4gIC50YWJsZS10aXRsZSBoMyB7XG4gICAgIGNvbG9yOiAjZmFmYWZhO1xuICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgIGZvbnQtc3R5bGU6bm9ybWFsO1xuICAgICBmb250LWZhbWlseTogXCJSb2JvdG9cIiwgaGVsdmV0aWNhLCBhcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgdGV4dC1zaGFkb3c6IC0xcHggLTFweCAxcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgICB0ZXh0LXRyYW5zZm9ybTp1cHBlcmNhc2U7XG4gIH1cbiAgXG4gIC5tb2RhbC1jbGFzcyB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAwO1xuICAgIHotaW5kZXg6IDEwNTA7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG92ZXJmbG93OiBzY3JvbGw7XG4gICAgb3V0bGluZTogMDtcbn1cbiAgXG4gIC8qKiogVGFibGUgU3R5bGVzICoqL1xuICBcbiAgLnRhYmxlLWZpbGwge1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6M3B4O1xuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XG4gICAgaGVpZ2h0OiAzMjBweDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgbWF4LXdpZHRoOiA2MDBweDtcbiAgICBwYWRkaW5nOjVweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3gtc2hhZG93OiAwIDVweCAxMHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICBhbmltYXRpb246IGZsb2F0IDVzIGluZmluaXRlO1xuICB9XG4gICBcbiAgdGgge1xuICAgIGNvbG9yOiNENURERTU7O1xuICAgIGJhY2tncm91bmQ6IzFiMWUyNDtcbiAgICBib3JkZXItYm90dG9tOjRweCBzb2xpZCAjOWVhN2FmO1xuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICMzNDNhNDU7XG4gICAgZm9udC1zaXplOjIzcHg7XG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcbiAgICBwYWRkaW5nOjI0cHg7XG4gICAgdGV4dC1hbGlnbjpsZWZ0O1xuICAgIHRleHQtc2hhZG93OiAwIDFweCAxcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgIHZlcnRpY2FsLWFsaWduOm1pZGRsZTtcbiAgfVxuICBcbiAgdGg6Zmlyc3QtY2hpbGQge1xuICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6M3B4O1xuICB9XG4gICBcbiAgdGg6bGFzdC1jaGlsZCB7XG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6M3B4O1xuICAgIGJvcmRlci1yaWdodDpub25lO1xuICB9XG4gICAgXG4gIHRyIHtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI0MxQzNEMTtcbiAgICAvL2JvcmRlci1ib3R0b20tOiAxcHggc29saWQgI0MxQzNEMTtcbiAgICBjb2xvcjojNjY2Qjg1O1xuICAgIGZvbnQtc2l6ZToxNnB4O1xuICAgIGZvbnQtd2VpZ2h0Om5vcm1hbDtcbiAgICB0ZXh0LXNoYWRvdzogMCAxcHggMXB4IHJnYmEoMjU2LCAyNTYsIDI1NiwgMC4xKTtcbiAgfVxuICAgXG4gIHRyOmhvdmVyIHRkIHtcbiAgICBiYWNrZ3JvdW5kOiM0RTUwNjY7XG4gICAgY29sb3I6I0ZGRkZGRjtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgIzIyMjYyZTtcbiAgfVxuICAgXG4gIHRyOmZpcnN0LWNoaWxkIHtcbiAgICBib3JkZXItdG9wOm5vbmU7XG4gIH1cbiAgXG4gIHRyOmxhc3QtY2hpbGQge1xuICAgIGJvcmRlci1ib3R0b206bm9uZTtcbiAgfVxuICAgXG4gIHRyOm50aC1jaGlsZChvZGQpIHRkIHtcbiAgICBiYWNrZ3JvdW5kOiNFQkVCRUI7XG4gIH1cbiAgIFxuICB0cjpudGgtY2hpbGQob2RkKTpob3ZlciB0ZCB7XG4gICAgYmFja2dyb3VuZDojNEU1MDY2O1xuICB9XG4gIFxuICB0cjpsYXN0LWNoaWxkIHRkOmZpcnN0LWNoaWxkIHtcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOjNweDtcbiAgfVxuICAgXG4gIHRyOmxhc3QtY2hpbGQgdGQ6bGFzdC1jaGlsZCB7XG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6M3B4O1xuICB9XG4gICBcbiAgdGQge1xuICAgIGJhY2tncm91bmQ6I0ZGRkZGRjtcbiAgICBwYWRkaW5nOjIwcHg7XG4gICAgdGV4dC1hbGlnbjpsZWZ0O1xuICAgIHZlcnRpY2FsLWFsaWduOm1pZGRsZTtcbiAgICBmb250LXdlaWdodDozMDA7XG4gICAgZm9udC1zaXplOjE4cHg7XG4gICAgdGV4dC1zaGFkb3c6IC0xcHggLTFweCAxcHggcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNDMUMzRDE7XG4gIH1cbiAgXG4gIHRkOmxhc3QtY2hpbGQge1xuICAgIGJvcmRlci1yaWdodDogMHB4O1xuICB9XG4gIFxuICB0aC50ZXh0LWxlZnQge1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbiAgXG4gIHRoLnRleHQtY2VudGVyIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgXG4gIHRoLnRleHQtcmlnaHQge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICB9XG4gIFxuICB0ZC50ZXh0LWxlZnQge1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbiAgXG4gIHRkLnRleHQtY2VudGVyIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbiAgXG4gIHRkLnRleHQtcmlnaHQge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICB9XG4gICJdfQ== */"

/***/ }),

/***/ "./src/app/modules/vendor/report/report.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/modules/vendor/report/report.component.ts ***!
  \***********************************************************/
/*! exports provided: ReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportComponent", function() { return ReportComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../app.component */ "./src/app/app.component.ts");
/* harmony import */ var _vendor_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../vendor.service */ "./src/app/modules/vendor/vendor.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _config_web_urls__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../config/web-urls */ "./src/app/config/web-urls.ts");






var ReportComponent = /** @class */ (function () {
    function ReportComponent(vendorService, router) {
        this.vendorService = vendorService;
        this.router = router;
        this.paginationObject = {
            filterBy: '',
            searchData: '',
            totalItems: '',
            itemsPerPage: 10,
            currentPage: 1,
            startDate: null,
            endDate: null,
            getAllData: false
        };
        this.pageName = 'Report';
        this.showButton = true;
        this.items = [];
        this.showTable = false;
        this.allImportTrans = [];
        this.allProduct = [];
        this.submitted = false;
        this.showTransactionDetails = false;
        this.showConfirmModal = false;
    }
    ReportComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var temp;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, localStorage.getItem('VendorData')];
                    case 1:
                        temp = _a.sent();
                        this.vendorData = JSON.parse(temp);
                        console.log('warehouse data in vendor', this.vendorData);
                        if (!this.vendorData) {
                            localStorage.clear();
                            this.router.navigate(['dw-home']);
                            return [2 /*return*/];
                        }
                        this.getImportTransactions();
                        this.getProducts();
                        this.actions = {
                            isDelete: false,
                            isEdit: false,
                            isDetailsView: true
                        };
                        return [2 /*return*/];
                }
            });
        });
    };
    ReportComponent.prototype.getImportTransactions = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _app_component__WEBPACK_IMPORTED_MODULE_2__["LoaderState$"].next(true);
                        params = {
                            transactionId: '',
                            warehouseId: this.vendorData.warehouse._id,
                            vendorId: this.vendorData._id,
                            productId: (this.selectedProduct != 'Select' && this.selectedProduct != undefined) ? this.selectedProduct : '',
                            paginationObject: JSON.stringify(this.paginationObject)
                        };
                        console.log('params in get import', params);
                        return [4 /*yield*/, this.vendorService.getTransaction$(_config_web_urls__WEBPACK_IMPORTED_MODULE_5__["GET_TRANSACTION"], params).subscribe({
                                next: function (results) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_2__["LoaderState$"].next(false);
                                    console.log('get import trans result', results);
                                    _this.items = [];
                                    if (results.data.result.length > 0) {
                                        _this.allImportTrans = results.data.result;
                                        results.data.result.forEach(function (element) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                                            var importObj, _a, _b;
                                            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_c) {
                                                switch (_c.label) {
                                                    case 0:
                                                        _a = {
                                                            id: element._id
                                                        };
                                                        _b = 'Product/Qty';
                                                        return [4 /*yield*/, this.getProductName(element.productDetails)];
                                                    case 1:
                                                        importObj = (_a[_b] = _c.sent(),
                                                            _a.Vendor = element.vendor.name,
                                                            _a['DO Number'] = element.DONumber,
                                                            _a['Vehicle Number'] = element.vehicleNumber,
                                                            _a.Date = new Date(element.createdAt).toLocaleDateString(),
                                                            _a['Bill Type'] = element.actionType,
                                                            _a);
                                                        this.items.push(importObj);
                                                        return [2 /*return*/];
                                                }
                                            });
                                        }); });
                                        _this.paginationObject.totalItems = results.data.count;
                                        _this.showTable = true;
                                    }
                                    else {
                                        _this.items = [];
                                        _this.showTable = false;
                                    }
                                }, error: function (error) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_2__["LoaderState$"].next(false);
                                    if (error.status === 404) {
                                        _app_component__WEBPACK_IMPORTED_MODULE_2__["PopUpState$"].next({
                                            state: 'error',
                                            message: error.error.message ? error.error.message : 'No past transaction'
                                        });
                                    }
                                    console.log('error in get transaction', error);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ReportComponent.prototype.getProductName = function (productData) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var tagStr, _i, productData_1, element;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                tagStr = '';
                console.log('tagStr 111', Array.isArray(productData));
                if (!Array.isArray(productData)) {
                    tagStr = tagStr + ' ' + productData.name + ' (' + productData.quantity + ')';
                }
                else {
                    for (_i = 0, productData_1 = productData; _i < productData_1.length; _i++) {
                        element = productData_1[_i];
                        if (tagStr !== '') {
                            tagStr = tagStr + ', ';
                        }
                        console.log('in loop get name');
                        tagStr = tagStr + ' ' + element.name + ' (' + element.quantity + ')';
                    }
                }
                console.log('tagStr', tagStr);
                return [2 /*return*/, tagStr];
            });
        });
    };
    ReportComponent.prototype.changePage = function (event) {
        this.paginationObject.currentPage = event;
    };
    ReportComponent.prototype.disableDetailsModal = function () {
        this.showTransactionDetails = false;
    };
    ReportComponent.prototype.showUpdateModal = function (event) {
        console.log('in show details view', event);
        var index = this.allImportTrans.findIndex(function (ind) { return ind._id === event.id; });
        this.updateDetails = this.allImportTrans[index];
        console.log('this.updateDetails -->', this.updateDetails);
        this.invoiceHeader = (this.updateDetails.actionType === 'import') ? 'Import Transaction Invoice' : 'Export Transaction Invoice';
        this.showTransactionDetails = true;
    };
    ReportComponent.prototype.getProducts = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            productId: '',
                            warehouseId: this.vendorData.warehouse._id,
                            packageTypeId: '',
                            vendorId: this.vendorData._id
                        };
                        console.log('params get product', params);
                        return [4 /*yield*/, this.vendorService.getProducts$(_config_web_urls__WEBPACK_IMPORTED_MODULE_5__["GET_PRODUCT"], params).subscribe({
                                next: function (results) {
                                    console.log('get product result', results);
                                    if (results.data.result.length > 0) {
                                        _this.allProduct = results.data.result;
                                    }
                                    else {
                                        _this.allProduct = [];
                                    }
                                }, error: function (error) {
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ReportComponent.prototype.logout = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, localStorage.clear()];
                    case 1:
                        _a.sent();
                        this.router.navigate(['dw-home']);
                        return [2 /*return*/];
                }
            });
        });
    };
    ReportComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-report',
            template: __webpack_require__(/*! ./report.component.html */ "./src/app/modules/vendor/report/report.component.html"),
            styles: [__webpack_require__(/*! ../../../app.component.scss */ "./src/app/app.component.scss"), __webpack_require__(/*! ./report.component.scss */ "./src/app/modules/vendor/report/report.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_vendor_service__WEBPACK_IMPORTED_MODULE_3__["VendorService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], ReportComponent);
    return ReportComponent;
}());



/***/ }),

/***/ "./src/app/modules/vendor/vendor-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/modules/vendor/vendor-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: VendorRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendorRoutingModule", function() { return VendorRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _report_report_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./report/report.component */ "./src/app/modules/vendor/report/report.component.ts");




var routes = [
    {
        path: '',
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'report',
            },
            { path: 'report', component: _report_report_component__WEBPACK_IMPORTED_MODULE_3__["ReportComponent"] },
        ]
    }
];
var VendorRoutingModule = /** @class */ (function () {
    function VendorRoutingModule() {
    }
    VendorRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], VendorRoutingModule);
    return VendorRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/vendor/vendor.module.ts":
/*!*************************************************!*\
  !*** ./src/app/modules/vendor/vendor.module.ts ***!
  \*************************************************/
/*! exports provided: VendorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendorModule", function() { return VendorModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _vendor_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./vendor-routing.module */ "./src/app/modules/vendor/vendor-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _report_report_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./report/report.component */ "./src/app/modules/vendor/report/report.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/modules/shared/shared.module.ts");
/* harmony import */ var _vendor_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./vendor.service */ "./src/app/modules/vendor/vendor.service.ts");
/* harmony import */ var _store_store_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../store/store.service */ "./src/app/store/store.service.ts");










var VendorModule = /** @class */ (function () {
    function VendorModule() {
    }
    VendorModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_report_report_component__WEBPACK_IMPORTED_MODULE_6__["ReportComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_5__["NgMultiSelectDropDownModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
                _vendor_routing_module__WEBPACK_IMPORTED_MODULE_3__["VendorRoutingModule"]
            ],
            providers: [_vendor_service__WEBPACK_IMPORTED_MODULE_8__["VendorService"], _store_store_service__WEBPACK_IMPORTED_MODULE_9__["StoreService"]]
        })
    ], VendorModule);
    return VendorModule;
}());



/***/ }),

/***/ "./src/app/modules/vendor/vendor.service.ts":
/*!**************************************************!*\
  !*** ./src/app/modules/vendor/vendor.service.ts ***!
  \**************************************************/
/*! exports provided: VendorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendorService", function() { return VendorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _config_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../config/app-config */ "./src/app/config/app-config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");




var VendorService = /** @class */ (function () {
    function VendorService(http) {
        this.http = http;
    }
    // API related to Vendor  -------- Started -----------------------------
    VendorService.prototype.getVendor$ = function (url, params) {
        console.log(url, 'url');
        console.log(_config_app_config__WEBPACK_IMPORTED_MODULE_2__["BASE_URL"], 'BASE_URL');
        console.log('params', params);
        var accessToken = localStorage.getItem('access_token');
        var httpOptions;
        var httpParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
            .set('warehouseId', params.warehouseId)
            .set('vendorId', params.vendorId);
        if (accessToken) {
            httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get(_config_app_config__WEBPACK_IMPORTED_MODULE_2__["BASE_URL"] + url, httpOptions);
    };
    VendorService.prototype.getTransaction$ = function (url, params) {
        console.log(url, 'url');
        console.log(_config_app_config__WEBPACK_IMPORTED_MODULE_2__["BASE_URL"], 'BASE_URL');
        console.log('params', params);
        var accessToken = localStorage.getItem('access_token');
        var httpOptions;
        var httpParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
            .set('transactionId', params.transactionId)
            .set('warehouseId', params.warehouseId)
            .set('vendorId', params.vendorId)
            .set('productId', params.productId)
            .set('paginationObject', params.paginationObject);
        if (accessToken) {
            httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get(_config_app_config__WEBPACK_IMPORTED_MODULE_2__["BASE_URL"] + url, httpOptions);
    };
    VendorService.prototype.getProducts$ = function (url, params) {
        console.log(url, 'url');
        console.log(_config_app_config__WEBPACK_IMPORTED_MODULE_2__["BASE_URL"], 'BASE_URL');
        console.log('params', params);
        var accessToken = localStorage.getItem('access_token');
        var httpOptions;
        var httpParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]()
            .set('productId', params.productId)
            .set('warehouseId', params.warehouseId)
            .set('packageTypeId', params.packageTypeId)
            .set('vendorId', params.vendorId);
        if (accessToken) {
            httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                }),
                params: httpParams
            };
        }
        return this.http.get(_config_app_config__WEBPACK_IMPORTED_MODULE_2__["BASE_URL"] + url, httpOptions);
    };
    VendorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], VendorService);
    return VendorService;
}());



/***/ })

}]);
//# sourceMappingURL=modules-vendor-vendor-module.js.map