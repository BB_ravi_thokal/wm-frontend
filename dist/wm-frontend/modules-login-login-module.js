(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-login-login-module"],{

/***/ "./src/app/modules/login/change-password/change-password.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/modules/login/change-password/change-password.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto\">\n    <div class=\"card card0 border-0\">\n       <div class=\"row d-flex\">\n          <div class=\"col-lg-6\">\n             <div class=\"card1 pb-5\">\n                <div class=\"row\"> <img src=\"https://i.imgur.com/CXQmsmF.png\" class=\"logo\"> </div>\n                <div class=\"row px-3 justify-content-center mt-4 mb-5 border-line\"> <img src=\"https://i.imgur.com/uNGdWHi.png\" class=\"image\"> </div>\n             </div>\n          </div>\n          <div class=\"col-lg-6\">\n             <div class=\"card2 card border-0 px-4 py-5\">\n                \n                <form class=\"form-signin\" [formGroup]=\"changePassForm\">\n                    <div class=\"row mb-4 px-2 py-2\">\n                        <h2>Set your own password</h2>\n                     </div>\n                    <div class=\"form-group row px-3\">\n                       <label class=\"mb-1\">\n                          <h6 class=\"mb-0 text-sm\">Email Id</h6>\n                       </label>\n                       <input formControlName=\"emailId\" [ngClass]=\"{ 'is-invalid': submitted && f.emailId.errors }\" type=\"text\"  class=\"form-control\"  id=\"email\" placeholder=\"Enter a valid email address\"> \n                       <div *ngIf=\"submitted && f.emailId.errors\" class=\"invalid-feedback\">\n                          <div *ngIf=\"f.emailId.errors.required\">User id is required</div>\n                          <div *ngIf=\"f.emailId.errors.pattern\">Please enter valid email id</div>\n                       </div>\n                       \n                    </div>\n                    <div class=\"form-group row px-3\">\n                       <label class=\"mb-1\">\n                          <h6 class=\"mb-0 text-sm\">Temporary Password</h6>\n                       </label>\n                       <input formControlName=\"tempPassword\" [ngClass]=\"{ 'is-invalid': submitted && f.tempPassword.errors }\" class=\"input form-control\" type=\"password\" name=\"tempPassword\" placeholder=\"Enter password\"> \n                       <div *ngIf=\"submitted && f.tempPassword.errors\" class=\"invalid-feedback\">\n                          <div *ngIf=\"f.tempPassword.errors.required\">Password is required</div>\n                       </div>\n                    </div>\n                    <div class=\"form-group row px-3\">\n                        <label class=\"mb-1\">\n                           <h6 class=\"mb-0 text-sm\">New Password</h6>\n                        </label>\n                        <input formControlName=\"newPassword\" [ngClass]=\"{ 'is-invalid': submitted && f.newPassword.errors }\" class=\"input form-control\" type=\"password\" name=\"newPassword\" placeholder=\"Enter password\"> \n                        <div *ngIf=\"submitted && f.newPassword.errors\" class=\"invalid-feedback\">\n                           <div *ngIf=\"f.newPassword.errors.required\">Password is required</div>\n                        </div>\n                     </div>\n                    <div class=\"row mb-3 px-3\"> <button type=\"submit\" class=\"btn btn-blue text-center\" (click)=\"login()\">Change</button> </div>\n                    <!-- <div class=\"row mb-4 px-3\"> <small class=\"font-weight-bold\">Don't have an account? <a class=\"text-danger \">Register</a></small> </div> -->\n                </form>\n                \n             </div>\n          </div>\n       </div>\n    </div>\n </div>"

/***/ }),

/***/ "./src/app/modules/login/change-password/change-password.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/modules/login/change-password/change-password.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media screen and (max-width: 991px) {\n  .card2 {\n    margin: 0px 0px !important; } }\n\n.card2 {\n  margin: 0px 0px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0Rlc2t0b3AvUmF2aS9XTS93bS1mcm9udGVuZC93bS1mcm9udGVuZC9zcmMvYXBwL21vZHVsZXMvbG9naW4vY2hhbmdlLXBhc3N3b3JkL2NoYW5nZS1wYXNzd29yZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJO0lBQ0ksMEJBQ0osRUFBQSxFQUFDOztBQUdMO0VBQ0ksMEJBQ0osRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvbG9naW4vY2hhbmdlLXBhc3N3b3JkL2NoYW5nZS1wYXNzd29yZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gICAgLmNhcmQyIHtcbiAgICAgICAgbWFyZ2luOiAwcHggMHB4ICFpbXBvcnRhbnRcbiAgICB9XG59XG5cbi5jYXJkMiB7XG4gICAgbWFyZ2luOiAwcHggMHB4ICFpbXBvcnRhbnRcbn0iXX0= */"

/***/ }),

/***/ "./src/app/modules/login/change-password/change-password.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/modules/login/change-password/change-password.component.ts ***!
  \****************************************************************************/
/*! exports provided: ChangePasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordComponent", function() { return ChangePasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../login.service */ "./src/app/modules/login/login.service.ts");
/* harmony import */ var _config_web_urls__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../config/web-urls */ "./src/app/config/web-urls.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var ChangePasswordComponent = /** @class */ (function () {
    function ChangePasswordComponent(formBuilder, loginService, router) {
        this.formBuilder = formBuilder;
        this.loginService = loginService;
        this.router = router;
        this.submitted = false;
    }
    ChangePasswordComponent.prototype.ngOnInit = function () {
        this.formValidation();
    };
    Object.defineProperty(ChangePasswordComponent.prototype, "f", {
        get: function () { return this.changePassForm.controls; },
        enumerable: true,
        configurable: true
    });
    ChangePasswordComponent.prototype.formValidation = function () {
        this.changePassForm = this.formBuilder.group({
            emailId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
            ])),
            tempPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ])),
            newPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
            ]))
        });
    };
    ChangePasswordComponent.prototype.login = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(true);
                        this.submitted = true;
                        console.log('this.changePassForm.valid', this.changePassForm.get('emailId').value);
                        this.submitted = true;
                        if (!this.changePassForm.valid) return [3 /*break*/, 2];
                        console.log('emailId ===>', this.changePassForm.get('emailId').value);
                        console.log('tempPassword ===>', this.changePassForm.get('tempPassword').value);
                        console.log('newPassword ===>', this.changePassForm.get('newPassword').value);
                        data = {
                            emailId: this.changePassForm.get('emailId').value,
                            temporaryPassword: this.changePassForm.get('tempPassword').value,
                            newPassword: this.changePassForm.get('newPassword').value
                        };
                        return [4 /*yield*/, this.loginService.login$(_config_web_urls__WEBPACK_IMPORTED_MODULE_5__["CHANGE_PASSWORD"], data).subscribe({
                                next: function (results) {
                                    console.log('change password success', results);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["PopUpState$"].next({
                                        state: 'success',
                                        message: 'Password change successfully'
                                    });
                                    _this.router.navigate(['dw-home']);
                                }, error: function (error) {
                                    console.log('change password success', error);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["PopUpState$"].next({
                                        state: 'error',
                                        message: error.error.message ? error.error.message : 'Not able to connect with server please try later'
                                    });
                                }
                            })];
                    case 1:
                        _a.sent();
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                        return [3 /*break*/, 3];
                    case 2:
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                        return [2 /*return*/];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ChangePasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-change-password',
            template: __webpack_require__(/*! ./change-password.component.html */ "./src/app/modules/login/change-password/change-password.component.html"),
            styles: [__webpack_require__(/*! ../../../app.component.scss */ "./src/app/app.component.scss"), __webpack_require__(/*! ../login.component.scss */ "./src/app/modules/login/login.component.scss"), __webpack_require__(/*! ./change-password.component.scss */ "./src/app/modules/login/change-password/change-password.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], ChangePasswordComponent);
    return ChangePasswordComponent;
}());



/***/ }),

/***/ "./src/app/modules/login/forgot-password/forgot-password.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/modules/login/forgot-password/forgot-password.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto\">\n    <div class=\"card card0 border-0\">\n       <div class=\"row d-flex\">\n          <div class=\"col-lg-6\">\n             <div class=\"card1 pb-5\">\n                <div class=\"row\"> <img src=\"https://i.imgur.com/CXQmsmF.png\" class=\"logo\"> </div>\n                <div class=\"row px-3 justify-content-center mt-4 mb-5 border-line\"> <img src=\"https://i.imgur.com/uNGdWHi.png\" class=\"image\"> </div>\n             </div>\n          </div>\n          <div class=\"col-lg-6\">\n            <div class=\"card2 card border-0 px-4 py-5\">\n              <form class=\"form-signin\" [formGroup]=\"getOTPForm\">\n                <div class=\"row mb-4 px-2 py-2\">\n                    <h2>Enter your registered email to get temporary password</h2>\n                </div>\n                <div class=\"form-group row px-3\">\n                    <label class=\"mb-1\">\n                        <h6 class=\"mb-0 text-sm\">Email Id</h6>\n                    </label>\n                    <input formControlName=\"emailId\" [ngClass]=\"{ 'is-invalid': submitted && f.emailId.errors }\" type=\"text\"  class=\"form-control\"  id=\"email\" placeholder=\"Enter a valid email address\"> \n                    <div *ngIf=\"submitted && f.emailId.errors\" class=\"invalid-feedback\">\n                        <div *ngIf=\"f.emailId.errors.required\">Email id is required</div>\n                        <div *ngIf=\"f.emailId.errors.pattern\">Please enter valid email id</div>\n                    </div>\n                </div>\n                <div class=\"row px-3 mb-4\">\n                    <div class=\"custom-control custom-checkbox custom-control-inline\"> <input id=\"chk1\" type=\"checkbox\" name=\"chk\" class=\"custom-control-input\"> </div>\n                    <a class=\"ml-auto mb-0 text-sm\">Forgot register email?</a>\n                </div>\n                <div class=\"row mb-3 px-3\"> <button type=\"submit\" class=\"btn btn-blue text-center\" (click)=\"login()\">Get Temporary Password</button> </div>\n              </form>\n            </div>\n          </div>\n       </div>\n    </div>\n </div>"

/***/ }),

/***/ "./src/app/modules/login/forgot-password/forgot-password.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/modules/login/forgot-password/forgot-password.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media screen and (max-width: 991px) {\n  .card2 {\n    margin: 0px 0px !important; } }\n\n.card2 {\n  margin: 0px 0px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0Rlc2t0b3AvUmF2aS9XTS93bS1mcm9udGVuZC93bS1mcm9udGVuZC9zcmMvYXBwL21vZHVsZXMvbG9naW4vZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJO0lBQ0ksMEJBQ0osRUFBQSxFQUFDOztBQUdMO0VBQ0ksMEJBQ0osRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvbG9naW4vZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gICAgLmNhcmQyIHtcbiAgICAgICAgbWFyZ2luOiAwcHggMHB4ICFpbXBvcnRhbnRcbiAgICB9XG59XG5cbi5jYXJkMiB7XG4gICAgbWFyZ2luOiAwcHggMHB4ICFpbXBvcnRhbnRcbn0iXX0= */"

/***/ }),

/***/ "./src/app/modules/login/forgot-password/forgot-password.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/modules/login/forgot-password/forgot-password.component.ts ***!
  \****************************************************************************/
/*! exports provided: ForgotPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordComponent", function() { return ForgotPasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../login.service */ "./src/app/modules/login/login.service.ts");
/* harmony import */ var _config_web_urls__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../config/web-urls */ "./src/app/config/web-urls.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var ForgotPasswordComponent = /** @class */ (function () {
    function ForgotPasswordComponent(formBuilder, loginService, router) {
        this.formBuilder = formBuilder;
        this.loginService = loginService;
        this.router = router;
        this.submitted = false;
    }
    ForgotPasswordComponent.prototype.ngOnInit = function () {
        this.formValidation();
    };
    Object.defineProperty(ForgotPasswordComponent.prototype, "f", {
        get: function () { return this.getOTPForm.controls; },
        enumerable: true,
        configurable: true
    });
    ForgotPasswordComponent.prototype.formValidation = function () {
        this.getOTPForm = this.formBuilder.group({
            emailId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
            ]))
        });
    };
    ForgotPasswordComponent.prototype.login = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(true);
                        this.submitted = true;
                        console.log('this.getOTPForm.valid', this.getOTPForm.get('emailId').value);
                        this.submitted = true;
                        if (!this.getOTPForm.valid) return [3 /*break*/, 2];
                        data = {
                            emailId: this.getOTPForm.get('emailId').value
                        };
                        return [4 /*yield*/, this.loginService.login$(_config_web_urls__WEBPACK_IMPORTED_MODULE_5__["FORGOT_PASSWORD"], data).subscribe({
                                next: function (results) {
                                    console.log('forgot password result', results);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["PopUpState$"].next({
                                        state: 'success',
                                        message: 'Temporary password sent on your email.'
                                    });
                                    _this.router.navigate(['login/change-password']);
                                }, error: function (error) {
                                    console.log('forgot password result', error);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["PopUpState$"].next({
                                        state: 'error',
                                        message: error.error.message ? error.error.message : 'Not able to connect with server please try later'
                                    });
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                        return [2 /*return*/];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ForgotPasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-forgot-password',
            template: __webpack_require__(/*! ./forgot-password.component.html */ "./src/app/modules/login/forgot-password/forgot-password.component.html"),
            styles: [__webpack_require__(/*! ../../../app.component.scss */ "./src/app/app.component.scss"), __webpack_require__(/*! ../login.component.scss */ "./src/app/modules/login/login.component.scss"), __webpack_require__(/*! ./forgot-password.component.scss */ "./src/app/modules/login/forgot-password/forgot-password.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], ForgotPasswordComponent);
    return ForgotPasswordComponent;
}());



/***/ }),

/***/ "./src/app/modules/login/login-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/modules/login/login-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: LoginRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginRoutingModule", function() { return LoginRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _vendor_login_vendor_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./vendor-login/vendor-login.component */ "./src/app/modules/login/vendor-login/vendor-login.component.ts");
/* harmony import */ var _warehouse_login_warehouse_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./warehouse-login/warehouse-login.component */ "./src/app/modules/login/warehouse-login/warehouse-login.component.ts");
/* harmony import */ var _warehouse_signup_warehouse_signup_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./warehouse-signup/warehouse-signup.component */ "./src/app/modules/login/warehouse-signup/warehouse-signup.component.ts");
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ "./src/app/modules/login/forgot-password/forgot-password.component.ts");
/* harmony import */ var _change_password_change_password_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./change-password/change-password.component */ "./src/app/modules/login/change-password/change-password.component.ts");








var routes = [
    {
        path: '',
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'warehouse-login',
            },
            { path: 'warehouse-login', component: _warehouse_login_warehouse_login_component__WEBPACK_IMPORTED_MODULE_4__["WarehouseLoginComponent"] },
            { path: 'vendor-login', component: _vendor_login_vendor_login_component__WEBPACK_IMPORTED_MODULE_3__["VendorLoginComponent"] },
            { path: 'warehouse-sign', component: _warehouse_signup_warehouse_signup_component__WEBPACK_IMPORTED_MODULE_5__["WarehouseSignupComponent"] },
            { path: 'forgot-password', component: _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordComponent"] },
            { path: 'change-password', component: _change_password_change_password_component__WEBPACK_IMPORTED_MODULE_7__["ChangePasswordComponent"] }
        ]
    }
];
var LoginRoutingModule = /** @class */ (function () {
    function LoginRoutingModule() {
    }
    LoginRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], LoginRoutingModule);
    return LoginRoutingModule;
}());



/***/ }),

/***/ "./src/app/modules/login/login.component.scss":
/*!****************************************************!*\
  !*** ./src/app/modules/login/login.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card0 {\n  box-shadow: 0px 4px 8px 0px #757575;\n  border-radius: 0px; }\n\n.card2 {\n  margin: 0px 40px; }\n\n.logo {\n  width: 200px;\n  height: 100px;\n  margin-top: 20px;\n  margin-left: 35px; }\n\n.image {\n  width: 360px;\n  height: 280px; }\n\n.border-line {\n  border-right: 1px solid #EEEEEE; }\n\n.facebook {\n  background-color: #3b5998;\n  color: #fff;\n  font-size: 18px;\n  padding-top: 5px;\n  border-radius: 50%;\n  width: 35px;\n  height: 35px;\n  cursor: pointer; }\n\n.twitter {\n  background-color: #1DA1F2;\n  color: #fff;\n  font-size: 18px;\n  padding-top: 5px;\n  border-radius: 50%;\n  width: 35px;\n  height: 35px;\n  cursor: pointer; }\n\n.linkedin {\n  background-color: #2867B2;\n  color: #fff;\n  font-size: 18px;\n  padding-top: 5px;\n  border-radius: 50%;\n  width: 35px;\n  height: 35px;\n  cursor: pointer; }\n\n.line {\n  height: 1px;\n  width: 45%;\n  background-color: #E0E0E0;\n  margin-top: 10px; }\n\n.or {\n  width: 10%;\n  font-weight: bold; }\n\n@media screen and (max-width: 991px) {\n  .logo {\n    margin-left: 0px; }\n  .image {\n    width: 300px;\n    height: 220px; }\n  .border-line {\n    border-right: none; }\n  .card2 {\n    border-top: 1px solid #EEEEEE !important;\n    margin: 0px 15px; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0Rlc2t0b3AvUmF2aS9XTS93bS1mcm9udGVuZC93bS1mcm9udGVuZC9zcmMvYXBwL21vZHVsZXMvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBUUE7RUFDSSxtQ0FBbUM7RUFDbkMsa0JBQ0osRUFBQTs7QUFFQTtFQUNJLGdCQUNKLEVBQUE7O0FBRUE7RUFDSSxZQUFZO0VBQ1osYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixpQkFDSixFQUFBOztBQUVBO0VBQ0ksWUFBWTtFQUNaLGFBQ0osRUFBQTs7QUFFQTtFQUNJLCtCQUNKLEVBQUE7O0FBRUE7RUFDSSx5QkFBeUI7RUFDekIsV0FBVztFQUNYLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxZQUFZO0VBQ1osZUFDSixFQUFBOztBQUVBO0VBQ0kseUJBQXlCO0VBQ3pCLFdBQVc7RUFDWCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsWUFBWTtFQUNaLGVBQ0osRUFBQTs7QUFFQTtFQUNJLHlCQUF5QjtFQUN6QixXQUFXO0VBQ1gsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7RUFDWixlQUNKLEVBQUE7O0FBRUE7RUFDSSxXQUFXO0VBQ1gsVUFBVTtFQUNWLHlCQUF5QjtFQUN6QixnQkFDSixFQUFBOztBQUVBO0VBQ0ksVUFBVTtFQUNWLGlCQUNKLEVBQUE7O0FBR0E7RUFDSTtJQUNJLGdCQUNKLEVBQUE7RUFFQTtJQUNJLFlBQVk7SUFDWixhQUNKLEVBQUE7RUFFQTtJQUNJLGtCQUNKLEVBQUE7RUFFQTtJQUNJLHdDQUF3QztJQUN4QyxnQkFDSixFQUFBLEVBQUMiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gYm9keSB7XG4vLyAgICAgY29sb3I6ICMwMDA7XG4vLyAgICAgb3ZlcmZsb3cteDogaGlkZGVuO1xuLy8gICAgIGhlaWdodDogMTAwJTtcbi8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjQjBCRUM1O1xuLy8gICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXRcbi8vIH1cblxuLmNhcmQwIHtcbiAgICBib3gtc2hhZG93OiAwcHggNHB4IDhweCAwcHggIzc1NzU3NTtcbiAgICBib3JkZXItcmFkaXVzOiAwcHhcbn1cblxuLmNhcmQyIHtcbiAgICBtYXJnaW46IDBweCA0MHB4XG59XG5cbi5sb2dvIHtcbiAgICB3aWR0aDogMjAwcHg7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAzNXB4XG59XG5cbi5pbWFnZSB7XG4gICAgd2lkdGg6IDM2MHB4O1xuICAgIGhlaWdodDogMjgwcHhcbn1cblxuLmJvcmRlci1saW5lIHtcbiAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjRUVFRUVFXG59XG5cbi5mYWNlYm9vayB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNiNTk5ODtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgcGFkZGluZy10b3A6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgd2lkdGg6IDM1cHg7XG4gICAgaGVpZ2h0OiAzNXB4O1xuICAgIGN1cnNvcjogcG9pbnRlclxufVxuXG4udHdpdHRlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzFEQTFGMjtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgcGFkZGluZy10b3A6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgd2lkdGg6IDM1cHg7XG4gICAgaGVpZ2h0OiAzNXB4O1xuICAgIGN1cnNvcjogcG9pbnRlclxufVxuXG4ubGlua2VkaW4ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMyODY3QjI7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIHBhZGRpbmctdG9wOiA1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIHdpZHRoOiAzNXB4O1xuICAgIGhlaWdodDogMzVweDtcbiAgICBjdXJzb3I6IHBvaW50ZXJcbn1cblxuLmxpbmUge1xuICAgIGhlaWdodDogMXB4O1xuICAgIHdpZHRoOiA0NSU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0UwRTBFMDtcbiAgICBtYXJnaW4tdG9wOiAxMHB4XG59XG5cbi5vciB7XG4gICAgd2lkdGg6IDEwJTtcbiAgICBmb250LXdlaWdodDogYm9sZFxufVxuXG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gICAgLmxvZ28ge1xuICAgICAgICBtYXJnaW4tbGVmdDogMHB4XG4gICAgfVxuXG4gICAgLmltYWdlIHtcbiAgICAgICAgd2lkdGg6IDMwMHB4O1xuICAgICAgICBoZWlnaHQ6IDIyMHB4XG4gICAgfVxuXG4gICAgLmJvcmRlci1saW5lIHtcbiAgICAgICAgYm9yZGVyLXJpZ2h0OiBub25lXG4gICAgfVxuXG4gICAgLmNhcmQyIHtcbiAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNFRUVFRUUgIWltcG9ydGFudDtcbiAgICAgICAgbWFyZ2luOiAwcHggMTVweFxuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/modules/login/login.module.ts":
/*!***********************************************!*\
  !*** ./src/app/modules/login/login.module.ts ***!
  \***********************************************/
/*! exports provided: LoginModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginModule", function() { return LoginModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/modules/login/login-routing.module.ts");
/* harmony import */ var _vendor_login_vendor_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./vendor-login/vendor-login.component */ "./src/app/modules/login/vendor-login/vendor-login.component.ts");
/* harmony import */ var _warehouse_login_warehouse_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./warehouse-login/warehouse-login.component */ "./src/app/modules/login/warehouse-login/warehouse-login.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _warehouse_signup_warehouse_signup_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./warehouse-signup/warehouse-signup.component */ "./src/app/modules/login/warehouse-signup/warehouse-signup.component.ts");
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ "./src/app/modules/login/forgot-password/forgot-password.component.ts");
/* harmony import */ var _change_password_change_password_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./change-password/change-password.component */ "./src/app/modules/login/change-password/change-password.component.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./login.service */ "./src/app/modules/login/login.service.ts");
/* harmony import */ var _store_store_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../store/store.service */ "./src/app/store/store.service.ts");












var LoginModule = /** @class */ (function () {
    function LoginModule() {
    }
    LoginModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _vendor_login_vendor_login_component__WEBPACK_IMPORTED_MODULE_4__["VendorLoginComponent"],
                _warehouse_login_warehouse_login_component__WEBPACK_IMPORTED_MODULE_5__["WarehouseLoginComponent"],
                _warehouse_signup_warehouse_signup_component__WEBPACK_IMPORTED_MODULE_7__["WarehouseSignupComponent"],
                _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_8__["ForgotPasswordComponent"],
                _change_password_change_password_component__WEBPACK_IMPORTED_MODULE_9__["ChangePasswordComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                _login_routing_module__WEBPACK_IMPORTED_MODULE_3__["LoginRoutingModule"]
            ],
            providers: [_login_service__WEBPACK_IMPORTED_MODULE_10__["LoginService"], _store_store_service__WEBPACK_IMPORTED_MODULE_11__["StoreService"]],
        })
    ], LoginModule);
    return LoginModule;
}());



/***/ }),

/***/ "./src/app/modules/login/login.service.ts":
/*!************************************************!*\
  !*** ./src/app/modules/login/login.service.ts ***!
  \************************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_webApi_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/webApi.service */ "./src/app/services/webApi.service.ts");
/* harmony import */ var _config_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../config/app-config */ "./src/app/config/app-config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");





var LoginService = /** @class */ (function () {
    function LoginService(webService, http) {
        this.webService = webService;
        this.http = http;
    }
    /**
     * This method is for Post Api calls
     * @param url String Api URL
     * @param requestBody Object required for the Post request
     * @returns Return the respective response from the Apis.
     */
    LoginService.prototype.login$ = function (url, requestBody) {
        console.log(url, 'url');
        console.log(_config_app_config__WEBPACK_IMPORTED_MODULE_3__["BASE_URL"], 'BASE_URL');
        console.log(requestBody, 'requestBody');
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.post(_config_app_config__WEBPACK_IMPORTED_MODULE_3__["BASE_URL"] + url, requestBody, httpOptions);
    };
    LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_webApi_service__WEBPACK_IMPORTED_MODULE_2__["WebService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/modules/login/vendor-login/vendor-login.component.html":
/*!************************************************************************!*\
  !*** ./src/app/modules/login/vendor-login/vendor-login.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto\">\n    <div class=\"card card0 border-0\">\n       <div class=\"row d-flex\">\n          <div class=\"col-lg-6\">\n            <div class=\"card1 pb-5\">\n               <div class=\"row\"> <img src=\"../../../../assets/img/wmlogowithtitle3.png\" class=\"logo\"> </div>\n               <div class=\"row px-3 justify-content-center mt-4 mb-5 border-line\"> <img\n                       src=\"../../../../assets/img/wlogin.png\" class=\"image\"> </div>\n           </div>\n          </div>\n          <div class=\"col-lg-6\">\n             <div class=\"card2 card border-0 px-4 py-5\">\n                \n                <form class=\"form-signin\" [formGroup]=\"loginForm\">\n                    <div class=\"row mb-4 px-2 py-2\">\n                        <h2>Login to your account</h2>\n                     </div>\n                    <!-- <div class=\"dropdown row px-3 mb-4\">\n                        <button type=\"button\" class=\"btn-dropDown btn-blue dropdown-toggle\" data-toggle=\"dropdown\" formControlName=\"warehouse\">\n                          {{selectedWareHouse}}\n                        </button>\n                        <div class=\"dropdown-menu\">\n                          <span class=\"dropdown-item\" (click)='selectWarehouse(\"warehouse 1\")' >Warehouse 1</span>\n                          <span class=\"dropdown-item\" (click)='selectWarehouse(\"warehouse 2\")'>Warehouse 2</span>\n                          <span class=\"dropdown-item\" (click)='selectWarehouse(\"warehouse 3\")'>Warehouse 3</span>\n                        </div>\n                      </div> -->\n                    <div class=\"form-group row px-3\">\n                       <label class=\"mb-1\">\n                          <h6 class=\"mb-0 text-sm\">Email Id</h6>\n                       </label>\n                       <input formControlName=\"userId\" [ngClass]=\"{ 'is-invalid': submitted && f.userId.errors }\" type=\"text\"  class=\"form-control\"  id=\"email\" placeholder=\"Enter a valid email address\"> \n                       <div *ngIf=\"submitted && f.userId.errors\" class=\"invalid-feedback\">\n                          <div *ngIf=\"f.userId.errors.required\">User id is required</div>\n                          <div *ngIf=\"f.emailId.errors.pattern\">Please enter valid email id</div>\n                       </div>\n                       \n                    </div>\n                    <div class=\"form-group row px-3\">\n                       <label class=\"mb-1\">\n                          <h6 class=\"mb-0 text-sm\">Password</h6>\n                       </label>\n                       <input formControlName=\"password\" [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" class=\"input form-control\" type=\"password\" name=\"password\" placeholder=\"Enter password\"> \n                       <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n                          <div *ngIf=\"f.password.errors.required\">Password is required</div>\n                       </div>\n                    </div>\n                    <div class=\"row px-3 mb-4\">\n                       <div class=\"custom-control custom-checkbox custom-control-inline\"> <input id=\"chk1\" type=\"checkbox\" name=\"chk\" class=\"custom-control-input\"> </div>\n                       <a [routerLink]=\"['/login/forgot-password']\" class=\"ml-auto mb-0 text-sm\">Forgot Password?</a>\n                    </div>\n                    <div class=\"row mb-3 px-3\"> <button type=\"submit\" class=\"btn btn-blue text-center\" (click)=\"login()\">Login</button> </div>\n                    <!-- <div class=\"row mb-4 px-3\"> <small class=\"font-weight-bold\">Don't have an account? <a class=\"text-danger \">Register</a></small> </div> -->\n                    <div class=\"row mb-4 px-3\"> <small class=\"font-weight-bold\"><a\n                      [routerLink]=\"['/login/change-password']\" class=\"text-warning \">Login with temporary password</a></small>\n                    </div>\n                </form>\n                \n             </div>\n          </div>\n       </div>\n    </div>\n </div>"

/***/ }),

/***/ "./src/app/modules/login/vendor-login/vendor-login.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/modules/login/vendor-login/vendor-login.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media screen and (max-width: 991px) {\n  .card2 {\n    margin: 0px 0px !important; } }\n\n.card2 {\n  margin: 0px 0px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0Rlc2t0b3AvUmF2aS9XTS93bS1mcm9udGVuZC93bS1mcm9udGVuZC9zcmMvYXBwL21vZHVsZXMvbG9naW4vdmVuZG9yLWxvZ2luL3ZlbmRvci1sb2dpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJO0lBQ0ksMEJBQ0osRUFBQSxFQUFDOztBQUdMO0VBQ0ksMEJBQ0osRUFBQSIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvbG9naW4vdmVuZG9yLWxvZ2luL3ZlbmRvci1sb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gICAgLmNhcmQyIHtcbiAgICAgICAgbWFyZ2luOiAwcHggMHB4ICFpbXBvcnRhbnRcbiAgICB9XG59XG5cbi5jYXJkMiB7XG4gICAgbWFyZ2luOiAwcHggMHB4ICFpbXBvcnRhbnRcbn0iXX0= */"

/***/ }),

/***/ "./src/app/modules/login/vendor-login/vendor-login.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/modules/login/vendor-login/vendor-login.component.ts ***!
  \**********************************************************************/
/*! exports provided: VendorLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendorLoginComponent", function() { return VendorLoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../login.service */ "./src/app/modules/login/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _config_web_urls__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../config/web-urls */ "./src/app/config/web-urls.ts");







var VendorLoginComponent = /** @class */ (function () {
    function VendorLoginComponent(formBuilder, router, loginService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.loginService = loginService;
        this.submitted = false;
    }
    VendorLoginComponent.prototype.ngOnInit = function () {
        this.formValidation();
    };
    Object.defineProperty(VendorLoginComponent.prototype, "f", {
        get: function () { return this.loginForm.controls; },
        enumerable: true,
        configurable: true
    });
    VendorLoginComponent.prototype.formValidation = function () {
        this.loginForm = this.formBuilder.group({
            userId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
            ])),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ]))
        });
    };
    VendorLoginComponent.prototype.login = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.submitted = true;
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(true);
                        if (!this.loginForm.valid) return [3 /*break*/, 2];
                        data = {
                            userId: this.loginForm.get('userId').value,
                            password: this.loginForm.get('password').value
                        };
                        return [4 /*yield*/, this.loginService.login$(_config_web_urls__WEBPACK_IMPORTED_MODULE_6__["LOGIN"], data).subscribe({
                                next: function (results) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    localStorage.setItem('access_token', results.data.token);
                                    localStorage.setItem('VendorData', JSON.stringify(results.data.warehouseData));
                                    // this.storeService.dispatchWarehouseData(results.data.warehouseData);
                                    console.log('success result', results);
                                    _this.router.navigate(['vendor/report']);
                                }, error: function (error) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["PopUpState$"].next({
                                        state: 'error',
                                        message: error.error.message ? error.error.message : 'Not able to login at the moment. Please try again later'
                                    });
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                        return [2 /*return*/];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    VendorLoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-vendor-login',
            template: __webpack_require__(/*! ./vendor-login.component.html */ "./src/app/modules/login/vendor-login/vendor-login.component.html"),
            styles: [__webpack_require__(/*! ../../../app.component.scss */ "./src/app/app.component.scss"), __webpack_require__(/*! ../login.component.scss */ "./src/app/modules/login/login.component.scss"), __webpack_require__(/*! ./vendor-login.component.scss */ "./src/app/modules/login/vendor-login/vendor-login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"]])
    ], VendorLoginComponent);
    return VendorLoginComponent;
}());



/***/ }),

/***/ "./src/app/modules/login/warehouse-login/warehouse-login.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/modules/login/warehouse-login/warehouse-login.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto\">\n    <div class=\"card card0 border-0\">\n        <div class=\"row d-flex\">\n            <div class=\"col-lg-6\">\n                <div class=\"card1 pb-5\">\n                    <div class=\"row\"> <img src=\"../../../../assets/img/wmlogowithtitle3.png\" class=\"logo\"> </div>\n                    <div class=\"row px-3 justify-content-center mt-4 mb-5 border-line\"> <img\n                            src=\"../../../../assets/img/wlogin.png\" class=\"image\"> </div>\n                </div>\n            </div>\n            <div class=\"col-lg-6\">\n                <div class=\"card2 card border-0 px-4 py-5\">\n                    <form class=\"form-signin\" [formGroup]=\"loginForm\">\n                        <div class=\"row mb-4 px-2 py-2\">\n                            <h2>Login to access your warehouse</h2>\n                        </div>\n                        <div class=\"form-group row px-3\">\n                            <label class=\"mb-1\">\n                                <h6 class=\"mb-0 text-sm\">Email Id</h6>\n                            </label>\n                            <input formControlName=\"userId\" [ngClass]=\"{ 'is-invalid': submitted && f.userId.errors }\"\n                                type=\"text\" class=\"form-control\" id=\"email\" placeholder=\"Enter a valid email address\">\n                            <div *ngIf=\"submitted && f.userId.errors\" class=\"invalid-feedback\">\n                                <div *ngIf=\"f.userId.errors.required\">User id is required</div>\n                                <div *ngIf=\"f.userId.errors.pattern\">Please enter valid email id</div>\n                            </div>\n\n                        </div>\n                        <div class=\"form-group row px-3\">\n                            <label class=\"mb-1\">\n                                <h6 class=\"mb-0 text-sm\">Password</h6>\n                            </label>\n                            <input formControlName=\"password\"\n                                [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" class=\"input form-control\"\n                                type=\"password\" name=\"password\" placeholder=\"Enter password\">\n                            <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n                                <div *ngIf=\"f.password.errors.required\">Password is required</div>\n                            </div>\n                        </div>\n                        <div class=\"row px-3 mb-4\">\n                            <div class=\"custom-control custom-checkbox custom-control-inline\"> <input id=\"chk1\"\n                                    type=\"checkbox\" name=\"chk\" class=\"custom-control-input\"> </div>\n                            <a [routerLink]=\"['/login/forgot-password']\" class=\"ml-auto mb-0 text-sm\">Forgot\n                                Password?</a>\n                        </div>\n                        <div class=\"row mb-3 px-3\"> <button type=\"submit\" class=\"btn btn-blue text-center\"\n                                (click)=\"login()\">Login</button> </div>\n                        <div class=\"row px-3\"> <small class=\"font-weight-bold\">Don't have an account? <a\n                                [routerLink]=\"['/login/warehouse-sign']\" class=\"text-danger \">Register</a></small>\n                        </div>\n                        <div class=\"row mb-4 px-3\"> <small class=\"font-weight-bold\"><a\n                            [routerLink]=\"['/login/change-password']\" class=\"text-warning \">Login with temporary password</a></small>\n                        </div>\n                    </form>\n                </div>\n            </div>\n        </div>\n    </div>\n    \n</div>"

/***/ }),

/***/ "./src/app/modules/login/warehouse-login/warehouse-login.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/modules/login/warehouse-login/warehouse-login.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media screen and (max-width: 991px) {\n  .card2 {\n    margin: 0px 0px !important; }\n  .border-line {\n    border-right: none;\n    display: none; } }\n\n.card2 {\n  margin: 0px 0px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0Rlc2t0b3AvUmF2aS9XTS93bS1mcm9udGVuZC93bS1mcm9udGVuZC9zcmMvYXBwL21vZHVsZXMvbG9naW4vd2FyZWhvdXNlLWxvZ2luL3dhcmVob3VzZS1sb2dpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJO0lBQ0ksMEJBQ0osRUFBQTtFQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGFBQWEsRUFBQSxFQUNoQjs7QUFHTDtFQUNJLDBCQUNKLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL2xvZ2luL3dhcmVob3VzZS1sb2dpbi93YXJlaG91c2UtbG9naW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCkge1xuICAgIC5jYXJkMiB7XG4gICAgICAgIG1hcmdpbjogMHB4IDBweCAhaW1wb3J0YW50XG4gICAgfVxuICAgIC5ib3JkZXItbGluZSB7XG4gICAgICAgIGJvcmRlci1yaWdodDogbm9uZTtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG59XG5cbi5jYXJkMiB7XG4gICAgbWFyZ2luOiAwcHggMHB4ICFpbXBvcnRhbnRcbn0iXX0= */"

/***/ }),

/***/ "./src/app/modules/login/warehouse-login/warehouse-login.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/modules/login/warehouse-login/warehouse-login.component.ts ***!
  \****************************************************************************/
/*! exports provided: WarehouseLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarehouseLoginComponent", function() { return WarehouseLoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../login.service */ "./src/app/modules/login/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _config_web_urls__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../config/web-urls */ "./src/app/config/web-urls.ts");
/* harmony import */ var _store_store_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../store/store.service */ "./src/app/store/store.service.ts");








var WarehouseLoginComponent = /** @class */ (function () {
    function WarehouseLoginComponent(formBuilder, router, loginService, storeService) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.loginService = loginService;
        this.storeService = storeService;
        this.submitted = false;
    }
    WarehouseLoginComponent.prototype.ngOnInit = function () {
        this.formValidation();
    };
    Object.defineProperty(WarehouseLoginComponent.prototype, "f", {
        get: function () { return this.loginForm.controls; },
        enumerable: true,
        configurable: true
    });
    WarehouseLoginComponent.prototype.formValidation = function () {
        this.loginForm = this.formBuilder.group({
            userId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
            ])),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ]))
        });
    };
    WarehouseLoginComponent.prototype.login = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.submitted = true;
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(true);
                        if (!this.loginForm.valid) return [3 /*break*/, 2];
                        data = {
                            userId: this.loginForm.get('userId').value,
                            password: this.loginForm.get('password').value
                        };
                        return [4 /*yield*/, this.loginService.login$(_config_web_urls__WEBPACK_IMPORTED_MODULE_6__["LOGIN"], data).subscribe({
                                next: function (results) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    localStorage.setItem('access_token', results.data.token);
                                    localStorage.setItem('WarehouseData', JSON.stringify(results.data.warehouseData));
                                    // this.storeService.dispatchWarehouseData(results.data.warehouseData);
                                    console.log('success result', results);
                                    _this.router.navigate(['warehouse/']);
                                }, error: function (error) {
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                                    _app_component__WEBPACK_IMPORTED_MODULE_3__["PopUpState$"].next({
                                        state: 'error',
                                        message: error.error.message ? error.error.message : 'Not able to login at the moment. Please try again later'
                                    });
                                    console.log(error, 'error response in service');
                                    console.log('error.status', error.status);
                                    console.log('error.error.message', error.error.message);
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
                        return [2 /*return*/];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    WarehouseLoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-warehouse-login',
            template: __webpack_require__(/*! ./warehouse-login.component.html */ "./src/app/modules/login/warehouse-login/warehouse-login.component.html"),
            styles: [__webpack_require__(/*! ../../../app.component.scss */ "./src/app/app.component.scss"), __webpack_require__(/*! ../login.component.scss */ "./src/app/modules/login/login.component.scss"), __webpack_require__(/*! ./warehouse-login.component.scss */ "./src/app/modules/login/warehouse-login/warehouse-login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"],
            _store_store_service__WEBPACK_IMPORTED_MODULE_7__["StoreService"]])
    ], WarehouseLoginComponent);
    return WarehouseLoginComponent;
}());



/***/ }),

/***/ "./src/app/modules/login/warehouse-signup/warehouse-signup.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/modules/login/warehouse-signup/warehouse-signup.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto\">\n    <div class=\"card card0 border-0\">\n       <div class=\"row d-flex\">\n          <div class=\"col-lg-6\">\n             <div class=\"card1 pb-5\">\n                <div class=\"row\"> <img src=\"https://i.imgur.com/CXQmsmF.png\" class=\"logo\"> </div>\n                <div class=\"row px-3 justify-content-center mt-4 mb-5 border-line\"> <img src=\"https://i.imgur.com/uNGdWHi.png\" class=\"image\"> </div>\n             </div>\n          </div>\n          <div class=\"col-lg-6\">\n             <div class=\"card2 card border-0 px-4 py-5\">\n                \n                <form class=\"form-signin\" [formGroup]=\"registerWarehouseForm\">\n                    <div class=\"row mb-4 px-2 py-2\">\n                        <h2>Hey, please fill your warehouse details and request for access</h2>\n                     </div>\n                    <div class=\"form-group row px-3\">\n                       <label class=\"mb-1\">\n                          <h6 class=\"mb-0 text-sm\">Warehouse Name</h6>\n                       </label>\n                       <input formControlName=\"wareHouseName\" [ngClass]=\"{ 'is-invalid': submitted && f.wareHouseName.errors }\" type=\"text\"  class=\"form-control\"  placeholder=\"Enter warehouse name\"> \n                       <div *ngIf=\"submitted && f.wareHouseName.errors\" class=\"invalid-feedback\">\n                          <div *ngIf=\"f.wareHouseName.errors.required\">Please add warehouse name</div>\n                       </div>\n                       \n                    </div>\n                    <div class=\"form-group row px-3\">\n                        <label class=\"mb-1\">\n                           <h6 class=\"mb-0 text-sm\">Owner Name</h6>\n                        </label>\n                        <input formControlName=\"ownerName\" [ngClass]=\"{ 'is-invalid': submitted && f.ownerName.errors }\" type=\"text\"  class=\"form-control\"  placeholder=\"Enter owner name\"> \n                        <div *ngIf=\"submitted && f.ownerName.errors\" class=\"invalid-feedback\">\n                           <div *ngIf=\"f.ownerName.errors.required\">Please add owner name</div>\n                        </div>\n                        \n                     </div>\n                     <div class=\"form-group row px-3\">\n                        <label class=\"mb-1\">\n                           <h6 class=\"mb-0 text-sm\">Owner contact number</h6>\n                        </label>\n                        <input formControlName=\"ownerContactNumber\" [ngClass]=\"{ 'is-invalid': submitted && f.ownerContactNumber.errors }\" type=\"number\"  class=\"form-control\"  placeholder=\"Enter contact number\"> \n                        <div *ngIf=\"submitted && f.ownerContactNumber.errors\" class=\"invalid-feedback\">\n                           <div *ngIf=\"f.ownerContactNumber.errors.required\">Please add owner contact number</div>\n                        </div>\n                        \n                     </div>\n                     <div class=\"form-group row px-3\">\n                        <label class=\"mb-1\">\n                           <h6 class=\"mb-0 text-sm\">Owner email id</h6>\n                        </label>\n                        <input formControlName=\"ownerEmailId\" [ngClass]=\"{ 'is-invalid': submitted && f.ownerEmailId.errors }\" type=\"text\"  class=\"form-control\"  placeholder=\"Enter a valid email address\"> \n                        <div *ngIf=\"submitted && f.ownerEmailId.errors\" class=\"invalid-feedback\">\n                           <div *ngIf=\"f.ownerEmailId.errors.required\">Please add email address</div>\n                           <div *ngIf=\"f.ownerEmailId.errors.pattern\">Please enter valid email id</div>\n                        </div>\n                        \n                     </div>\n                     <div class=\"form-group row px-3\">\n                        <label class=\"mb-1\">\n                           <h6 class=\"mb-0 text-sm\">Address</h6>\n                        </label>\n                        <input formControlName=\"address\" [ngClass]=\"{ 'is-invalid': submitted && f.address.errors }\" type=\"text\"  class=\"form-control\"   placeholder=\"Enter warehouse address\"> \n                        <div *ngIf=\"submitted && f.address.errors\" class=\"invalid-feedback\">\n                           <div *ngIf=\"f.address.errors.required\">Please provide address</div>\n                        </div>\n                        \n                     </div>\n                    <div class=\"row mb-3 px-3\"> <button type=\"submit\" class=\"btn btn-blue text-center\" (click)=\"requestAccess()\">Request</button> </div>\n                </form>\n                \n             </div>\n          </div>\n       </div>\n    </div>\n </div>"

/***/ }),

/***/ "./src/app/modules/login/warehouse-signup/warehouse-signup.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/modules/login/warehouse-signup/warehouse-signup.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media screen and (max-width: 991px) {\n  .card2 {\n    margin: 0px 0px !important; } }\n\n.card2 {\n  margin: 0px 0px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0Rlc2t0b3AvUmF2aS9XTS93bS1mcm9udGVuZC93bS1mcm9udGVuZC9zcmMvYXBwL21vZHVsZXMvbG9naW4vd2FyZWhvdXNlLXNpZ251cC93YXJlaG91c2Utc2lnbnVwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0k7SUFDSSwwQkFDSixFQUFBLEVBQUM7O0FBR0w7RUFDSSwwQkFDSixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbW9kdWxlcy9sb2dpbi93YXJlaG91c2Utc2lnbnVwL3dhcmVob3VzZS1zaWdudXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCkge1xuICAgIC5jYXJkMiB7XG4gICAgICAgIG1hcmdpbjogMHB4IDBweCAhaW1wb3J0YW50XG4gICAgfVxufVxuXG4uY2FyZDIge1xuICAgIG1hcmdpbjogMHB4IDBweCAhaW1wb3J0YW50XG59Il19 */"

/***/ }),

/***/ "./src/app/modules/login/warehouse-signup/warehouse-signup.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/modules/login/warehouse-signup/warehouse-signup.component.ts ***!
  \******************************************************************************/
/*! exports provided: WarehouseSignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarehouseSignupComponent", function() { return WarehouseSignupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../app.component */ "./src/app/app.component.ts");




var WarehouseSignupComponent = /** @class */ (function () {
    function WarehouseSignupComponent(formBuilder) {
        this.formBuilder = formBuilder;
        this.selectedWareHouse = 'Choose Warehouse';
        this.submitted = false;
    }
    WarehouseSignupComponent.prototype.ngOnInit = function () {
        this.formValidation();
    };
    Object.defineProperty(WarehouseSignupComponent.prototype, "f", {
        get: function () { return this.registerWarehouseForm.controls; },
        enumerable: true,
        configurable: true
    });
    WarehouseSignupComponent.prototype.formValidation = function () {
        this.registerWarehouseForm = this.formBuilder.group({
            wareHouseName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ])),
            ownerName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ])),
            ownerContactNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ])),
            ownerEmailId: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
            ])),
            address: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ])),
        });
    };
    WarehouseSignupComponent.prototype.requestAccess = function () {
        _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(true);
        this.submitted = true;
        console.log('this.loginForm.valid', this.registerWarehouseForm.get('userId').value);
        this.submitted = true;
        if (this.registerWarehouseForm.valid) {
            console.log('wareHouseName ===>', this.registerWarehouseForm.get('wareHouseName').value);
            console.log('ownerName ===>', this.registerWarehouseForm.get('ownerName').value);
            _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
        }
        else {
            _app_component__WEBPACK_IMPORTED_MODULE_3__["LoaderState$"].next(false);
            return;
        }
    };
    WarehouseSignupComponent.prototype.selectWarehouse = function (wareHouse) {
        console.log('selected warehouse', wareHouse);
        this.selectedWareHouse = wareHouse;
    };
    WarehouseSignupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-warehouse-signup',
            template: __webpack_require__(/*! ./warehouse-signup.component.html */ "./src/app/modules/login/warehouse-signup/warehouse-signup.component.html"),
            styles: [__webpack_require__(/*! ../../../app.component.scss */ "./src/app/app.component.scss"), __webpack_require__(/*! ../login.component.scss */ "./src/app/modules/login/login.component.scss"), __webpack_require__(/*! ./warehouse-signup.component.scss */ "./src/app/modules/login/warehouse-signup/warehouse-signup.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
    ], WarehouseSignupComponent);
    return WarehouseSignupComponent;
}());



/***/ }),

/***/ "./src/app/services/webApi.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/webApi.service.ts ***!
  \********************************************/
/*! exports provided: WebService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebService", function() { return WebService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _config_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../config/app-config */ "./src/app/config/app-config.ts");




var WebService = /** @class */ (function () {
    function WebService(http) {
        this.http = http;
    }
    /**
     * @description This method is for Get Api Calls
     * @param url String url for the API
     * @param httpOptions header part is optional
     * @returns This function returns the respective response from the Api
     */
    WebService.prototype.getApi = function (url) {
        var accessToken = localStorage.getItem('access_token');
        var httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        else {
            httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                })
            };
        }
        return this.http.get(_config_app_config__WEBPACK_IMPORTED_MODULE_3__["BASE_URL"] + url, httpOptions);
    };
    /**
     * This method is for Post Api calls
     * @param url String Api URL
     * @param requestBody Object required for the Post request
     * @returns Return the respective response from the Apis.
     */
    WebService.prototype.postApi$ = function (url, requestBody) {
        console.log(url, 'url');
        console.log(_config_app_config__WEBPACK_IMPORTED_MODULE_3__["BASE_URL"], 'BASE_URL');
        console.log(requestBody, 'requestBody');
        var accessToken = localStorage.getItem('access_token');
        var httpOptions;
        if (accessToken) {
            httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    // tslint:disable-next-line:object-literal-key-quotes
                    authorization: accessToken,
                })
            };
        }
        return this.http.post(_config_app_config__WEBPACK_IMPORTED_MODULE_3__["BASE_URL"] + url, requestBody, httpOptions);
    };
    WebService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], WebService);
    return WebService;
}());



/***/ })

}]);
//# sourceMappingURL=modules-login-login-module.js.map