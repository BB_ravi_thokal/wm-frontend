(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./modules/login/login.module": [
		"./src/app/modules/login/login.module.ts",
		"default~modules-login-login-module~modules-vendor-vendor-module~modules-warehouse-warehouse-module",
		"modules-login-login-module"
	],
	"./modules/vendor/vendor.module": [
		"./src/app/modules/vendor/vendor.module.ts",
		"default~modules-login-login-module~modules-vendor-vendor-module~modules-warehouse-warehouse-module",
		"default~modules-vendor-vendor-module~modules-warehouse-warehouse-module",
		"modules-vendor-vendor-module"
	],
	"./modules/warehouse/warehouse.module": [
		"./src/app/modules/warehouse/warehouse.module.ts",
		"default~modules-login-login-module~modules-vendor-vendor-module~modules-warehouse-warehouse-module",
		"default~modules-vendor-vendor-module~modules-warehouse-warehouse-module",
		"modules-warehouse-warehouse-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modules_landing_landing_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/landing/landing.component */ "./src/app/modules/landing/landing.component.ts");
/* harmony import */ var _services_warehouse_auth_guard_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/warehouse-auth-guard.service */ "./src/app/services/warehouse-auth-guard.service.ts");
/* harmony import */ var _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/auth-guard.service */ "./src/app/services/auth-guard.service.ts");






var routes = [
    {
        path: '',
        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
        pathMatch: 'full',
        redirectTo: 'dw-home',
    },
    {
        path: 'dw-home',
        component: _modules_landing_landing_component__WEBPACK_IMPORTED_MODULE_3__["LandingComponent"]
    },
    {
        path: 'login',
        canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
        loadChildren: './modules/login/login.module#LoginModule',
    },
    {
        path: 'warehouse',
        canActivate: [_services_warehouse_auth_guard_service__WEBPACK_IMPORTED_MODULE_4__["WarehouseAuthGuard"]],
        loadChildren: './modules/warehouse/warehouse.module#WarehouseModule',
    },
    {
        path: 'vendor',
        // canActivate: [VendorAuthGuard],
        loadChildren: './modules/vendor/vendor.module#VendorModule',
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ngx-loading [show]=\"loading\" [config]=\"{ \n    backdropBackgroundColour: 'rgba(255,255,255,0.3)', backdropBorderRadius: '10px',\n    primaryColour: '#ffffff', secondaryColour: '#ffffff', tertiaryColour: '#ffffff' }\" [template]=\"customLoadingTemplate\"></ngx-loading> -->\n<ng-container *ngIf=\"loading\">\n    <div class=\"loader-background\"><div class=\"loader\"></div></div>\n</ng-container>\n\n<div style=\"margin-left: 10%; margin-right: 10%;\">\n    <div class=\"alert alert-success alert-dismissible\" *ngIf=\"isSuccess\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n        <strong>{{message}}</strong>\n    </div>\n    <div class=\"alert alert-danger alert-dismissible\" *ngIf=\"isError\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" (click)=\"disablePopUp()\">&times;</button>\n        <strong>{{message}}</strong>\n    </div>\n\n    <div class=\"toast\" *ngIf=\"isError\">\n        <div class=\"toast-body\">\n            {{message}}\n        </div>\n    </div>\n</div>\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".text-sm {\n  font-size: 14px !important; }\n\n.toast {\n  box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(244, 67, 54, 0.4);\n  background-color: #f55145;\n  color: #ffffff;\n  display: block !important; }\n\n::-moz-placeholder {\n  color: #BDBDBD;\n  opacity: 1;\n  font-weight: 300; }\n\n::placeholder {\n  color: #BDBDBD;\n  opacity: 1;\n  font-weight: 300; }\n\n:-ms-input-placeholder {\n  color: #BDBDBD;\n  font-weight: 300; }\n\n::-ms-input-placeholder {\n  color: #BDBDBD;\n  font-weight: 300; }\n\n.input,\ntextarea {\n  padding: 10px 12px 10px 12px;\n  border: 1px solid lightgrey;\n  border-radius: 2px;\n  margin-bottom: 5px;\n  margin-top: 2px;\n  width: 100%;\n  box-sizing: border-box;\n  color: #2C3E50;\n  font-size: 14px;\n  letter-spacing: 1px; }\n\n.select {\n  padding: 5px 5px 5px 5px;\n  /*border: 1px solid lightgrey;*/\n  border-radius: 2px;\n  margin-bottom: 5px;\n  margin-top: 2px;\n  width: 100%;\n  box-sizing: border-box;\n  color: #2C3E50;\n  font-size: 14px;\n  letter-spacing: 1px; }\n\n.inputError {\n  padding: 10px 12px 10px 12px;\n  border: 1px solid red;\n  border-radius: 2px;\n  margin-bottom: 5px;\n  margin-top: 2px;\n  width: 100%;\n  box-sizing: border-box;\n  color: #2C3E50;\n  font-size: 14px;\n  letter-spacing: 1px; }\n\n.input:focus,\nselect:focus,\ntextarea:focus {\n  box-shadow: none !important;\n  border: 1px solid #304FFE;\n  outline-width: 0; }\n\n.select:focus {\n  box-shadow: none !important;\n  border: 1px solid #304FFE;\n  outline-width: 0; }\n\nbutton:focus {\n  box-shadow: none !important;\n  outline-width: 0; }\n\na {\n  color: inherit;\n  cursor: pointer; }\n\n.btn-blue {\n  background-color: #1A237E;\n  height: 50px;\n  color: #fff;\n  border-radius: 2px;\n  text-align: center; }\n\n.btn-dropDown {\n  padding: 10px 12px 10px 12px;\n  border: 1px solid lightgrey;\n  border-radius: 2px;\n  margin-bottom: 10px;\n  margin-top: 2px;\n  width: 100%;\n  box-sizing: border-box;\n  font-size: 14px;\n  letter-spacing: 1px; }\n\n.btn-dropDown:hover {\n  background-color: #000;\n  cursor: pointer; }\n\n.btn-blue:hover {\n  background-color: #000;\n  cursor: pointer; }\n\n.bg-blue {\n  color: #fff;\n  background-color: #1A237E; }\n\n.alert {\n  z-index: 9999999999 !important;\n  position: absolute !important; }\n\n.loader {\n  border-radius: 50%;\n  border-top: 4px solid;\n  border-right: 4px solid;\n  border-bottom: 4px solid;\n  border-left: 4px solid transparent;\n  width: 40px;\n  height: 40px;\n  position: fixed;\n  top: 50%;\n  margin: auto;\n  z-index: 999;\n  -webkit-animation: spin 2s linear infinite;\n  /* Safari */\n  animation: spin 2s linear infinite; }\n\n.loader-background {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(255, 255, 255, 0.9);\n  z-index: 99999999;\n  display: flex;\n  justify-content: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rL0Rlc2t0b3AvUmF2aS9XTS93bS1mcm9udGVuZC93bS1mcm9udGVuZC9zcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDBCQUNKLEVBQUE7O0FBRUE7RUFDSSxzRkFBc0Y7RUFDdEYseUJBQXlCO0VBQ3pCLGNBQWM7RUFDZCx5QkFBeUIsRUFBQTs7QUFHN0I7RUFDSSxjQUFjO0VBQ2QsVUFBVTtFQUNWLGdCQUNKLEVBQUE7O0FBSkE7RUFDSSxjQUFjO0VBQ2QsVUFBVTtFQUNWLGdCQUNKLEVBQUE7O0FBRUE7RUFDSSxjQUFjO0VBQ2QsZ0JBQ0osRUFBQTs7QUFFQTtFQUNJLGNBQWM7RUFDZCxnQkFDSixFQUFBOztBQUVBOztFQUVJLDRCQUE0QjtFQUM1QiwyQkFBMkI7RUFDM0Isa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsV0FBVztFQUNYLHNCQUFzQjtFQUN0QixjQUFjO0VBQ2QsZUFBZTtFQUNmLG1CQUNKLEVBQUE7O0FBRUE7RUFDSSx3QkFBd0I7RUFDeEIsK0JBQUE7RUFDQSxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixXQUFXO0VBQ1gsc0JBQXNCO0VBQ3RCLGNBQWM7RUFDZCxlQUFlO0VBQ2YsbUJBQ0osRUFBQTs7QUFFQTtFQUNJLDRCQUE0QjtFQUM1QixxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsV0FBVztFQUNYLHNCQUFzQjtFQUN0QixjQUFjO0VBQ2QsZUFBZTtFQUNmLG1CQUNKLEVBQUE7O0FBRUE7OztFQUtJLDJCQUEyQjtFQUMzQix5QkFBeUI7RUFDekIsZ0JBQ0osRUFBQTs7QUFFQTtFQUdJLDJCQUEyQjtFQUMzQix5QkFBeUI7RUFDekIsZ0JBQ0osRUFBQTs7QUFFQTtFQUdJLDJCQUEyQjtFQUMzQixnQkFDSixFQUFBOztBQUVBO0VBQ0ksY0FBYztFQUNkLGVBQ0osRUFBQTs7QUFFQTtFQUNJLHlCQUF5QjtFQUV6QixZQUFZO0VBQ1osV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0IsRUFBQTs7QUFHdEI7RUFFSSw0QkFBNEI7RUFDNUIsMkJBQTJCO0VBQzNCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLFdBQVc7RUFDWCxzQkFBc0I7RUFFdEIsZUFBZTtFQUNmLG1CQUNKLEVBQUE7O0FBRUE7RUFDSSxzQkFBc0I7RUFDdEIsZUFDSixFQUFBOztBQUVBO0VBQ0ksc0JBQXNCO0VBQ3RCLGVBQ0osRUFBQTs7QUFFQTtFQUNJLFdBQVc7RUFDWCx5QkFDSixFQUFBOztBQUVBO0VBQ0ksOEJBQThCO0VBQzlCLDZCQUE2QixFQUFBOztBQUdqQztFQUNJLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsdUJBQXVCO0VBQ3ZCLHdCQUF3QjtFQUN4QixrQ0FBa0M7RUFDbEMsV0FBVztFQUNYLFlBQVk7RUFDWixlQUFlO0VBQ2YsUUFBUTtFQUNSLFlBQVk7RUFDWixZQUFZO0VBQ1osMENBQTBDO0VBQUUsV0FBQTtFQUM1QyxrQ0FBa0MsRUFBQTs7QUFFcEM7RUFDRSxlQUFlO0VBQ2YsTUFBTTtFQUNOLE9BQU87RUFDUCxXQUFXO0VBQ1gsWUFBWTtFQUNaLDBDQUEwQztFQUMxQyxpQkFBaUI7RUFDakIsYUFBYTtFQUNiLHVCQUF1QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRleHQtc20ge1xuICAgIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50XG59XG5cbi50b2FzdCB7XG4gICAgYm94LXNoYWRvdzogMCA0cHggMjBweCAwcHggcmdiYSgwLCAwLCAwLCAwLjE0KSwgMCA3cHggMTBweCAtNXB4IHJnYmEoMjQ0LCA2NywgNTQsIDAuNCk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y1NTE0NTtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xufVxuXG46OnBsYWNlaG9sZGVyIHtcbiAgICBjb2xvcjogI0JEQkRCRDtcbiAgICBvcGFjaXR5OiAxO1xuICAgIGZvbnQtd2VpZ2h0OiAzMDBcbn1cblxuOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XG4gICAgY29sb3I6ICNCREJEQkQ7XG4gICAgZm9udC13ZWlnaHQ6IDMwMFxufVxuXG46Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XG4gICAgY29sb3I6ICNCREJEQkQ7XG4gICAgZm9udC13ZWlnaHQ6IDMwMFxufVxuXG4uaW5wdXQsXG50ZXh0YXJlYSB7XG4gICAgcGFkZGluZzogMTBweCAxMnB4IDEwcHggMTJweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyZXk7XG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgICBtYXJnaW4tdG9wOiAycHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBjb2xvcjogIzJDM0U1MDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweFxufVxuXG4uc2VsZWN0IHtcbiAgICBwYWRkaW5nOiA1cHggNXB4IDVweCA1cHg7XG4gICAgLypib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyZXk7Ki9cbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICAgIG1hcmdpbi10b3A6IDJweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGNvbG9yOiAjMkMzRTUwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4XG59XG5cbi5pbnB1dEVycm9yIHtcbiAgICBwYWRkaW5nOiAxMHB4IDEycHggMTBweCAxMnB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJlZDtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICAgIG1hcmdpbi10b3A6IDJweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGNvbG9yOiAjMkMzRTUwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4XG59XG5cbi5pbnB1dDpmb2N1cyxcbnNlbGVjdDpmb2N1cyxcbnRleHRhcmVhOmZvY3VzIHtcbiAgICAtbW96LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzMwNEZGRTtcbiAgICBvdXRsaW5lLXdpZHRoOiAwXG59XG5cbi5zZWxlY3Q6Zm9jdXMge1xuICAgIC1tb3otYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xuICAgIC13ZWJraXQtYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xuICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMzA0RkZFO1xuICAgIG91dGxpbmUtd2lkdGg6IDBcbn1cblxuYnV0dG9uOmZvY3VzIHtcbiAgICAtbW96LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gICAgb3V0bGluZS13aWR0aDogMFxufVxuXG5hIHtcbiAgICBjb2xvcjogaW5oZXJpdDtcbiAgICBjdXJzb3I6IHBvaW50ZXJcbn1cblxuLmJ0bi1ibHVlIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMUEyMzdFO1xuICAgIC8vd2lkdGg6IDE1MHB4O1xuICAgIGhlaWdodDogNTBweDtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uYnRuLWRyb3BEb3duIHtcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjMUEyMzdFO1xuICAgIHBhZGRpbmc6IDEwcHggMTJweCAxMHB4IDEycHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmV5O1xuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIG1hcmdpbi10b3A6IDJweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIC8vIGNvbG9yOiAjMkMzRTUwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4XG59XG5cbi5idG4tZHJvcERvd246aG92ZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XG4gICAgY3Vyc29yOiBwb2ludGVyXG59XG5cbi5idG4tYmx1ZTpob3ZlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcbiAgICBjdXJzb3I6IHBvaW50ZXJcbn1cblxuLmJnLWJsdWUge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMxQTIzN0Vcbn1cblxuLmFsZXJ0IHtcbiAgICB6LWluZGV4OiA5OTk5OTk5OTk5ICFpbXBvcnRhbnQ7XG4gICAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XG59XG5cbi5sb2FkZXIge1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBib3JkZXItdG9wOiA0cHggc29saWQ7XG4gICAgYm9yZGVyLXJpZ2h0OiA0cHggc29saWQ7XG4gICAgYm9yZGVyLWJvdHRvbTogNHB4IHNvbGlkO1xuICAgIGJvcmRlci1sZWZ0OiA0cHggc29saWQgdHJhbnNwYXJlbnQ7XG4gICAgd2lkdGg6IDQwcHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICB0b3A6IDUwJTtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgei1pbmRleDogOTk5O1xuICAgIC13ZWJraXQtYW5pbWF0aW9uOiBzcGluIDJzIGxpbmVhciBpbmZpbml0ZTsgLyogU2FmYXJpICovXG4gICAgYW5pbWF0aW9uOiBzcGluIDJzIGxpbmVhciBpbmZpbml0ZTtcbiAgfVxuICAubG9hZGVyLWJhY2tncm91bmQge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjkpO1xuICAgIHotaW5kZXg6IDk5OTk5OTk5O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: LoaderState$, PopUpState$, AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderState$", function() { return LoaderState$; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopUpState$", function() { return PopUpState$; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var LoaderState$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
var PopUpState$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.isError = false;
        this.isSuccess = false;
        this.loading = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                PopUpState$
                    .subscribe(function (data) {
                    console.log('inside popup subscriber', data);
                    if (data.state.toLowerCase() === 'success') {
                        _this.isSuccess = true;
                        _this.isError = false;
                        _this.message = data.message;
                    }
                    else if (data.state.toLowerCase() === 'error') {
                        _this.isError = true;
                        _this.isSuccess = false;
                        _this.message = data.message;
                    }
                    else {
                        _this.isError = false;
                        _this.isSuccess = false;
                        _this.message = '';
                    }
                });
                LoaderState$
                    .subscribe(function (data) {
                    console.log('inside popup subscriber', data);
                    if (data) {
                        _this.loading = true;
                    }
                    else {
                        _this.loading = false;
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    AppComponent.prototype.disablePopUp = function () {
        this.isError = false;
        this.isSuccess = false;
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _modules_landing_landing_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modules/landing/landing.component */ "./src/app/modules/landing/landing.component.ts");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm2015/ngrx-store.js");
/* harmony import */ var _store_reducer_composition__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./store/reducer-composition */ "./src/app/store/reducer-composition.ts");
/* harmony import */ var _services_warehouse_auth_guard_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./services/warehouse-auth-guard.service */ "./src/app/services/warehouse-auth-guard.service.ts");
/* harmony import */ var _services_vendor_auth_guard_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/vendor-auth-guard.service */ "./src/app/services/vendor-auth-guard.service.ts");
/* harmony import */ var _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/auth-guard.service */ "./src/app/services/auth-guard.service.ts");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _modules_landing_landing_component__WEBPACK_IMPORTED_MODULE_6__["LandingComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_12__["NgxPaginationModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _ngrx_store__WEBPACK_IMPORTED_MODULE_7__["StoreModule"].forRoot({
                    warehouseData: _store_reducer_composition__WEBPACK_IMPORTED_MODULE_8__["reducers"].warehouseData,
                }),
            ],
            providers: [_services_warehouse_auth_guard_service__WEBPACK_IMPORTED_MODULE_9__["WarehouseAuthGuard"], _services_vendor_auth_guard_service__WEBPACK_IMPORTED_MODULE_10__["VendorAuthGuard"], _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_11__["AuthGuard"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["CUSTOM_ELEMENTS_SCHEMA"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/modules/landing/landing.component.html":
/*!********************************************************!*\
  !*** ./src/app/modules/landing/landing.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  \n  <!-- ======= Header ======= -->\n  <header id=\"header\" class=\"fixed-top \">\n    <div class=\"container d-flex align-items-center\">\n\n      <!-- <h1 class=\"logo mr-auto\"><a href=\"index.html\">Arsha</a></h1> -->\n      <!-- Uncomment below if you prefer to use an image logo -->\n      <a href=\"index.html\" class=\"logo mr-auto\"><img src=\"../../../assets/img/wmlogowithtitle3.png\" alt=\"\" class=\"img-fluid\"></a>\n\n      <nav class=\"nav-menu d-none d-lg-block\">\n        <ul>\n          <li class=\"active\"><a href=\"index.html\">Home</a></li>\n          <li><a href=\"#about\">About</a></li>\n          <li><a href=\"#services\">Services</a></li>\n          <!-- <li><a href=\"#portfolio\">Portfolio</a></li> -->\n          <li><a href=\"#team\">Team</a></li>\n          <li><a href=\"#contact\">Contact</a></li>\n          <!-- <li class=\"drop-down\"><a href=\"\">Drop Down</a>\n            <ul>\n              <li><a href=\"#\">Drop Down 1</a></li>\n              <li class=\"drop-down\"><a href=\"#\">Deep Drop Down</a>\n                <ul>\n                  <li><a href=\"#\">Deep Drop Down 1</a></li>\n                  <li><a href=\"#\">Deep Drop Down 2</a></li>\n                  <li><a href=\"#\">Deep Drop Down 3</a></li>\n                  <li><a href=\"#\">Deep Drop Down 4</a></li>\n                  <li><a href=\"#\">Deep Drop Down 5</a></li>\n                </ul>\n              </li>\n              <li><a href=\"#\">Drop Down 2</a></li>\n              <li><a href=\"#\">Drop Down 3</a></li>\n              <li><a href=\"#\">Drop Down 4</a></li>\n            </ul>\n          </li> -->\n          <li><a [routerLink]=\"['/login/vendor-login']\" class=\"get-started-btn scrollto\">Vendor Login</a></li>\n          <li><a [routerLink]=\"['/login/warehouse-login']\" class=\"get-started-btn scrollto\">WAREHOUSE LOGIN</a></li>\n\n        </ul>\n      </nav>\n      \n      <!-- .nav-menu -->\n    </div>\n  </header><!-- End Header -->\n\n  <!-- ======= Hero Section ======= -->\n  <section id=\"hero\" class=\"d-flex align-items-center\">\n\n    <div class=\"container\">\n      <div class=\"row\">\n        <div class=\"col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1\" data-aos=\"fade-up\" data-aos-delay=\"200\">\n          <h1>Better Solutions For Your Storage</h1>\n          <h2>We provide an innovative & secure storage solution for your businesses. Our flexible Warehouse Management System allow you to track your daily works like Import, Export and Report, we keep it safe & secure!</h2>\n          <div class=\"d-lg-flex\">\n            <a href=\"#about\" class=\"btn-get-started scrollto\">Get Started</a>\n            <a href=\"https://www.youtube.com/watch?v=jDDaplaOz7Q\" class=\"venobox btn-watch-video\" data-vbtype=\"video\" data-autoplay=\"true\"> Watch Video <i class=\"icofont-play-alt-2\"></i></a>\n          </div>\n        </div>\n        <div class=\"col-lg-6 order-1 order-lg-2 hero-img\" data-aos=\"zoom-in\" data-aos-delay=\"200\">\n          <img src=\"../../../assets/img/hero-img.png\" class=\"img-fluid animated\" alt=\"\">\n        </div>\n      </div>\n    </div>\n\n  </section><!-- End Hero -->\n\n  <main id=\"main\">\n\n    <!-- ======= Cliens Section ======= -->\n    <section id=\"cliens\" class=\"cliens section-bg\">\n      <div class=\"container\">\n\n        <div class=\"row\" data-aos=\"zoom-in\">\n\n          <div class=\"col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center\">\n            <!-- <img src=\"../../../assets/img/clients/client-1.png\" class=\"img-fluid\" alt=\"\"> -->\n            <span class=\"img-fluid\" alt=\"\">OM LOGISTICS</span>\n          </div>\n\n        </div>\n\n      </div>\n    </section><!-- End Cliens Section -->\n\n    <!-- ======= About Us Section ======= -->\n    <section id=\"about\" class=\"about\">\n      <div class=\"container\" data-aos=\"fade-up\">\n\n        <div class=\"section-title\">\n          <h2>About Us</h2>\n        </div>\n\n        <div class=\"row content\">\n          <div class=\"col-lg-6\">\n            <p>\n              It is an software application specializing in supporting the day to day operations within a\n              warehouse. The application does this by allowing the users to have a centralized system where different warehouse tasks are\n              managed through an interface on a handheld device or a tablet working in the warehouse or a desktop in the office.\n            </p>\n            <ul>\n              <li><i class=\"ri-check-double-line\"></i> Import or Export Entries</li>\n              <li><i class=\"ri-check-double-line\"></i> Managing Product as well as Vendor</li>\n              <li><i class=\"ri-check-double-line\"></i> Reports </li>\n            </ul>\n          </div>\n          <div class=\"col-lg-6 pt-4 pt-lg-0\">\n            <p>\n              Picking and fulfillment operations can be optimized and made more efficient \n              when the Warehouse Management System in place makes sense and is easy to access for everyday users.\n            </p>\n            <a href=\"#\" class=\"btn-learn-more\">Learn More</a>\n          </div>\n        </div>\n\n      </div>\n    </section><!-- End About Us Section -->\n\n    <!-- ======= Why Us Section ======= -->\n    <section id=\"why-us\" class=\"why-us section-bg\">\n      <div class=\"container-fluid\" data-aos=\"fade-up\">\n\n        <div class=\"row\">\n\n          <div class=\"col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1\">\n\n            <div class=\"content\">\n              <h3>Frustrated from daily calculation and Paperwork  <strong>We’re here to help!</strong></h3>\n              <p>\n                Moving onto a dedicated inventory solution can provide massive benefits to your business. Here are some signs.\n              </p>\n            </div>\n\n            <div class=\"accordion-list\">\n              <ul>\n                <li>\n                  <a data-toggle=\"collapse\" class=\"collapse\" href=\"#accordion-list-1\"><span>01</span> If You need more flexibility ? <i class=\"bx bx-chevron-down icon-show\"></i><i class=\"bx bx-chevron-up icon-close\"></i></a>\n                  <div id=\"accordion-list-1\" class=\"collapse show\" data-parent=\".accordion-list\">\n                    <p>\n                      Spreadsheets are an inflexible way to manage inventory. If you find that you need to add extra sheets to your solution, or are struggling because employees can’t access real-time data, then it might be time to upgrade to cloud-based inventory software.\n                    </p>\n                  </div>\n                </li>\n\n                <li>\n                  <a data-toggle=\"collapse\" href=\"#accordion-list-2\" class=\"collapsed\"><span>02</span> If You need more accuracy ? <i class=\"bx bx-chevron-down icon-show\"></i><i class=\"bx bx-chevron-up icon-close\"></i></a>\n                  <div id=\"accordion-list-2\" class=\"collapse\" data-parent=\".accordion-list\">\n                    <p>\n                      Do you find it difficult to track how much of a particular item you have at any one time? Using a dedicated solution, you — and your employees — will be able to get up-to-date information on your inventory at any time, from any place.\n                    </p>\n                  </div>\n                </li>\n\n                <li>\n                  <a data-toggle=\"collapse\" href=\"#accordion-list-3\" class=\"collapsed\"><span>03</span> Slowing growth <i class=\"bx bx-chevron-down icon-show\"></i><i class=\"bx bx-chevron-up icon-close\"></i></a>\n                  <div id=\"accordion-list-3\" class=\"collapse\" data-parent=\".accordion-list\">\n                    <p>\n                      Maybe your salespeople have to contact your stockroom to get find out what they have to sell, or you find yourself spending too much time manually writing reports rather than focusing on your customers.\n                    </p>\n                  </div>\n                </li>\n\n                <li>\n                  <a data-toggle=\"collapse\" href=\"#accordion-list-3\" class=\"collapsed\"><span>04</span> If Inventory costs are growing <i class=\"bx bx-chevron-down icon-show\"></i><i class=\"bx bx-chevron-up icon-close\"></i></a>\n                  <div id=\"accordion-list-3\" class=\"collapse\" data-parent=\".accordion-list\">\n                    <p>\n                      Holding costs can severely undermine your bottom line when they are not kept under control. The key to minimising holding costs is only to store as much stock as you need. An efficient inventory management system helps you keep your storage efficient.\n                    </p>\n                  </div>\n                </li>\n\n              </ul>\n            </div>\n\n          </div>\n\n          <div class=\"col-lg-5 align-items-stretch order-1 order-lg-2 img\" style='background-image: url(\"../../../assets/img/why-us.png\");' data-aos=\"zoom-in\" data-aos-delay=\"150\">&nbsp;</div>\n        </div>\n\n      </div>\n    </section><!-- End Why Us Section -->\n\n    <!-- ======= Skills Section ======= -->\n    <!-- <section id=\"skills\" class=\"skills\">\n      <div class=\"container\" data-aos=\"fade-up\">\n\n        <div class=\"row\">\n          <div class=\"col-lg-6 d-flex align-items-center\" data-aos=\"fade-right\" data-aos-delay=\"100\">\n            <img src=\"../../../assets/img/skills.png\" class=\"img-fluid\" alt=\"\">\n          </div>\n          <div class=\"col-lg-6 pt-4 pt-lg-0 content\" data-aos=\"fade-left\" data-aos-delay=\"100\">\n            <h3>Voluptatem dignissimos provident quasi corporis voluptates</h3>\n            <p class=\"font-italic\">\n              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore\n              magna aliqua.\n            </p>\n\n            <div class=\"skills-content\">\n\n              <div class=\"progress\">\n                <span class=\"skill\">HTML <i class=\"val\">100%</i></span>\n                <div class=\"progress-bar-wrap\">\n                  <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                </div>\n              </div>\n\n              <div class=\"progress\">\n                <span class=\"skill\">CSS <i class=\"val\">90%</i></span>\n                <div class=\"progress-bar-wrap\">\n                  <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"90\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                </div>\n              </div>\n\n              <div class=\"progress\">\n                <span class=\"skill\">JavaScript <i class=\"val\">75%</i></span>\n                <div class=\"progress-bar-wrap\">\n                  <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"75\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                </div>\n              </div>\n\n              <div class=\"progress\">\n                <span class=\"skill\">Photoshop <i class=\"val\">55%</i></span>\n                <div class=\"progress-bar-wrap\">\n                  <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"55\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                </div>\n              </div>\n\n            </div>\n\n          </div>\n        </div>\n\n      </div>\n    </section> -->\n    <!-- End Skills Section -->\n\n    <!-- ======= Services Section ======= -->\n    <section id=\"services\" class=\"services section-bg\">\n      <div class=\"container\" data-aos=\"fade-up\">\n\n        <div class=\"section-title\">\n          <h2>Services</h2>\n          <p></p>\n        </div>\n\n        <div class=\"row\">\n          <div class=\"col-xl-3 col-md-6 d-flex align-items-stretch\" data-aos=\"zoom-in\" data-aos-delay=\"100\">\n            <div class=\"icon-box\">\n              <div class=\"icon\"><i class=\"bx bxl-dribbble\"></i></div>\n              <h4><a href=\"\">Availability</a></h4>\n              <p>System will available 24/7 for any type of operation</p>\n            </div>\n          </div>\n\n          <div class=\"col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0\" data-aos=\"zoom-in\" data-aos-delay=\"200\">\n            <div class=\"icon-box\">\n              <div class=\"icon\"><i class=\"bx bx-file\"></i></div>\n              <h4><a href=\"\">Invoice and Reports</a></h4>\n              <p>Able to get daily operation Invoice and different type of Reports</p>\n            </div>\n          </div>\n\n          <div class=\"col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0\" data-aos=\"zoom-in\" data-aos-delay=\"300\">\n            <div class=\"icon-box\">\n              <div class=\"icon\"><i class=\"bx bx-tachometer\"></i></div>\n              <h4><a href=\"\">Accuracy</a></h4>\n              <p>Able to get up-to-date information of your inventory at any time</p>\n            </div>\n          </div>\n\n          <div class=\"col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0\" data-aos=\"zoom-in\" data-aos-delay=\"400\">\n            <div class=\"icon-box\">\n              <div class=\"icon\"><i class=\"bx bx-layer\"></i></div>\n              <h4><a href=\"\">Security and Backup</a></h4>\n              <p>Your data Security and Backup on daily basis.</p>\n            </div>\n          </div>\n\n        </div>\n\n      </div>\n    </section>\n    <!-- End Services Section -->\n\n\n\n    <!-- ======= Cta Section ======= -->\n    <section id=\"cta\" class=\"cta\">\n      <div class=\"container\" data-aos=\"zoom-in\">\n\n        <div class=\"row\">\n          <div class=\"col-lg-9 text-center text-lg-left\">\n            <h3>Call To Action</h3>\n            <p> We offer a 1 month FREE Membership Trial to users who have not taken advantage of this before. For one month, you will be able to experience the benefits of having a Membership plan for free: </p>\n          </div>\n          <div class=\"col-lg-3 cta-btn-container text-center\">\n            <a class=\"cta-btn align-middle\" href=\"tel:+91 70453 82844\">Call To Action</a>\n          </div>\n        </div>\n       \n      </div>\n    </section><!-- End Cta Section -->\n\n    <!-- ======= Portfolio Section ======= -->\n    <!-- <section id=\"portfolio\" class=\"portfolio\">\n      <div class=\"container\" data-aos=\"fade-up\">\n\n        <div class=\"section-title\">\n          <h2>Portfolio</h2>\n          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>\n        </div>\n\n        <ul id=\"portfolio-flters\" class=\"d-flex justify-content-center\" data-aos=\"fade-up\" data-aos-delay=\"100\">\n          <li data-filter=\"*\" class=\"filter-active\">All</li>\n          <li data-filter=\".filter-app\">App</li>\n          <li data-filter=\".filter-card\">Card</li>\n          <li data-filter=\".filter-web\">Web</li>\n        </ul>\n\n        <div class=\"row portfolio-container\" data-aos=\"fade-up\" data-aos-delay=\"200\">\n\n          <div class=\"col-lg-4 col-md-6 portfolio-item filter-app\">\n            <div class=\"portfolio-img\"><img src=\"../../../assets/img/portfolio/portfolio-1.jpg\" class=\"img-fluid\" alt=\"\"></div>\n            <div class=\"portfolio-info\">\n              <h4>App 1</h4>\n              <p>App</p>\n              <a href=\"../../../assets/img/portfolio/portfolio-1.jpg\" data-gall=\"portfolioGallery\" class=\"venobox preview-link\" title=\"App 1\"><i class=\"bx bx-plus\"></i></a>\n              <a href=\"portfolio-details.html\" class=\"details-link\" title=\"More Details\"><i class=\"bx bx-link\"></i></a>\n            </div>\n          </div>\n\n          <div class=\"col-lg-4 col-md-6 portfolio-item filter-web\">\n            <div class=\"portfolio-img\"><img src=\"../../../assets/img/portfolio/portfolio-2.jpg\" class=\"img-fluid\" alt=\"\"></div>\n            <div class=\"portfolio-info\">\n              <h4>Web 3</h4>\n              <p>Web</p>\n              <a href=\"../../../assets/img/portfolio/portfolio-2.jpg\" data-gall=\"portfolioGallery\" class=\"venobox preview-link\" title=\"Web 3\"><i class=\"bx bx-plus\"></i></a>\n              <a href=\"portfolio-details.html\" class=\"details-link\" title=\"More Details\"><i class=\"bx bx-link\"></i></a>\n            </div>\n          </div>\n\n          <div class=\"col-lg-4 col-md-6 portfolio-item filter-app\">\n            <div class=\"portfolio-img\"><img src=\"../../../assets/img/portfolio/portfolio-3.jpg\" class=\"img-fluid\" alt=\"\"></div>\n            <div class=\"portfolio-info\">\n              <h4>App 2</h4>\n              <p>App</p>\n              <a href=\"../../../assets/img/portfolio/portfolio-3.jpg\" data-gall=\"portfolioGallery\" class=\"venobox preview-link\" title=\"App 2\"><i class=\"bx bx-plus\"></i></a>\n              <a href=\"portfolio-details.html\" class=\"details-link\" title=\"More Details\"><i class=\"bx bx-link\"></i></a>\n            </div>\n          </div>\n\n          <div class=\"col-lg-4 col-md-6 portfolio-item filter-card\">\n            <div class=\"portfolio-img\"><img src=\"../../../assets/img/portfolio/portfolio-4.jpg\" class=\"img-fluid\" alt=\"\"></div>\n            <div class=\"portfolio-info\">\n              <h4>Card 2</h4>\n              <p>Card</p>\n              <a href=\"../../../assets/img/portfolio/portfolio-4.jpg\" data-gall=\"portfolioGallery\" class=\"venobox preview-link\" title=\"Card 2\"><i class=\"bx bx-plus\"></i></a>\n              <a href=\"portfolio-details.html\" class=\"details-link\" title=\"More Details\"><i class=\"bx bx-link\"></i></a>\n            </div>\n          </div>\n\n          <div class=\"col-lg-4 col-md-6 portfolio-item filter-web\">\n            <div class=\"portfolio-img\"><img src=\"../../../assets/img/portfolio/portfolio-5.jpg\" class=\"img-fluid\" alt=\"\"></div>\n            <div class=\"portfolio-info\">\n              <h4>Web 2</h4>\n              <p>Web</p>\n              <a href=\"../../../assets/img/portfolio/portfolio-5.jpg\" data-gall=\"portfolioGallery\" class=\"venobox preview-link\" title=\"Web 2\"><i class=\"bx bx-plus\"></i></a>\n              <a href=\"portfolio-details.html\" class=\"details-link\" title=\"More Details\"><i class=\"bx bx-link\"></i></a>\n            </div>\n          </div>\n\n          <div class=\"col-lg-4 col-md-6 portfolio-item filter-app\">\n            <div class=\"portfolio-img\"><img src=\"../../../assets/img/portfolio/portfolio-6.jpg\" class=\"img-fluid\" alt=\"\"></div>\n            <div class=\"portfolio-info\">\n              <h4>App 3</h4>\n              <p>App</p>\n              <a href=\"../../../assets/img/portfolio/portfolio-6.jpg\" data-gall=\"portfolioGallery\" class=\"venobox preview-link\" title=\"App 3\"><i class=\"bx bx-plus\"></i></a>\n              <a href=\"portfolio-details.html\" class=\"details-link\" title=\"More Details\"><i class=\"bx bx-link\"></i></a>\n            </div>\n          </div>\n\n          <div class=\"col-lg-4 col-md-6 portfolio-item filter-card\">\n            <div class=\"portfolio-img\"><img src=\"../../../assets/img/portfolio/portfolio-7.jpg\" class=\"img-fluid\" alt=\"\"></div>\n            <div class=\"portfolio-info\">\n              <h4>Card 1</h4>\n              <p>Card</p>\n              <a href=\"../../../assets/img/portfolio/portfolio-7.jpg\" data-gall=\"portfolioGallery\" class=\"venobox preview-link\" title=\"Card 1\"><i class=\"bx bx-plus\"></i></a>\n              <a href=\"portfolio-details.html\" class=\"details-link\" title=\"More Details\"><i class=\"bx bx-link\"></i></a>\n            </div>\n          </div>\n\n          <div class=\"col-lg-4 col-md-6 portfolio-item filter-card\">\n            <div class=\"portfolio-img\"><img src=\"../../../assets/img/portfolio/portfolio-8.jpg\" class=\"img-fluid\" alt=\"\"></div>\n            <div class=\"portfolio-info\">\n              <h4>Card 3</h4>\n              <p>Card</p>\n              <a href=\"../../../assets/img/portfolio/portfolio-8.jpg\" data-gall=\"portfolioGallery\" class=\"venobox preview-link\" title=\"Card 3\"><i class=\"bx bx-plus\"></i></a>\n              <a href=\"portfolio-details.html\" class=\"details-link\" title=\"More Details\"><i class=\"bx bx-link\"></i></a>\n            </div>\n          </div>\n\n          <div class=\"col-lg-4 col-md-6 portfolio-item filter-web\">\n            <div class=\"portfolio-img\"><img src=\"../../../assets/img/portfolio/portfolio-9.jpg\" class=\"img-fluid\" alt=\"\"></div>\n            <div class=\"portfolio-info\">\n              <h4>Web 3</h4>\n              <p>Web</p>\n              <a href=\"../../../assets/img/portfolio/portfolio-9.jpg\" data-gall=\"portfolioGallery\" class=\"venobox preview-link\" title=\"Web 3\"><i class=\"bx bx-plus\"></i></a>\n              <a href=\"portfolio-details.html\" class=\"details-link\" title=\"More Details\"><i class=\"bx bx-link\"></i></a>\n            </div>\n          </div>\n\n        </div>\n\n      </div>\n    </section> -->\n    <!-- End Portfolio Section -->\n\n    <!-- ======= Team Section ======= -->\n    <section id=\"team\" class=\"team section-bg\">\n      <div class=\"container\" data-aos=\"fade-up\">\n\n        <div class=\"section-title\">\n          <h2>Team</h2>\n          <p>Meet the leadership team of Warehouse Managment Solution</p>\n        </div>\n\n        <div class=\"row\">\n\n          <div class=\"col-lg-6 mt-4 mt-lg-0\">\n            <div class=\"member d-flex align-items-start\" data-aos=\"zoom-in\" data-aos-delay=\"100\">\n              <div class=\"pic\"><img src=\"../../../assets/img/team/SantoshProfilePhoto.png\" class=\"img-fluid\" alt=\"\"></div>\n              <div class=\"member-info\">\n                <h4>Santosh Kalange</h4>\n                <span>Software Developer</span>\n                <p></p>\n                <div class=\"social\">\n                  <a href=\"https://twitter.com/KALANGE10\"><i class=\"ri-twitter-fill\"></i></a>\n                  <a href=\"https://www.facebook.com/santoshkalange10/\"><i class=\"ri-facebook-fill\"></i></a>\n                  <a href=\"https://www.instagram.com/_.santosh._10/\"><i class=\"ri-instagram-fill\"></i></a>\n                  <a href=\"https://www.linkedin.com/in/santosh-k-88467b128/\"> <i class=\"ri-linkedin-box-fill\"></i> </a>\n                </div>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"col-lg-6 mt-4 mt-lg-0\">\n            <div class=\"member d-flex align-items-start\" data-aos=\"zoom-in\" data-aos-delay=\"200\">\n              <div class=\"pic\"><img src=\"../../../assets/img/team/RaviProfilePhoto.png\" class=\"img-fluid\" alt=\"\"></div>\n              <div class=\"member-info\">\n                <h4>Ravi Thokal</h4>\n                <span>Software Developer</span>\n                <p></p>\n                <div class=\"social\">\n                  <a href=\"\"><i class=\"ri-twitter-fill\"></i></a>\n                  <a href=\"https://www.facebook.com/ravi.thokal.3\"><i class=\"ri-facebook-fill\"></i></a>\n                  <a href=\"https://www.instagram.com/ravi_thokal/\"><i class=\"ri-instagram-fill\"></i></a>\n                  <a href=\"https://www.linkedin.com/in/ravi-thokal-109250148/\"> <i class=\"ri-linkedin-box-fill\"></i> </a>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </section>\n    <!-- End Team Section -->\n\n    <!-- ======= Pricing Section ======= -->\n    <section id=\"pricing\" class=\"pricing\">\n      <div class=\"container\" data-aos=\"fade-up\">\n\n        <div class=\"section-title\">\n          <h2>Pricing</h2>\n          <p>Intrested...Start your 1 MONTH FREE TRIAL....Find a storage plan.</p>\n        </div>\n\n        <div class=\"row\">\n\n          <div class=\"col-lg-4\" data-aos=\"fade-up\" data-aos-delay=\"100\">\n            <div class=\"box\">\n              <h3>Quarterly plan</h3>\n              <h4><sup>₹</sup>2500<span>Three Months</span></h4>\n              <ul>\n                <li><i class=\"bx bx-check\"></i> 1 Month Free Trial</li>\n                <li><i class=\"bx bx-check\"></i> Web + Mobile Application</li>\n                <li><i class=\"bx bx-check\"></i> 24/7 Availability</li>\n                <li><i class=\"bx bx-check\"></i> <span> Free Support</span></li>\n                <!-- <li><i class=\"bx bx-x\"></i> <span> </span></li> -->\n              </ul>\n              <a Contact Us class=\"buy-btn\">Contact Us</a>\n            </div>\n          </div>\n\n          <div class=\"col-lg-4 mt-4 mt-lg-0\" data-aos=\"fade-up\" data-aos-delay=\"200\">\n            <div class=\"box featured\">\n              <h3>Semi Annual Plan</h3>\n              <h4><sup>₹</sup>5000<span>6 Months</span></h4>\n              <ul>\n                <li><i class=\"bx bx-check\"></i> 1 Month Free Trial</li>\n                <li><i class=\"bx bx-check\"></i> Web + Mobile Application</li>\n                <li><i class=\"bx bx-check\"></i> 24/7 Availability</li>\n                <li><i class=\"bx bx-check\"></i> <span> Free Support</span></li>\n                <!-- <li><i class=\"bx bx-x\"></i> <span> </span></li> -->\n              </ul>\n              <a href=\"tel:+91 70453 82844\" class=\"buy-btn\">Contact Us</a>\n            </div>\n          </div>\n\n          <div class=\"col-lg-4 mt-4 mt-lg-0\" data-aos=\"fade-up\" data-aos-delay=\"300\">\n            <div class=\"box\">\n              <h3>Annual Plan</h3>\n              <h4><sup>₹</sup>10000<span>12 Month Plan</span></h4>\n              <ul>\n                <li><i class=\"bx bx-check\"></i> 1 Month Free Trial</li>\n                <li><i class=\"bx bx-check\"></i> Web + Mobile Application</li>\n                <li><i class=\"bx bx-check\"></i> 24/7 Availability</li>\n                <li><i class=\"bx bx-check\"></i> <span> Free Support</span></li>\n                <!-- <li><i class=\"bx bx-x\"></i> <span> </span></li> -->\n              </ul>\n              <a href=\"tel:+91 70453 82844\" class=\"buy-btn\">Contact Us</a>\n            </div>\n          </div>\n\n        </div>\n\n      </div>\n    </section>\n    <!-- End Pricing Section -->\n\n    <!-- ======= Frequently Asked Questions Section ======= -->\n    <!-- <section id=\"faq\" class=\"faq section-bg\">\n      <div class=\"container\" data-aos=\"fade-up\">\n\n        <div class=\"section-title\">\n          <h2>Frequently Asked Questions</h2>\n          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>\n        </div>\n\n        <div class=\"faq-list\">\n          <ul>\n            <li data-aos=\"fade-up\" data-aos-delay=\"100\">\n              <i class=\"bx bx-help-circle icon-help\"></i> <a data-toggle=\"collapse\" class=\"collapse\" href=\"#faq-list-1\">Non consectetur a erat nam at lectus urna duis? <i class=\"bx bx-chevron-down icon-show\"></i><i class=\"bx bx-chevron-up icon-close\"></i></a>\n              <div id=\"faq-list-1\" class=\"collapse show\" data-parent=\".faq-list\">\n                <p>\n                  Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.\n                </p>\n              </div>\n            </li>\n\n            <li data-aos=\"fade-up\" data-aos-delay=\"200\">\n              <i class=\"bx bx-help-circle icon-help\"></i> <a data-toggle=\"collapse\" href=\"#faq-list-2\" class=\"collapsed\">Feugiat scelerisque varius morbi enim nunc? <i class=\"bx bx-chevron-down icon-show\"></i><i class=\"bx bx-chevron-up icon-close\"></i></a>\n              <div id=\"faq-list-2\" class=\"collapse\" data-parent=\".faq-list\">\n                <p>\n                  Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.\n                </p>\n              </div>\n            </li>\n\n            <li data-aos=\"fade-up\" data-aos-delay=\"300\">\n              <i class=\"bx bx-help-circle icon-help\"></i> <a data-toggle=\"collapse\" href=\"#faq-list-3\" class=\"collapsed\">Dolor sit amet consectetur adipiscing elit? <i class=\"bx bx-chevron-down icon-show\"></i><i class=\"bx bx-chevron-up icon-close\"></i></a>\n              <div id=\"faq-list-3\" class=\"collapse\" data-parent=\".faq-list\">\n                <p>\n                  Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis\n                </p>\n              </div>\n            </li>\n\n            <li data-aos=\"fade-up\" data-aos-delay=\"400\">\n              <i class=\"bx bx-help-circle icon-help\"></i> <a data-toggle=\"collapse\" href=\"#faq-list-4\" class=\"collapsed\">Tempus quam pellentesque nec nam aliquam sem et tortor consequat? <i class=\"bx bx-chevron-down icon-show\"></i><i class=\"bx bx-chevron-up icon-close\"></i></a>\n              <div id=\"faq-list-4\" class=\"collapse\" data-parent=\".faq-list\">\n                <p>\n                  Molestie a iaculis at erat pellentesque adipiscing commodo. Dignissim suspendisse in est ante in. Nunc vel risus commodo viverra maecenas accumsan. Sit amet nisl suscipit adipiscing bibendum est. Purus gravida quis blandit turpis cursus in.\n                </p>\n              </div>\n            </li>\n\n            <li data-aos=\"fade-up\" data-aos-delay=\"500\">\n              <i class=\"bx bx-help-circle icon-help\"></i> <a data-toggle=\"collapse\" href=\"#faq-list-5\" class=\"collapsed\">Tortor vitae purus faucibus ornare. Varius vel pharetra vel turpis nunc eget lorem dolor? <i class=\"bx bx-chevron-down icon-show\"></i><i class=\"bx bx-chevron-up icon-close\"></i></a>\n              <div id=\"faq-list-5\" class=\"collapse\" data-parent=\".faq-list\">\n                <p>\n                  Laoreet sit amet cursus sit amet dictum sit amet justo. Mauris vitae ultricies leo integer malesuada nunc vel. Tincidunt eget nullam non nisi est sit amet. Turpis nunc eget lorem dolor sed. Ut venenatis tellus in metus vulputate eu scelerisque.\n                </p>\n              </div>\n            </li>\n\n          </ul>\n        </div>\n\n      </div>\n    </section> -->\n    <!-- End Frequently Asked Questions Section -->\n\n    <!-- ======= Contact Section ======= -->\n    <section id=\"contact\" class=\"contact\">\n      <div class=\"container\" data-aos=\"fade-up\">\n\n        <div class=\"section-title\">\n          <h2>Contact</h2>\n          <p></p>\n        </div>\n\n        <div class=\"row\">\n\n          <div class=\"col-lg-5 d-flex align-items-stretch\">\n            <div class=\"info\">\n              <div class=\"address\">\n                <i class=\"icofont-google-map\"></i>\n                <h4>Location:</h4>\n                <p>Suryanagar, Vikhroli (W), Mumbai - 400083</p>\n              </div>\n\n              <div class=\"email\">\n                <i class=\"icofont-envelope\"></i>\n                <h4>Email:</h4>\n                <p>warehousemanagesystem@gmail.com</p>\n              </div>\n\n              <div class=\"phone\">\n                <i class=\"icofont-phone\"></i>\n                <h4>Call:</h4>\n                <p><a href=\"tel:+91 70453 82844\">+91 7045382844</a> / <a href=\"tel:+91 98216 14175\">+91 9821614175</a></p>\n              </div>\n              \n              <iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1142.5346410979844!2d72.92361910358093!3d19.119627789319864!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c7952e0537d9%3A0xa78d0b96bdf2c40f!2sWarehouse%20Management%20System!5e0!3m2!1sen!2sin!4v1611859491636!5m2!1sen!2sin\" frameborder=\"0\" style=\"border:0; width: 100%; height: 290px;\" allowfullscreen></iframe>\n            </div>\n\n          </div>\n\n          <div class=\"col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch\">\n            <form action=\"forms/contact.php\" method=\"post\" role=\"form\" class=\"php-email-form\">\n              <div class=\"form-row\">\n                <div class=\"form-group col-md-6\">\n                  <label for=\"name\">Your Name</label>\n                  <input type=\"text\" name=\"name\" class=\"form-control\" id=\"name\" data-rule=\"minlen:4\" data-msg=\"Please enter at least 4 chars\" />\n                  <div class=\"validate\"></div>\n                </div>\n                <div class=\"form-group col-md-6\">\n                  <label for=\"name\">Your Email</label>\n                  <input type=\"email\" class=\"form-control\" name=\"email\" id=\"email\" data-rule=\"email\" data-msg=\"Please enter a valid email\" />\n                  <div class=\"validate\"></div>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"name\">Subject</label>\n                <input type=\"text\" class=\"form-control\" name=\"subject\" id=\"subject\" data-rule=\"minlen:4\" data-msg=\"Please enter at least 8 chars of subject\" />\n                <div class=\"validate\"></div>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"name\">Message</label>\n                <textarea class=\"form-control\" name=\"message\" rows=\"10\" data-rule=\"required\" data-msg=\"Please write something for us\"></textarea>\n                <div class=\"validate\"></div>\n              </div>\n              <div class=\"mb-3\">\n                <div class=\"loading\">Loading</div>\n                <div class=\"error-message\"></div>\n                <div class=\"sent-message\">Your message has been sent. Thank you!</div>\n              </div>\n              <div class=\"text-center\"><button type=\"submit\">Send Message</button></div>\n            </form>\n          </div>\n\n        </div>\n\n      </div>\n    </section><!-- End Contact Section -->\n\n  </main><!-- End #main -->\n\n  <!-- ======= Footer ======= -->\n  <footer id=\"footer\">\n\n    <!-- <div class=\"footer-newsletter\">\n      <div class=\"container\">\n        <div class=\"row justify-content-center\">\n          <div class=\"col-lg-6\">\n            <h4>Join Our Newsletter</h4>\n            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>\n            <form action=\"\" method=\"post\">\n              <input type=\"email\" name=\"email\"><input type=\"submit\" value=\"Subscribe\">\n            </form>\n          </div>\n        </div>\n      </div>\n    </div> -->\n\n    <div class=\"footer-top\">\n      <div class=\"container\">\n        <div class=\"row\">\n\n          <div class=\"col-lg-3 col-md-6 footer-contact\">\n            <a href=\"index.html\" class=\"logo mr-auto\"><img src=\"../../../assets/img/wmlogowithtitle3.png\" alt=\"\" class=\"img-fluid\"></a>\n            \n          </div>\n\n          <div class=\"col-lg-3 col-md-6 footer-links\">\n            <h4>Contact</h4>\n              <p>\n              Suryanagar, Vikhroli (W) <br>\n              Mumbai - 400083, Maharashtra<br>\n              India   \n              </p>\n              <strong>Phone:</strong><br> \n              <a href=\"tel:+91 70453 82844\">+91 7045382844</a><br>\n              <a href=\"tel:+91 98216 14175\">+91 9821614175</a><br>\n              <strong>Email:</strong> warehousemanagesystem@gmail.com<br>\n          </div>\n\n          <div class=\"col-lg-3 col-md-6 footer-links\">\n            <h4>Useful Links</h4>\n            <ul>\n              <li><i class=\"bx bx-chevron-right\"></i> <a href=\"#\">Home</a></li>\n              <li><i class=\"bx bx-chevron-right\"></i> <a href=\"#about\">About us</a></li>\n              <li><i class=\"bx bx-chevron-right\"></i> <a href=\"#service\">Services</a></li>\n              <li><i class=\"bx bx-chevron-right\"></i> <a href=\"#\">Terms of service</a></li>\n              <li><i class=\"bx bx-chevron-right\"></i> <a href=\"#\">Privacy policy</a></li>\n            </ul>\n          </div>\n\n          <div class=\"col-lg-3 col-md-6 footer-links\">\n            <h4>Our Social Networks</h4>\n            <p>____________________________________</p>\n            <div class=\"social-links mt-3\">\n              <a href=\"#\" class=\"twitter\"><i class=\"bx bxl-twitter\"></i></a>\n              <a href=\"https://www.facebook.com/manageyourstoragesystem\" class=\"facebook\"><i class=\"bx bxl-facebook\"></i></a>\n              <a href=\"https://www.facebook.com/manageyourstoragesystem\" class=\"instagram\"><i class=\"bx bxl-instagram\"></i></a>\n              <a href=\"#\" class=\"google-plus\"><i class=\"bx bxl-skype\"></i></a>\n              <a href=\"#\" class=\"linkedin\"><i class=\"bx bxl-linkedin\"></i></a>\n            </div>\n          </div>\n\n        </div>\n      </div>\n    </div>\n\n    <div class=\"container footer-bottom clearfix\">\n      <div class=\"copyright\">\n        &copy; Copyright <strong><span>warehousemanagmentsystem</span></strong>. All Rights Reserved\n      </div>\n      <div class=\"credits\">\n        <!-- All the links in the footer should remain intact. -->\n        <!-- You can delete the links only if you purchased the pro version. -->\n        <!-- Licensing information: https://bootstrapmade.com/license/ -->\n        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/ -->\n        Designed by <a href=\"#team\">Team</a>\n      </div>\n    </div>\n  </footer><!-- End Footer -->\n\n  <a href=\"#\" class=\"back-to-top\"><i class=\"ri-arrow-up-line\"></i></a>\n  <!-- <div id=\"preloader\"></div> -->\n\n<!-- <router-outlet></router-outlet> -->\n"

/***/ }),

/***/ "./src/app/modules/landing/landing.component.scss":
/*!********************************************************!*\
  !*** ./src/app/modules/landing/landing.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvbGFuZGluZy9sYW5kaW5nLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/modules/landing/landing.component.ts":
/*!******************************************************!*\
  !*** ./src/app/modules/landing/landing.component.ts ***!
  \******************************************************/
/*! exports provided: LandingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingComponent", function() { return LandingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LandingComponent = /** @class */ (function () {
    function LandingComponent() {
        this.title = 'wm-frontend';
    }
    LandingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-landing',
            template: __webpack_require__(/*! ./landing.component.html */ "./src/app/modules/landing/landing.component.html"),
            styles: [__webpack_require__(/*! ./landing.component.scss */ "./src/app/modules/landing/landing.component.scss")]
        })
    ], LandingComponent);
    return LandingComponent;
}());



/***/ }),

/***/ "./src/app/services/auth-guard.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/auth-guard.service.ts ***!
  \************************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/build/jwt-decode.esm.js");




var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    /**
     * @description This method will check user already login or not
     * @returns Return the true if user already logged in or else false
     */
    AuthGuard.prototype.canActivate = function () {
        try {
            var isTokenAvailable = localStorage.getItem('access_token');
            if (isTokenAvailable) {
                var decoded = Object(jwt_decode__WEBPACK_IMPORTED_MODULE_3__["default"])(isTokenAvailable);
                if (decoded.role === 'Vendor') {
                    this.router.navigate(['/vendor']);
                }
                else if (decoded.role === 'WAREHOUSE') {
                    this.router.navigate(['/warehouse']);
                }
                return true;
            }
            else {
                return true;
            }
        }
        catch (err) {
            return false;
        }
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/services/vendor-auth-guard.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/vendor-auth-guard.service.ts ***!
  \*******************************************************/
/*! exports provided: VendorAuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendorAuthGuard", function() { return VendorAuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/build/jwt-decode.esm.js");




var VendorAuthGuard = /** @class */ (function () {
    function VendorAuthGuard(router) {
        this.router = router;
    }
    /**
     * @description This method will check user already login or not
     * @returns Return the true if user already logged in or else false
     */
    VendorAuthGuard.prototype.canActivate = function () {
        try {
            var isTokenAvailable = localStorage.getItem('access_token');
            if (isTokenAvailable) {
                var decoded = Object(jwt_decode__WEBPACK_IMPORTED_MODULE_3__["default"])(isTokenAvailable);
                if (decoded.role === 'Vendor') {
                    return true;
                }
                else {
                    this.router.navigate(['/dw-home']);
                }
                return true;
            }
            else {
                this.router.navigate(['/dw-home']);
            }
        }
        catch (err) {
            return false;
        }
    };
    VendorAuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], VendorAuthGuard);
    return VendorAuthGuard;
}());



/***/ }),

/***/ "./src/app/services/warehouse-auth-guard.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/warehouse-auth-guard.service.ts ***!
  \**********************************************************/
/*! exports provided: WarehouseAuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WarehouseAuthGuard", function() { return WarehouseAuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/build/jwt-decode.esm.js");




var WarehouseAuthGuard = /** @class */ (function () {
    function WarehouseAuthGuard(router) {
        this.router = router;
    }
    /**
     * @description This method will check user already login or not
     * @returns Return the true if user already logged in or else false
     */
    WarehouseAuthGuard.prototype.canActivate = function () {
        try {
            var isTokenAvailable = localStorage.getItem('access_token');
            if (isTokenAvailable) {
                var decoded = Object(jwt_decode__WEBPACK_IMPORTED_MODULE_3__["default"])(isTokenAvailable);
                if (decoded.role === 'WAREHOUSE') {
                    return true;
                }
                else {
                    this.router.navigate(['/dw-home']);
                }
            }
            else {
                this.router.navigate(['/dw-home']);
            }
        }
        catch (err) {
            return false;
        }
    };
    WarehouseAuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], WarehouseAuthGuard);
    return WarehouseAuthGuard;
}());



/***/ }),

/***/ "./src/app/store/reducer-composition.ts":
/*!**********************************************!*\
  !*** ./src/app/store/reducer-composition.ts ***!
  \**********************************************/
/*! exports provided: reducers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reducers", function() { return reducers; });
/* harmony import */ var _warehouse_reducer_warehouse_reducer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./warehouse/reducer/warehouse.reducer */ "./src/app/store/warehouse/reducer/warehouse.reducer.ts");

var reducers = {
    warehouseData: _warehouse_reducer_warehouse_reducer__WEBPACK_IMPORTED_MODULE_0__["warehouseReducer"],
};


/***/ }),

/***/ "./src/app/store/warehouse/action/warehouse.action.ts":
/*!************************************************************!*\
  !*** ./src/app/store/warehouse/action/warehouse.action.ts ***!
  \************************************************************/
/*! exports provided: warehouseAction */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "warehouseAction", function() { return warehouseAction; });
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm2015/ngrx-store.js");

var warehouseAction = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])('', Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])());


/***/ }),

/***/ "./src/app/store/warehouse/reducer/warehouse.reducer.ts":
/*!**************************************************************!*\
  !*** ./src/app/store/warehouse/reducer/warehouse.reducer.ts ***!
  \**************************************************************/
/*! exports provided: initialState, warehouseReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialState", function() { return initialState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "warehouseReducer", function() { return warehouseReducer; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/fesm2015/ngrx-store.js");
/* harmony import */ var _action_warehouse_action__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../action/warehouse.action */ "./src/app/store/warehouse/action/warehouse.action.ts");



var initialState = {
    data: {},
};
var Reducer = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_1__["createReducer"])(initialState, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_1__["on"])(_action_warehouse_action__WEBPACK_IMPORTED_MODULE_2__["warehouseAction"], function (state) { return (tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, state)); }));
function warehouseReducer(state, action) {
    return Reducer(state, action);
}


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/macbook/Desktop/Ravi/WM/wm-frontend/wm-frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map